<?php
/**
 * REST API CORS filter.
 *
 * @package  Postlight_Headless_WP
 */

/**
 * Allow GET requests from origin
 * Thanks to https://joshpress.net/access-control-headers-for-the-wordpress-rest-api/
 */
add_action(
    'rest_api_init',
    function () {
        remove_filter( 'rest_pre_serve_request', 'rest_send_cors_headers' );

        add_filter(
            'rest_pre_serve_request',
            function ( $value ) {
                $allowed_domains = get_frontend_origin();
                if(in_array($_SERVER['HTTP_ORIGIN'], $allowed_domains)) {
                    header( 'Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN'] );
                    header( 'Access-Control-Allow-Methods: GET' );
                    header( 'Access-Control-Allow-Credentials: true' );
                }
                return $value;
            }
        );
    },
    15
);

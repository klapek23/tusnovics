<?php

function get_theme_options() {
    $footerLeftColumn = toz_option('Footer left column' , '76515782678');
    $footerCopy = toz_option('Footer copy' , '5001668206');
    $fbAppId = toz_option('Facebook APP ID' , '22819558609');
    $data = new StdClass();
    $data->footerLeftColumn =$footerLeftColumn;
    $data->footerCopy =$footerCopy;
    $data->fbAppId =$fbAppId;
    // $data->env = getenv();
    return $data;
}

function login($request) {
    $creds = array();
    $creds['user_login'] = $request['username'];
    $creds['user_password'] = $request['password'];
    $creds['remember'] = true;

    $user = wp_signon( $creds, false );

    if ( is_wp_error($user) ) {
        return new WP_HTTP_Response(null, 403);
    }
    return new WP_REST_Response($user);
}

function is_logged_in() {
    $loggedIn = is_user_logged_in();
    return $loggedIn;
}

add_action( 'rest_api_init', function () {
  register_rest_route( 'custom', '/theme-options', array(
    'methods' => 'GET',
    'callback' => 'get_theme_options',
  ) );

  register_rest_route( 'custom', '/login', array(
      'methods' => 'POST',
      'callback' => 'login',
    ) );

    register_rest_route( 'custom', '/is-logged-in', array(
      'methods' => 'GET',
      'callback' => 'is_logged_in',
    ) );
} );

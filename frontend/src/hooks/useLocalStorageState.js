import { useEffect, useState } from 'react';

export default function useLocalStorageState(defaultValue, key) {
  const [value, setValue] = useState(() => {
    const tmpValue =
      typeof window !== 'undefined' ? window.localStorage.getItem(key) : null;
    return tmpValue !== null ? JSON.parse(tmpValue) : defaultValue;
  });

  useEffect(() => {
    if (window) window.localStorage.setItem(key, JSON.stringify(value));
  }, [key, value]);
  return [value, setValue];
}

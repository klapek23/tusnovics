import React from 'react';
import createKey from '@/src/utils/createKey';
import TsvcShortNewsListItem from '@/src/components/TsvcShortNewsListItem';

export default function TsvcShortNewsList({ title, items }) {
  return (
    <section className="tsvc-news-list tsvc-news-list--short">
      <h3>{title}</h3>
      <ul>
        {items.map((item) => (
          <TsvcShortNewsListItem
            {...item}
            key={createKey(item.title.rendered)}
          />
        ))}
      </ul>
    </section>
  );
}

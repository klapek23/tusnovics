import React from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import config from '@/config';
import TsvcLink from '@/src/components/common/TsvcLink';

export default function TsvcDesktopMenu({ items }) {
  const { pathname } = useRouter();
  const pathnameArr = pathname.split('/');
  const currentTopLevelItem = `/${pathnameArr[1]}`;
  const currentSecondLevelItem = `${pathnameArr[1]}/${pathnameArr[2]}`;

  function renderItems(menuItems) {
    return menuItems.map((item) => {
      const regexp = new RegExp(`^${config.wpUrl}`, 'gmi');
      const parsedItemUrl =
        `${item.url.replace(regexp, '').replace(/\/$/gim, '')}` || '/';

      if (item.object === 'custom') {
        return (
          <div
            className={`header-menu__item ${
              item.child_items ? 'has-submenu' : ''
            }`}
            key={item.ID}
          >
            <TsvcLink
              className={`${
                currentTopLevelItem === parsedItemUrl ? 'active' : ''
              }`}
              as={item.url}
              href={item.url}
              key={item.ID}
            >
              {item.title}
            </TsvcLink>
          </div>
        );
      }

      return (
        <div
          className={`header-menu__item ${
            item.child_items ? 'has-submenu' : ''
          }`}
          key={item.ID}
        >
          <TsvcLink
            className={`${
              currentTopLevelItem === parsedItemUrl ? 'active' : ''
            }`}
            as={item.url}
            href={item.url}
            key={item.ID}
          >
            {item.title}
          </TsvcLink>

          {item.child_items && (
            <div className="header-menu__submenu">
              <ul>
                {item.child_items.map((subitem) => {
                  const parsedSubitemUrl = subitem.url
                    .replace(regexp, '')
                    .replace(/\/$/gim, '');
                  return (
                    <li
                      key={subitem.ID}
                      className={`${
                        currentSecondLevelItem === parsedSubitemUrl
                          ? 'active'
                          : ''
                      }`}
                    >
                      <TsvcLink href={subitem.url} as={subitem.url}>
                        {subitem.title}
                      </TsvcLink>
                    </li>
                  );
                })}
              </ul>
            </div>
          )}
        </div>
      );
    });
  }

  const leftItems = items.filter((_, key) => key < 3);
  const rightItems = items.filter((_, key) => key >= 3);
  return (
    <div className="container-fluid">
      <div className="row">
        <div className="col-xs-12">
          <div className="header-menu__content">
            <div className="header-menu__content-box">
              <div className="row" style={{ width: '100%' }}>
                <div className="col-xs-4">
                  <div className="header-menu__items">
                    {renderItems(leftItems)}
                  </div>
                </div>
                <div
                  className="col-xs-4"
                  style={{ display: 'flex', alignSelf: 'center' }}
                >
                  <Link href="/">
                    <div className="header-menu__logo">
                      <img src="/static/logo.png" alt="tusnovics logo" />
                    </div>
                  </Link>
                </div>
                <div className="col-xs-4">
                  <div className="header-menu__items">
                    {renderItems(rightItems)}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

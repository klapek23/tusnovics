import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import TsvcDesktopMenu from '@/src/components/TsvcMenu/TsvcDesktopMenu';
import TsvcMobileMenu from '@/src/components/TsvcMenu/TsvcMobileMenu';

export default function Menu({ menu }) {
  const { pathname } = useRouter();
  const [lastScrollPosition, setLastScrollPosition] = useState(0);
  const [scrolled, setScrolled] = useState(false);
  const [ticking, setTicking] = useState(false);

  useEffect(() => {
    setScrolled(window.scrollY > 10);
    window.addEventListener('scroll', scrollHandler);
    return () => window.removeEventListener('scroll', scrollHandler);
  }, []);

  function scrollHandler() {
    setLastScrollPosition(window.scrollY);
    if (!ticking) {
      window.requestAnimationFrame(() => {
        setTicking(false);
        setScrolled(lastScrollPosition > 10);
      });

      setTicking(true);
    }
  }

  return (
    <div className={`header-menu ${scrolled ? 'header-menu--scrolled' : ''}`}>
      <div className="tsvc-content-section">
        <TsvcDesktopMenu items={menu.items} pathname={pathname} />
        <TsvcMobileMenu items={menu.items} pathname={pathname} />
      </div>
    </div>
  );
}

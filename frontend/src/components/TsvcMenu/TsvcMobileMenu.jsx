import React, { useEffect, useState } from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { AnimatePresence, motion } from 'framer-motion';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import TsvcLink from '@/src/components/common/TsvcLink';
import config from '@/config';

export default function TsvcMobileMenu({ items }) {
  const { pathname, ...router } = useRouter();
  const pathnameArr = pathname.split('/');
  const currentTopLevelItem = `/${pathnameArr[1]}`;
  const currentSecondLevelItem = `${pathnameArr[1]}/${pathnameArr[2]}`;
  const [mobileMenuActive, setMobileMenuActive] = useState(false);
  const [submenuActive, setSubmenuActive] = useState(true);
  const routeChangeHandler = () => setMobileMenuActive(false);

  useEffect(() => {
    router.events.on('routeChangeComplete', routeChangeHandler);
    return () => router.events.off('routeChangeComplete', routeChangeHandler);
  }, [router]);

  const toggleSubmenu = (e) => {
    e.preventDefault();
    setSubmenuActive(!submenuActive);
  };

  function toggleMobileMenu(e) {
    e.preventDefault();
    if (!mobileMenuActive) {
      blockPageScroll();
    } else {
      allowPageScroll();
    }
    setMobileMenuActive(!mobileMenuActive);
  }

  function blockPageScroll() {
    const body = document.getElementsByTagName('body')[0];
    body.style.overflowY = 'hidden';
  }

  function allowPageScroll() {
    const body = document.getElementsByTagName('body')[0];
    body.style.overflowY = 'auto';
  }

  function renderItems(menuItems) {
    return menuItems.map((item) => {
      const regexp = new RegExp(`^${config.wpUrl}`, 'gmi');
      const parsedItemUrl =
        `${item.url.replace(regexp, '').replace(/\/$/gim, '')}` || '/';

      return (
        <div
          className={`header-menu__item ${
            item.child_items ? 'has-submenu' : ''
          }`}
          key={item.ID}
        >
          <TsvcLink
            className={`${
              currentTopLevelItem === parsedItemUrl ? 'active' : ''
            }`}
            as={item.url}
            href={item.url}
            key={item.ID}
          >
            {item.title}

            {item.child_items && (
              <ExpandMoreIcon
                className="mobile-menu__expand-submenu"
                onClick={toggleSubmenu}
              />
            )}
          </TsvcLink>

          {item.child_items && (
            <AnimatePresence>
              {submenuActive && (
                <motion.div
                  className="header-menu__submenu"
                  initial={false}
                  animate={{ opacity: 1, height: 'auto' }}
                  exit={{ opacity: 0, height: 0 }}
                >
                  <ul>
                    {item.child_items.map((subitem) => {
                      const parsedSubitemUrl = subitem.url
                        .replace(regexp, '')
                        .replace(/\/$/gim, '');
                      return (
                        <li
                          key={subitem.ID}
                          className={`${
                            `/${currentSecondLevelItem}` === parsedSubitemUrl
                              ? 'active'
                              : ''
                          }`}
                        >
                          <TsvcLink href={subitem.url} as={subitem.url}>
                            {subitem.title}
                          </TsvcLink>
                        </li>
                      );
                    })}
                  </ul>
                </motion.div>
              )}
            </AnimatePresence>
          )}
        </div>
      );
    });
  }

  return (
    <>
      <AnimatePresence>
        {mobileMenuActive && (
          <motion.div
            className="mobile-menu"
            initial={{ opacity: 0 }}
            animate={{ opacity: 1 }}
            exit={{ opacity: 0 }}
          >
            {renderItems(items)}
          </motion.div>
        )}
      </AnimatePresence>

      <div className="container-fluid">
        <div className="row">
          <div className="col-xs-12">
            <div className="header-menu__content-mobile">
              <Link href="/">
                <div className="header-menu__logo">
                  <img src="/static/logo.png" alt="tusnovics logo" />
                </div>
              </Link>
              <button
                className={`hamburger hamburger--collapse ${
                  mobileMenuActive ? 'is-active' : ''
                }`}
                type="button"
                onClick={toggleMobileMenu}
              >
                <span className="hamburger-box">
                  <span className="hamburger-inner" />
                </span>
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

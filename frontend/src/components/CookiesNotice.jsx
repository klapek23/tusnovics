import React, { useEffect, useState } from 'react';
import { AnimatePresence, motion } from 'framer-motion';
import useLocalStorageState from '@/src/hooks/useLocalStorageState';

const CloseButton = ({ onClick }) => (
  <button
    type="button"
    className="cookiesNotice__button-close"
    onClick={onClick}
  >
    <span>X</span>
  </button>
);
const AcceptButton = ({ label, onClick }) => (
  <button
    type="button"
    className="cookiesNotice__button cookiesNotice__button-accept"
    onClick={onClick}
  >
    {label}
  </button>
);
const PrivacyPolicyButton = ({ url, label }) => (
  <a
    className="cookiesNotice__privacy-link"
    href={url}
    target="_blank"
    rel="noreferrer"
  >
    {label}
  </a>
);

export default function CookiesNotice({
  message,
  cookie_button_label: cookieButtonLabel,
  display_close_button: displayCloseButton,
  display_privacy_button: displayPrivacyButton,
  privacy_policy_label: privacyPolicyLabel,
  privacy_policy_url: privacyPolicyUrl,
  cookie_info_placement: placement,
}) {
  const key = 'display_cookies';
  const [display, setDisplay] = useLocalStorageState(true, key);
  const [clientSide, setClientSide] = useState(false);

  const acceptCookies = () => {
    setDisplay(false);
  };
  const closeCookies = () => {
    setDisplay(false);
  };

  useEffect(() => {
    setClientSide(true);
  }, []);

  return (
    <section className={`cookiesNotice cookiesNotice--${placement}`}>
      <AnimatePresence>
        {clientSide && display ? (
          <motion.div
            initial={{ opacity: 0, height: 0 }}
            animate={{ opacity: 1, height: 'auto' }}
            exit={{ opacity: 0, height: 0 }}
          >
            <div className="cookiesNotice__content">
              <article className="cookiesNotice__message">{message}</article>

              <div className="cookiesNotice__actions">
                <AcceptButton
                  label={cookieButtonLabel}
                  onClick={acceptCookies}
                />

                {displayPrivacyButton === '1' &&
                  privacyPolicyLabel &&
                  privacyPolicyUrl && (
                    <PrivacyPolicyButton
                      label={privacyPolicyLabel}
                      url={privacyPolicyUrl}
                    />
                  )}
              </div>

              {displayCloseButton === '1' && (
                <CloseButton onClick={closeCookies} />
              )}
            </div>
          </motion.div>
        ) : null}
      </AnimatePresence>
    </section>
  );
}

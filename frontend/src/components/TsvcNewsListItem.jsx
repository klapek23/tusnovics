import React from 'react';
import createKey from '@/src/utils/createKey';
import TsvcLink from './common/TsvcLink';
import TsvcButton from './common/TsvcButton';
import PageSection from './common/PageSection';

export default function TsvcNewsListItem({
  link,
  title,
  image,
  excerpt,
  date,
}) {
  return (
    <li className="tsvc-news-list__item tsvc-content">
      <PageSection>
        <div className="row">
          <div className="col-xs-12 col-lg-10 col-lg-offset-1">
            <TsvcLink
              as={link.as}
              href={link.href}
              title={link.title}
              target={link.target}
              key={createKey(link.title)}
              className="tsvc-news-list__item-block"
            >
              <div className="row">
                <div className="col-xs-12 col-sm-4">
                  <div className="tsvc-news-list__item-image">
                    <img src={image.url} alt={image.alt} />
                  </div>
                </div>
                <div className="col-xs-12 col-sm-8">
                  <div className="tsvc-news-list__item-content">
                    <div className="tsvc-news-list__item-title">
                      <h4
                        dangerouslySetInnerHTML={{ __html: title.rendered }}
                      />
                    </div>
                    <div className="tsvc-news-list__item-date">{date}</div>
                    <article
                      dangerouslySetInnerHTML={{ __html: excerpt.rendered }}
                    />

                    <TsvcButton color="black">Czytaj więcej</TsvcButton>
                  </div>
                </div>
              </div>
            </TsvcLink>
          </div>
        </div>
      </PageSection>
    </li>
  );
}

import React from 'react';
import createKey from '@/src/utils/createKey';

export default function TsvcSuppliersList({ title, items }) {
  const renderItem = (label, image) => (
    <div className="tsvc-suppliers-list__item" key={createKey(label)}>
      <div className="tsvc-suppliers-list__item-image">
        <img src={image.url} alt={image.alt} />
      </div>
    </div>
  );

  return (
    <div className="tsvc-suppliers-list">
      <div className="row">
        <div className="col-xs-12 col-lg-10 col-lg-offset-1">
          <h3>{title}</h3>
          <div className="tsvc-suppliers-list__items">
            {items.map(({ label, image, link }) =>
              link ? (
                <a
                  key={createKey(label)}
                  className="tsvc-suppliers-list__link"
                  href={link.url}
                  title={link.title}
                  target="_blank"
                  rel="noreferrer"
                >
                  {renderItem(label, image)}
                </a>
              ) : (
                renderItem(label, image)
              ),
            )}
          </div>
        </div>
      </div>
    </div>
  );
}

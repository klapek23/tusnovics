import React from 'react';
import Head from 'next/head';
import Script from 'next/script';

const Header = ({ seo, ogTags, pageTitle }) => (
  <Head>
    <meta charSet="utf-8" />
    <meta
      name="viewport"
      content="minimum-scale=1, initial-scale=1, width=device-width"
    />
    <title>{seo.title || pageTitle}</title>
    <meta name="description" content={seo.description} />
    <meta name="keywords" content={seo.keywords} />
    <link rel="shortcut icon" href="/static/fav.ico" />

    <meta property="fb:app_id" content={ogTags.appId} />
    <meta property="og:title" content={ogTags.title} />
    <meta property="og:description" content={ogTags.description} />
    {ogTags.image ? <meta property="og:image" content={ogTags.image} /> : null}
    <meta property="og:url" content={ogTags.url} />
    <meta property="og:type" content={ogTags.type} />
    <meta property="og:locale" content={ogTags.locale} />

    {/* Google tag (gtag.js) */}
    {process.env.NODE_ENV === 'production' && (
      <>
        <Script
          async
          src="https://www.googletagmanager.com/gtag/js?id=G-Q1ECKKBGE5"
        />
        <Script
          id="gtag"
          dangerouslySetInnerHTML={{
            __html: `
                  window.dataLayer = window.dataLayer || [];
                  function gtag(){dataLayer.push(arguments)}
                  gtag("js", new Date());

                  gtag("config", "G-Q1ECKKBGE5");
                `,
          }}
        />
      </>
    )}
  </Head>
);

export default Header;

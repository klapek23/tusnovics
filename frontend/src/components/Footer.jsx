import React from 'react';
import TsvcLink from '@/src/components/common/TsvcLink';
import TsvcBottomBar from '@/src/components/TsvcBottomBar';
import TsvcSearchDetailsCardForm from '@/src/forms/OneFieldForms/TsvcSearchDetailsCardForm';
import TsvcNewsletterForm from '@/src/forms/OneFieldForms/TsvcNewsletterForm';

const Footer = ({ leftColumn, footerMenu, copy }) => {
  return (
    <div className="tsvc-footer">
      <div className="tsvc-content-section">
        <div className="container-fluid">
          <div className="row">
            <div className="col-xs-12">
              <div className="tsvc-footer__logo">
                <img
                  src="/static/white-logo.png"
                  alt="Tusnovics logo - white"
                />
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-xs-12 col-sm-6 col-md-6 col-lg-3">
              <div
                className="tsvc-footer__left-column"
                dangerouslySetInnerHTML={{ __html: leftColumn }}
              />
            </div>
            <div className="col-xs-12 col-sm-5 col-sm-offset-1 col-lg-2 col-lg-offset-1">
              <ul className="tsvc-footer__menu">
                {footerMenu
                  ? footerMenu.items.map(({ url, title, ID, classes }) => (
                      <li key={ID}>
                        <TsvcLink
                          as={url}
                          href={url}
                          title={title}
                          target="_blank"
                          className={classes.join(' ')}
                        >
                          {title}
                        </TsvcLink>
                      </li>
                    ))
                  : null}
              </ul>
            </div>
            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-5 col-lg-offset-1">
              <TsvcSearchDetailsCardForm />
              <TsvcNewsletterForm />
            </div>
          </div>
        </div>
      </div>
      <TsvcBottomBar text={copy} />
    </div>
  );
};

export default Footer;

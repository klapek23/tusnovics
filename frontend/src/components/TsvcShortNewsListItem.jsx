import React from 'react';
import createKey from '@/src/utils/createKey';
import TsvcLink from '@/src/components/common/TsvcLink';
import TsvcButton from '@/src/components/common/TsvcButton';

export default function TsvcShortNewsListItem({ link, title, excerpt, date }) {
  return (
    <li className="tsvc-news-list__item">
      <TsvcLink
        as={link.as}
        href={link.href}
        key={createKey(link.title)}
        title={link.title}
        target={link.target}
        className="tsvc-news-list__item-block"
      >
        <div className="row">
          <div className="col-xs-12">
            <div className="tsvc-news-list__item-content">
              <div className="tsvc-news-list__item-title">
                <h4 dangerouslySetInnerHTML={{ __html: title.rendered }} />
              </div>
              <div className="tsvc-news-list__item-date">{date}</div>
              <article dangerouslySetInnerHTML={{ __html: excerpt.rendered }} />

              <TsvcButton color="black">Czytaj więcej</TsvcButton>
            </div>
          </div>
        </div>
      </TsvcLink>
    </li>
  );
}

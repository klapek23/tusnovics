import React from 'react';
import Menu from '@/src/components/TsvcMenu/TsvcMenu';
import Footer from '@/src/components/Footer';

export default function Layout(props) {
  const { children, headerMenu, footer, pathname } = props;
  return (
    <div style={{ overflowX: 'hidden' }}>
      <main>
        <Menu menu={headerMenu} pathname={pathname} />
        {children}
      </main>
      <Footer
        copy={footer.copy}
        leftColumn={footer.leftColumn}
        footerMenu={footer.footerMenu}
      />
    </div>
  );
}

import React from 'react';

const TsvcMainBanner = ({ title, subtitle, image }) => (
  <div className="tsvc-banner tsvc-main-banner">
    <img src={image.url} alt={image.alt} className="tsvc-main-banner__image" />
    <div className="tsvc-main-banner__caption">
      <div className="tsvc-main-banner__caption-center">
        <h1 dangerouslySetInnerHTML={{ __html: title }} />
        {subtitle ? <h2>{subtitle}</h2> : null}
      </div>
    </div>
  </div>
);

export default TsvcMainBanner;

import React, { useEffect, useState } from 'react';
import sampleSize from 'lodash/sampleSize';
import createKey from '@/src/utils/createKey';
import useCurrentWidth from '@/src/hooks/useWidth';
import TsvcLink from '@/src/components/common/TsvcLink';
import TsvcButton from '@/src/components/common/TsvcButton';

const TsvcManufacturers = ({ title, items, url, buttonLabel }) => {
  const [mounted, setMounted] = useState(false);
  const width = useCurrentWidth() || 1200;
  const sizesArr = [
    {
      sizes: {
        from: 0,
        to: 767,
      },
      count: 2,
    },
    {
      sizes: {
        from: 767,
        to: 1199,
      },
      count: 3,
    },
    {
      sizes: {
        from: 1199,
        to: 1399,
      },
      count: 4,
    },
    {
      sizes: {
        from: 1399,
        to: 20000,
      },
      count: 5,
    },
  ];

  useEffect(() => {
    setMounted(true);
  }, []);

  if (mounted) {
    const getItemsCount = () =>
      sizesArr.find(({ sizes }) => width >= sizes.from && width < sizes.to)
        .count;
    const itemsToRender = sampleSize(items, getItemsCount());

    return (
      <div className="tsvc-content tsvc-manufacturers">
        <div className="row">
          <div className="col-xs-12">
            <h3>{title}</h3>
          </div>
        </div>
        <div className="row">
          <div className="col-xs-12">
            <div className="tsvc-manufacturers__container">
              {itemsToRender.map(({ link, logo, t }) => (
                <a
                  className="tsvc-manufacturers__item"
                  href={link.url}
                  title={link.title}
                  key={createKey(t)}
                >
                  <img
                    className="tsvc-manufacturers__item-image"
                    src={logo.url}
                    alt={logo.alt}
                  />
                </a>
              ))}
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-xs-12">
            <TsvcLink as={url} href={url}>
              <TsvcButton>{buttonLabel}</TsvcButton>
            </TsvcLink>
          </div>
        </div>
      </div>
    );
  }

  return null;
};

export default TsvcManufacturers;

import React from 'react';
import { BsArrowRight } from 'react-icons/bs';

const TsvcButton = (
  { children, small, color, type, onClick } = { small: false, type: 'button' },
) => (
  <button
    // eslint-disable-next-line react/button-has-type
    type={type}
    onClick={onClick}
    className={`tsvc-button ${color ? `tsvc-button--${color}` : ''} ${
      small ? 'tsvc-button--small' : ''
    }`}
  >
    <span>{children}</span>
    {small ? null : (
      <em>
        <BsArrowRight />
      </em>
    )}
  </button>
);

export default TsvcButton;

import React from 'react';
import createKey from '@/src/utils/createKey';
import TsvcLink from '@/src/components/common/TsvcLink';
import TsvcButton from '@/src/components/common/TsvcButton';

const TsvcContentBoxes = ({
  title,
  items,
  animated,
  invertedAnimation = false,
  narrow = false,
  renderFooter,
}) => (
  <div
    className={`
      tsvc-content-boxes 
      ${animated ? 'tsvc-content-boxes--animated' : ''} 
      ${
        invertedAnimation
          ? 'tsvc-content-boxes--animated--inverted-animation'
          : ''
      }
      ${narrow ? 'tsvc-content-boxes--narrow' : ''}
      tsvc-content
    `}
  >
    <div className="row">
      <div
        className={`col-xs-12 ${
          narrow && 'col-md-10 col-md-offset-1 col-xl-8 col-xl-offset-2'
        }`}
      >
        {title && <h3>{title}</h3>}
      </div>
    </div>
    <div className="row">
      <div
        className={`col-xs-12 ${
          narrow && 'col-md-10 col-md-offset-1 col-xl-8 col-xl-offset-2'
        }`}
      >
        <div className="tsvc-content-boxes__container">
          {items.map(
            ({
              icon,
              bg_color_1: bgColor1,
              bg_color_2: bgColor2,
              label,
              link,
              title: itemTitle,
            }) => (
              <div
                key={createKey(label ?? itemTitle)}
                className="tsvc-content-boxes__container-box"
              >
                <TsvcLink
                  className="tsvc-content-boxes__item"
                  as={link.as}
                  href={link.href}
                  title={link.title}
                  target={link.target}
                >
                  <article>
                    <div
                      className="tsvc-content-boxes__item-bg"
                      style={{
                        backgroundImage: `linear-gradient(to bottom right, ${bgColor1}, ${bgColor2})`,
                      }}
                    >
                      {icon && <img src={icon.url} alt={icon.alt} />}
                      {itemTitle && <h2>{itemTitle}</h2>}
                    </div>
                    {!narrow && <TsvcButton>{label}</TsvcButton>}
                  </article>
                </TsvcLink>
              </div>
            ),
          )}
        </div>

        {renderFooter && (
          <div className="tsvc-content-boxes__footer">{renderFooter()}</div>
        )}
      </div>
    </div>
  </div>
);

export default TsvcContentBoxes;

import React from 'react';
import createKey from '@/src/utils/createKey';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import TsvcLink from '@/src/components/common/TsvcLink';
import TsvcButton from '@/src/components/common/TsvcButton';
import TsvcPDFSearchForm from '@/src/forms/OneFieldForms/TsvcPDFSearchForm';
import PageSection from '@/src/components/common/PageSection';

const renderLeftColumn = (logo, buttons) => (
  <div className="col-xs-12 col-md-4 col-xl-3">
    <div className="tsvc-manufacturers-list__item--left">
      <div className="tsvc-manufacturers-list__item-image">
        {logo && <img src={logo.url} alt={logo.alt} />}
      </div>
      <div className="tsvc-manufacturers-list__item-links">
        {buttons &&
          buttons.map(({ link, color }) => (
            <TsvcLink
              as={link.url}
              href={link.url}
              title={link.title}
              target={link.target}
              key={createKey(link.title)}
            >
              {link &&
              link.title &&
              link.title.toLowerCase() === 'instrukcja edata' ? (
                <TsvcButton color={color} small>
                  <span style={{ textTransform: 'none' }}>{link.title}</span>
                </TsvcButton>
              ) : (
                <TsvcButton color={color} small>
                  {link.title}
                </TsvcButton>
              )}
            </TsvcLink>
          ))}
      </div>
    </div>
  </div>
);

const renderRightColumn = (title, content, certificates, pdf_search) => (
  <div className="col-xs-12 col-md-8 col-xl-9">
    <div className="tsvc-manufacturers-list__item--right">
      <h2 dangerouslySetInnerHTML={{ __html: title }} />
      <article dangerouslySetInnerHTML={{ __html: content }} />
      {certificates && (
        <>
          <h3>Certyfikaty:</h3>
          <ul className="tsvc-manufacturers-list__item-certificates">
            {certificates.map(({ link }) => (
              <li key={createKey(link.title)}>
                <ArrowForwardIcon className="icon" />
                <TsvcLink as={link.url} title={link.title} target={link.target}>
                  {link.title}
                </TsvcLink>
              </li>
            ))}
          </ul>
        </>
      )}
      {pdf_search && renderPDFSearch(pdf_search)}
    </div>
  </div>
);

const renderPDFSearch = ({ title, placeholder, files }) => {
  return (
    <TsvcPDFSearchForm title={title} placeholder={placeholder} files={files} />
  );
};

const TsvcManufacturersListItem = ({
  title,
  buttons,
  content,
  certificates,
  logo,
  pdf_search,
}) => (
  <li className="tsvc-manufacturers-list__item tsvc-content">
    <PageSection>
      <div className="row">
        <div className="col-xs-12 col-md-9 col-md-offset-1">
          <div className="row">
            {renderLeftColumn(logo, buttons)}
            {renderRightColumn(title, content, certificates, pdf_search)}
          </div>
        </div>
      </div>
    </PageSection>
  </li>
);

export default TsvcManufacturersListItem;

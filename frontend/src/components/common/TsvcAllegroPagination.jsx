import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { BsArrowLeft, BsArrowRight } from 'react-icons/bs';

import TextField from '@material-ui/core/TextField';
import TsvcLink from '@/src/components/common/TsvcLink';

export default function TsvcAllegroPagination({
  pathname,
  query = {},
  currentPage,
  totalPages,
}) {
  const parsedCurrentPage = parseInt(currentPage, 10);
  const [current, setCurrent] = useState(parsedCurrentPage);
  const router = useRouter();

  useEffect(() => {
    setCurrent(parseInt(currentPage, 10));
  }, [currentPage]);

  const prevLink = {
    pathname,
    query: {
      ...query,
      page: parsedCurrentPage - 1,
    },
  };

  const nextLink = {
    pathname,
    query: {
      ...query,
      page: parsedCurrentPage + 1,
    },
  };

  const handleBlur = () => {
    if (current) {
      router.push(
        `${pathname}?${new URLSearchParams({
          ...query,
          page: current,
        }).toString()}`,
        `${pathname}?${new URLSearchParams({
          ...query,
          page: current,
        }).toString()}`,
      );
      window.scrollTo(0, 0);
    }
  };

  const handleCurrentPageChange = (e) => {
    setCurrent(e.target.value);
  };

  const handleKeyPress = (e) => {
    if (e.key === 'Enter') {
      e.target.blur();
    }
  };

  return (
    <div className="tsvc-pagination">
      {parsedCurrentPage > 1 && (
        <TsvcLink
          className="prev"
          title="Poprzednia strona"
          href={prevLink}
          as={prevLink}
        >
          <BsArrowLeft />
        </TsvcLink>
      )}
      <TextField
        value={current}
        onChange={handleCurrentPageChange}
        onBlur={handleBlur}
        onKeyPress={handleKeyPress}
      />{' '}
      <span>z {totalPages}</span>
      {parsedCurrentPage < totalPages && (
        <TsvcLink
          className="next"
          title="Następna strona"
          href={nextLink}
          as={nextLink}
        >
          <BsArrowRight />
        </TsvcLink>
      )}
    </div>
  );
}

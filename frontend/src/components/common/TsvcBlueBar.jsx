import React from 'react';
import PageSection from '@/src/components/common/PageSection';
import TsvcLink from '@/src/components/common/TsvcLink';
import TsvcButton from '@/src/components/common/TsvcButton';

export default function TsvcBlueBar({ link, text }) {
  return (
    <div className="tsvc-blue-bar">
      <PageSection smallPaddingBottom smallPaddingTop>
        <article className="tsvc-blue-bar__content">
          <h4>{text}</h4>
          <TsvcLink as={link.url} href={link.url} target={link.target}>
            <TsvcButton color="white">{link.title}</TsvcButton>
          </TsvcLink>
        </article>
      </PageSection>
    </div>
  );
}

import React from 'react';

const TsvcContent03 = ({ children, title }) => (
  <div className="tsvc-content-03 tsvc-content">
    <div className="row">
      <div className="col-xs-12 col-xl-10 col-xl-offset-1">
        <article>
          <h3>{title}</h3>
          {children}
        </article>
      </div>
    </div>
  </div>
);

export default TsvcContent03;

import React from 'react';
import createKey from '@/src/utils/createKey';
import ArrowRightAltIcon from '@material-ui/icons/ArrowRightAlt';

export default function TsvcFilesList({
  title,
  subtitle,
  items,
  noResultContent = () => null,
  downloadLabel,
}) {
  return (
    <div className="tsvc-files-list tsvc-content">
      <section className="tsvc-files-list__header">
        <h3>{title}</h3>
        <p>
          {subtitle} <strong>{items.length}</strong>
        </p>
      </section>
      {items.length > 0 ? (
        <ul className="tsvc-files-list__list">
          {items &&
            items.map(({ name, path }) => (
              <li key={createKey(name)}>
                <a href={path} download={name}>
                  <img src="/static/images/pdf-icon.png" alt="pdf icon" />
                </a>
                <span>{name}</span>
                <ArrowRightAltIcon />
                <a href={path} download={name}>
                  {downloadLabel}
                </a>
              </li>
            ))}
        </ul>
      ) : (
        noResultContent()
      )}
    </div>
  );
}

import React from 'react';
import { BsArrowLeft, BsArrowRight } from 'react-icons/bs';
import createKey from '@/src/utils/createKey';
import TsvcLink from '@/src/components/common/TsvcLink';

export default function TsvcPagination({
  pathname,
  query = {},
  currentPage,
  totalPages,
}) {
  const parsedCurrentPage = parseInt(currentPage, 10);

  const prevLink = {
    pathname,
    query: {
      ...query,
      page: parsedCurrentPage - 1,
    },
  };

  const nextLink = {
    pathname,
    query: {
      ...query,
      page: parsedCurrentPage + 1,
    },
  };

  return (
    <div className="tsvc-pagination">
      {parsedCurrentPage > 1 && (
        <TsvcLink
          className="prev"
          title="Poprzednia strona"
          href={prevLink}
          as={prevLink}
        >
          <BsArrowLeft />
        </TsvcLink>
      )}

      <ul>
        {Array.from(Array(totalPages), (e, i) => {
          const link = {
            pathname,
            query: {
              page: i + 1,
              ...query,
            },
          };
          return (
            <li key={createKey(pathname + i)}>
              <TsvcLink
                className={`${parsedCurrentPage === i + 1 ? 'active' : ''}`}
                title={`Strona ${i + 1}`}
                href={link}
                as={link}
              >
                {i + 1}
              </TsvcLink>
            </li>
          );
        })}
      </ul>

      {parsedCurrentPage < totalPages && (
        <TsvcLink
          className="next"
          title="Następna strona"
          href={nextLink}
          as={nextLink}
        >
          <BsArrowRight />
        </TsvcLink>
      )}
    </div>
  );
}

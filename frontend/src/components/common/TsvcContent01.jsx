import React from 'react';

import TsvcLink from '@/src/components/common/TsvcLink';
import TsvcButton from '@/src/components/common/TsvcButton';

const TsvcContent01 = ({ children, title, subtitle, image, link, extra }) => (
  <div className="tsvc-content-01 tsvc-content">
    <div className="row">
      <div className="col-xs-12 col-md-7 col-lg-6 col-lg-offset-1">
        <article>
          <h3>{title}</h3>
          <h4>{subtitle}</h4>
          <h5>{extra}</h5>
          <div>{children}</div>
          <TsvcLink as={link} href={link} target={link.target}>
            <TsvcButton>Zobacz więcej</TsvcButton>
          </TsvcLink>
        </article>
      </div>
      <div className="col-xs-12 col-md-5 col-lg-4">
        <img src={image.url} alt={image.alt} />
      </div>
    </div>
  </div>
);

export default TsvcContent01;

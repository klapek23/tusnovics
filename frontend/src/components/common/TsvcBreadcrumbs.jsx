import React from 'react';
import { BsArrowRight } from 'react-icons/bs';

import createKey from '@/src/utils/createKey';
import TsvcLink from '@/src/components/common/TsvcLink';

const TsvcBreadcrumbs = ({ items }) => {
  const createLinkContent = (as) => {
    const asArr = as.split('/').filter((item) => !!item);
    const asString = asArr[asArr.length - 1].replace(/-/g, ' ');
    const n = asString.indexOf('?');
    return n > 0 ? asString.substring(0, n) : asString;
  };
  const breadcrumbsItems = items.map(({ href, as }) => (
    <li key={createKey(as)}>
      <BsArrowRight />
      <TsvcLink href={`/${href}`} as={as}>
        {createLinkContent(as)}
      </TsvcLink>
    </li>
  ));
  return (
    <div className="tsvc-breadcrumbs">
      <div className="row">
        <div className="col-xs-12">
          <ul>
            <li key="index">
              <BsArrowRight />
              <TsvcLink href="/" as="/">
                Tusnovics
              </TsvcLink>
            </li>
            {breadcrumbsItems}
          </ul>
        </div>
      </div>
    </div>
  );
};

export default TsvcBreadcrumbs;

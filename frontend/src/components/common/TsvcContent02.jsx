import React from 'react';

import TsvcLink from '@/src/components/common/TsvcLink';
import TsvcButton from '@/src/components/common/TsvcButton';

const TsvcContent02 = ({ children, title, link }) => (
  <div className="tsvc-content-02 tsvc-content">
    <div className="row">
      <div className="col-xs-12 col-lg-9 col-lg-offset-2">
        <div className="tsvc-content-02__anchor">
          <div className="tsvc-content-02__overlay" />
          <article>
            <h3>{title}</h3>
            <div>{children}</div>
            <TsvcLink as={link.url} href={link.url} target={link.target}>
              <TsvcButton color="white">{link.title}</TsvcButton>
            </TsvcLink>
          </article>
        </div>
      </div>
    </div>
  </div>
);

export default TsvcContent02;

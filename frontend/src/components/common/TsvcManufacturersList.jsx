import React from 'react';
import createKey from '@/src/utils/createKey';
import TsvcManufacturersListItem from '@/src/components/common/TsvcManufacturersListItem';

const TsvcManufacturersList = ({ items }) => {
  return (
    <section className="tsvc-manufacturers-list">
      <ul>
        {items.map((item) => (
          <TsvcManufacturersListItem {...item} key={createKey(item.content)} />
        ))}
      </ul>
    </section>
  );
};

export default TsvcManufacturersList;

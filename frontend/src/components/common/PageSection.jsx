import React from 'react';

export default function PageSection({
  children,
  noPadding = false,
  smallPaddingTop = false,
  smallPaddingBottom = false,
  noPaddingBottom = false,
  noVerticalPadding = false,
  className = '',
  bg = 'transparent',
}) {
  return (
    <div style={{ backgroundColor: bg }}>
      <section
        className={`
        tsvc-content-section 
        ${noPadding ? 'tsvc-content-section--no-padding' : ''}
        ${smallPaddingTop ? 'tsvc-content-section--small-padding-top' : ''}
        ${
          smallPaddingBottom ? 'tsvc-content-section--small-padding-bottom' : ''
        }
        ${noPaddingBottom ? 'tsvc-content-section--no-padding-bottom' : ''}
        ${noVerticalPadding ? 'tsvc-content-section--no-vertical-padding' : ''}
        ${className}
      `}
      >
        <div className="container-fluid">{children}</div>
      </section>
    </div>
  );
}

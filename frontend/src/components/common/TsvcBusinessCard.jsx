import React from 'react';
import createKey from '@/src/utils/createKey';

const TsvcBusinessCard = ({ items }) => (
  <div className="tsvc-business-card tsvc-content">
    {items.map(({ label, sublabel, info }) => (
      <section className="tsvc-business-card__column" key={createKey(label)}>
        <h4>{label}</h4>
        <span className="tsvc-business-card__sublabel">{sublabel}</span>
        <article dangerouslySetInnerHTML={{ __html: info }} />
      </section>
    ))}
  </div>
);

export default TsvcBusinessCard;

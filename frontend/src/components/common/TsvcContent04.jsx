import React from 'react';
import createKey from '@/src/utils/createKey';
import TsvcButton from '@/src/components/common/TsvcButton';
import TsvcLink from '@/src/components/common/TsvcLink';

const TsvcContent04 = ({
  title,
  content,
  bg_color_1,
  bg_color_2,
  buttons,
  link,
}) => (
  <div className="tsvc-content-04 tsvc-content">
    <div className="row">
      <div className="col-xs-12 col-sm-4 col-md-4 col-lg-3">
        <div
          className="tsvc-content-04__item-bg"
          style={{
            backgroundImage: `linear-gradient(to bottom right, ${bg_color_1}, ${bg_color_2})`,
          }}
        >
          <h2>{title}</h2>
        </div>
        <div className="tsvc-content-04__buttons">
          {buttons &&
            buttons.map(({ link: buttonLink, color }) => (
              <TsvcLink
                as={buttonLink.url}
                href={buttonLink.url}
                title={buttonLink.title}
                target={buttonLink.target}
                key={createKey(buttonLink.title)}
              >
                <TsvcButton color={color} small>
                  {buttonLink.title}
                </TsvcButton>
              </TsvcLink>
            ))}
        </div>
      </div>
      <div className="col-xs-12 col-sm-8 col-md-8 col-lg-9">
        <article className="tsvc-content-04__info">
          <h3>{title}</h3>
          <section dangerouslySetInnerHTML={{ __html: content }} />
          {link && (
            <TsvcLink
              as={link.as}
              href={link.href}
              title={link.title}
              target={link.target}
            >
              <TsvcButton>{link.title}</TsvcButton>
            </TsvcLink>
          )}
        </article>
      </div>
    </div>
  </div>
);

export default TsvcContent04;

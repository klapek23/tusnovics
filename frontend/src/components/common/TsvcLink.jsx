import React from 'react';
import Link from 'next/link';

import config from '@/config';

const isRelative = (url) => url.startsWith('/');
const isFile = (url) =>
  url.startsWith('/wp-content') || url.startsWith(`${config.wpUrl}/wp-content`);
const isInternal = (url) => url.startsWith(config.wpUrl) || isRelative(url);

const removeWpPrefix = (url) => url.replace(config.wpUrl, '');
const removeTrailingSlash = (url) => url.replace(/\/$/, '');

// href = path to page file
// as = url address in browser bar

const TsvcLink = ({ children, as, href, title, className = '', target }) => {
  if (typeof href === 'string' && !isFile(href)) {
    const url = removeTrailingSlash(as);
    if (isRelative(as)) {
      return (
        <Link href={href} as={url} title={title} target={target || undefined}>
          <span className={`tsvc-link ${className}`}>{children}</span>
        </Link>
      );
    }

    if (isInternal(href)) {
      return (
        <Link
          href={removeWpPrefix(href)}
          as={removeWpPrefix(url)}
          title={title}
          target={target || undefined}
        >
          <span className={`tsvc-link ${className}`}>{children}</span>
        </Link>
      );
    }
  } else if (typeof href === 'string' && isFile(href)) {
    if (isRelative(href)) {
      return (
        <a
          className={`tsvc-link ${className}`}
          href={`${config.wpUrl}${href}`}
          title={title}
          target={target || undefined}
        >
          {children}
        </a>
      );
    }

    return (
      <a
        className={`tsvc-link ${className}`}
        href={href}
        title={title}
        target={target || undefined}
      >
        {children}
      </a>
    );
  } else if (typeof href === 'object' && href.pathname && href.query) {
    return (
      <Link href={href} as={as} title={title} target={target || undefined}>
        <span className={`tsvc-link ${className}`}>{children}</span>
      </Link>
    );
  }

  return (
    <a
      className={`tsvc-link ${className}`}
      href={as}
      title={title}
      target={target || undefined}
    >
      {children}
    </a>
  );
};

export default TsvcLink;

import React, { useEffect, useState } from 'react';
import { motion, AnimatePresence } from 'framer-motion';
import { useRouter } from 'next/router';

export default function TsvcSpinner() {
  const [showLoader, setShowLoader] = useState(false);
  const router = useRouter();
  function handleRouteChange() {
    setShowLoader(true);
  }

  function handleRouteChangeComplete() {
    setShowLoader(false);
  }

  function handleRouteChangeError() {
    setShowLoader(false);
  }

  useEffect(() => {
    router.events.on('routeChangeStart', handleRouteChange);
    router.events.on('routeChangeComplete', handleRouteChangeComplete);
    router.events.on('routeChangeError', handleRouteChangeError);

    return () => {
      router.events.off('routeChangeStart', handleRouteChange);
      router.events.off('routeChangeComplete', handleRouteChangeComplete);
      router.events.off('routeChangeError', handleRouteChangeError);
    };
  }, [router]);

  return (
    <AnimatePresence>
      {showLoader && (
        <motion.div
          className="spinner-wrapper"
          initial={{ opacity: 0 }}
          animate={{ opacity: 1 }}
          exit={{ opacity: 0 }}
        >
          <div className="spinner-bg" />
          <div className="spinner">
            <div className="double-bounce1" />
            <div className="double-bounce2" />
          </div>
        </motion.div>
      )}
    </AnimatePresence>
  );
}

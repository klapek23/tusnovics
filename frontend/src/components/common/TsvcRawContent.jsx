import React from 'react';

const TsvcRawContent = ({ children, title }) => (
  <div className="tsvc-raw-content tsvc-content">
    <article>
      {title && <h2 dangerouslySetInnerHTML={{ __html: title }} />}
      {children}
    </article>
  </div>
);

export default TsvcRawContent;

import React from 'react';

import createKey from '@/src/utils/createKey';
import TsvcNewsListItem from '@/src/components/TsvcNewsListItem';
import TsvcPagination from '@/src/components/common/TsvcPagination';
import PageSection from '@/src/components/common/PageSection';

export default function TsvcNewsList({
  title,
  items,
  currentPage,
  totalPages,
  pagination = false,
}) {
  const aktualnosciPathname = '/aktualnosci';

  return (
    <section className="tsvc-news-list">
      <PageSection smallPaddingTop noPaddingBottom bg="#f1f6fb">
        <div className="row">
          <div className="col-xs-12 col-sm-9 col-sm-offset-1 col-md-8">
            <h3>{title}</h3>
          </div>
        </div>
      </PageSection>
      <ul>
        {items.map((item) => (
          <TsvcNewsListItem {...item} key={createKey(item.title.rendered)} />
        ))}
      </ul>

      {pagination && totalPages > 1 && (
        <PageSection smallPaddingTop>
          <div className="row">
            <div className="col-xs-12 col-lg-10 col-lg-offset-1">
              <TsvcPagination
                pathname={aktualnosciPathname}
                currentPage={currentPage}
                totalPages={totalPages}
              />
            </div>
          </div>
        </PageSection>
      )}
    </section>
  );
}

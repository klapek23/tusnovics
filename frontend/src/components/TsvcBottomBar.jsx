import React from 'react';
import PageSection from './common/PageSection';

export default function TsvcBottomBar({ text }) {
  return (
    <div className="tsvc-bottom-bar">
      <PageSection noVerticalPadding>
        <article className="tsvc-bottom-bar__text">{text}</article>
      </PageSection>
    </div>
  );
}

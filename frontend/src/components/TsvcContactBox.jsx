import React from 'react';
import TsvcLink from '@/src/components/common/TsvcLink';
import TsvcButton from '@/src/components/common/TsvcButton';

const TsvcContactBox = ({ children, title, subtitle, link }) => (
  <div className="tsvc-contact-box tsvc-content">
    <div className="row">
      <div className="col-xs-12">
        <article>
          {title && <h3>{title}</h3>}
          <h4>{subtitle}</h4>
          <div>{children}</div>
          {link && (
            <TsvcLink as={link.url} href={link.url} title={link.title}>
              <TsvcButton>{link.title}</TsvcButton>
            </TsvcLink>
          )}
        </article>
      </div>
    </div>
  </div>
);

export default TsvcContactBox;

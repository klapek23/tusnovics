import React from 'react';

import TsvcProtectedFilesList from '@/src/components/TsvcProtectedFilesList';

export default function TsvcMSDSResults({ items, loggedIn, onLoginClick }) {
  const mappedItems = items.map(({ name, path }) => ({
    name,
    path: `/${path}`,
  }));

  const handleLoginClick = (path, name) => {
    if (onLoginClick) {
      onLoginClick(path, name);
    }
  };

  return (
    <div id="msds-files-list">
      <TsvcProtectedFilesList
        title="Wyniki wyszukiwania"
        subtitle="Liczba znalezionych wyników wyszukiwania:"
        downloadLabel="POBIERZ"
        loggedIn={loggedIn}
        onLoginClick={handleLoginClick}
        apiEndpoint="/api/msds"
        noResultContent={() => (
          <p>
            Nie znaleziono dokumentu.{' '}
            <strong>
              Napisz do nas:{' '}
              <a
                href="mailto:marketing@tusnovics.pl"
                title="marketing@tusnovics.pl"
              >
                marketing@tusnovics.pl
              </a>
            </strong>
          </p>
        )}
        items={mappedItems}
      />
    </div>
  );
}

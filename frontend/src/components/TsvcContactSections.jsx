import React from 'react';

import createKey from '@/src/utils/createKey';

export default function TsvcContactSections({ title, items, smallTitle }) {
  return (
    <div className="tsvc-contact-sections tsvc-content">
      {smallTitle ? (
        <h4 className="title">{title}</h4>
      ) : (
        <h3 className="title">{title}</h3>
      )}
      <div className="tsvc-contact-sections__list">
        {items.map(({ label, content, double }) => (
          <section
            className={`tsvc-contact-sections__item ${
              double ? 'tsvc-contact-sections__item--double' : ''
            }`}
            key={createKey(label)}
          >
            <h4>{label}</h4>
            <article dangerouslySetInnerHTML={{ __html: content }} />
          </section>
        ))}
      </div>
    </div>
  );
}

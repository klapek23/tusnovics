import React from 'react';

import createKey from '@/src/utils/createKey';
import PageSection from '@/src/components/common/PageSection';

export default function TsvcAboutSections({ sections }) {
  return (
    <div className="tsvc-about-sections">
      {sections.map(({ image, content }) => (
        <div
          key={createKey(content)}
          className="tsvc-about-sections__item tsvc-content"
        >
          <PageSection>
            <div className="row">
              <div className="col-xs-12 col-sm-5 col-md-4 col-lg-3">
                <div className="tsvc-about-sections__item-image">
                  <img src={image.url} alt={image.alt} />
                </div>
              </div>
              <div className="col-xs-12 col-sm-7 col-md-8 col-lg-9">
                <div className="tsvc-about-sections__item-content">
                  <article dangerouslySetInnerHTML={{ __html: content }} />
                </div>
              </div>
            </div>
          </PageSection>
        </div>
      ))}
    </div>
  );
}

import React from 'react';
import AwesomeSlider from 'react-awesome-slider';
import withAutoplay from 'react-awesome-slider/dist/autoplay';

const AutoplaySlider = withAutoplay(AwesomeSlider);

const TsvcSlider = ({ children }) => (
  <div className="tsvc-slider tsvc-banner">
    <AutoplaySlider organicArrows bullets={false} fillParent>
      {children.map((item) => (
        <div className="tsvc-slider__item" key={item}>
          {item}
        </div>
      ))}
    </AutoplaySlider>
  </div>
);

export default TsvcSlider;

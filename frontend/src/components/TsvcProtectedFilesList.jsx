import React from 'react';
import axios from 'axios';
import ArrowRightAltIcon from '@material-ui/icons/ArrowRightAlt';
import createKey from '@/src/utils/createKey';

export default function TsvcFilesList({
  title,
  subtitle,
  items,
  noResultContent = () => null,
  downloadLabel,
  loggedIn = true,
  onLoginClick,
  apiEndpoint = '',
}) {
  const handleNotLoggedLinkClick = (path, name) => {
    if (onLoginClick) {
      onLoginClick(path, name);
    }
  };

  const handleDownloadClick = async (path, name) => {
    if (!loggedIn) {
      handleNotLoggedLinkClick(path, name);
    } else {
      const response = await axios.get(`${apiEndpoint}?name=${name}`, {
        responseType: 'blob',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      });
      const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement('a');
      link.href = url;
      link.setAttribute('download', name);
      document.body.appendChild(link);
      link.click();
      link.remove();
    }
  };

  return (
    <div className="tsvc-files-list tsvc-content">
      <section className="tsvc-files-list__header">
        <h3>{title}</h3>
        <p>
          {subtitle} <strong>{items.length}</strong>
        </p>
      </section>
      {items.length > 0 ? (
        <ul className="tsvc-files-list__list">
          {items &&
            items.map(({ name, path }) => (
              <li key={createKey(path)}>
                <button
                  type="button"
                  onClick={() => handleDownloadClick(path, name)}
                >
                  <img src="/static/images/pdf-icon.png" alt="pdf icon" />
                </button>
                <span>{name}</span>
                <ArrowRightAltIcon />
                <button
                  type="button"
                  onClick={() => handleDownloadClick(path, name)}
                >
                  {downloadLabel}
                </button>
              </li>
            ))}
        </ul>
      ) : (
        noResultContent()
      )}
    </div>
  );
}

import React, { useState } from 'react';
import { motion, AnimatePresence } from 'framer-motion';

import style from './tsvc-seo-content.module.scss';
import { BsArrowDown, BsArrowUp } from 'react-icons/bs';

export function getSeoContentFromPage(page) {
  return page?.acf?.seo_content?.full
    ? {
        intro: page?.acf?.seo_content.intro,
        content: page?.acf?.seo_content.full,
      }
    : null;
}

export function getCategorySeoContent(category) {
  return category?.category_seo_content?.full
    ? {
        intro: category?.category_seo_content.intro,
        content: category?.category_seo_content.full,
      }
    : null
}

export default function TsvcSeoContent({ intro, content }) {
  const [open, setOpen] = useState(false);

  function handleButtonClick() {
    setOpen(!open);
  }

  return (
    <div className={style.root}>
      <h2 className={style.intro}>{intro}</h2>

      <AnimatePresence>
        <motion.div
          initial={{
            height: 0,
            opacity: 0,
          }}
          animate={{
            height: open ? 'auto' : 0,
            opacity: open ? 1 : 0,
          }}
          transition={{ ease: 'easeOut', duration: 0.3 }}
          className={style.content}
          dangerouslySetInnerHTML={{ __html: content }}
        />
      </AnimatePresence>

      <button className={style.button} onClick={handleButtonClick}>
        {open ? (
          <>
            <span>Mniej</span>
            <BsArrowUp className={style.button__icon} />
          </>
        ) : (
          <>
            <span>Zobacz więcej</span>
            <BsArrowDown className={style.button__icon} />
          </>
        )}
      </button>
    </div>
  );
}

import WPAPI from 'wpapi';
import config from '@/config';

const wp = new WPAPI({ endpoint: config.apiUrl });

// This route is copied from the plugin: wordpress/wp-content/plugins/wp-rest-api-v2-menus/wp-rest-api-v2-menus.php
wp.menus = wp.registerRoute('menus/v1', '/menus/(?P<id>[a-zA-Z(-]+)');
wp.cookiesSettings = wp.registerRoute('cookies-by-klapek23/v1', '/settings');
wp.login = wp.registerRoute('custom', '/login');
wp.themeOptions = wp.registerRoute('custom', '/theme-options');
wp.offer = wp.registerRoute('wp/v2', 'oferta/(?P<id>)');
wp.manufacturers = wp.registerRoute('wp/v2', 'manufacturers/(?P<id>)');
wp.bb_categories = wp.registerRoute('wp/v2', 'bb_categories/(?P<id>)');
wp.cmoiw = wp.registerRoute('wp/v2', 'cmoiw/(?P<id>)');

export default wp;

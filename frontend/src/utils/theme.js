import { createTheme } from '@material-ui/core/styles';
import { red } from '@material-ui/core/colors';

// Create a theme instance.
const theme = createTheme({
  typography: {
    fontFamily: ['Lato'],
    fontWeight: 400,
    fontSize: 16,
  },
  palette: {
    primary: {
      main: '#4eb6ad',
    },
    secondary: {
      main: '#7ed3eb',
    },
    error: {
      main: red.A400,
    },
    background: {
      default: '#fff',
    },
  },
});

export default theme;

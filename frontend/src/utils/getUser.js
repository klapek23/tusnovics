import { getIronSession } from 'iron-session';
import axios from 'axios';
import sessionOptions from '@/src/backend/utils/sessionOptions';

export default async function getUser({ req, res }) {
  const ssr = Boolean(req);

  if (ssr) {
    const session = await getIronSession(req, res, sessionOptions);
    return session.user;
  }

  const response = await axios.get('/api/auth/user');
  return response.data.user;
}

export const phoneRegex = /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s./0-9]*$/g;
export const emailRegex = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g;

export const isRequiredValid = (value) => !!value;

export const isEmail = (value) => emailRegex.test(value);

export const isPhone = (value) => phoneRegex.test(value);

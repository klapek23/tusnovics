import { escapeRegExp as regEscape } from 'lodash';

export default function escapeRegExp(str) {
  return regEscape(str);
}

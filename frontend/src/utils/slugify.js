import { kebabCase } from 'lodash';

function slugify(str) {
  return kebabCase(str);
}

export default slugify;

const errorMessages = {
  required: 'To pole jest wymagane',
  invalidEmail: 'Niepoprawny adres e-mail',
  invalidPassword: 'Niepoprawne hasło',
  invalidPhoneNumber: 'Niepoprawny numer telefonu',
  invalidCaptcha: 'Niepoprawna wartość captcha',
};

export default errorMessages;

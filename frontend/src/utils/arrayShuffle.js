import { shuffle as arrShuffle } from 'lodash';

export default function shuffle(array) {
  return arrShuffle(array);
}

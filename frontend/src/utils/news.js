export function convertDate(date, language) {
  return new Date(date).toLocaleDateString(language, {
    day: 'numeric',
    month: 'long',
    year: 'numeric',
  });
}

export const convertWPNews = (news) =>
  news.map(({ link, _embedded, ...item }, lang = 'pl') => ({
    ...item,
    link: {
      as: link,
      href: '/aktualnosci/[slug]',
      title: item.title.rendered,
    },
    image: {
      url:
        _embedded['wp:featuredmedia'] && _embedded['wp:featuredmedia'][0]
          ? _embedded['wp:featuredmedia'][0].source_url
          : undefined,
      alt:
        _embedded['wp:featuredmedia'] && _embedded['wp:featuredmedia'][0]
          ? _embedded['wp:featuredmedia'][0].alt_text
          : undefined,
    },
    date: convertDate(item.date, lang),
  }));

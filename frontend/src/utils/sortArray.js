import { sortBy } from 'lodash';

export default function sortArray(arr, defaultOrder = 'asc') {
  return sortBy(arr, defaultOrder);
}

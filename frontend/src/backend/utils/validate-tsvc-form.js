import errorMessages from '@/src/utils/errorMessages';
import { isEmail, isPhone } from '@/src/utils/validators';

const validateForm = (form) => {
  const errors = {};
  const formFields = Object.keys(form);
  formFields.forEach((field) => {
    const value = form[field];
    switch (field) {
      case 'name':
      case 'company':
      case 'message':
      case 'device':
      case 'serial_number':
      case 'rodo':
        if (!value) {
          errors[field] = errorMessages.required;
        }
        break;
      case 'email':
        if (!value) {
          errors[field] = errorMessages.required;
        }
        if (!isEmail(value)) {
          errors[field] = errorMessages.invalidEmail;
        }
        break;
      case 'phone':
        if (!value) {
          errors[field] = errorMessages.required;
        }
        if (!isPhone(value)) {
          errors[field] = errorMessages.invalidPhoneNumber;
        }
        break;
      case 'service_type':
        if (!Object.values(value).find((item) => item)) {
          errors[field] = errorMessages.required;
        }
        break;
      default:
        break;
    }
  });

  return Object.keys(errors).length > 0 ? errors : null;
};

export default validateForm;

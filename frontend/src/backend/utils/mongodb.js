const mongoClient = require('mongodb').MongoClient;

const connect = () => {
  const url = 'mongodb://klapek23:TakV8n33Cmmx@mongodb:27017';
  const dbName = 'tusnovics';

  return new Promise((resolve, reject) => {
    mongoClient.connect(
      url,
      {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      },
      (err, client) => {
        if (err) reject(err);

        const db = client.db(dbName);
        resolve({ db, client });
      },
    );
  });
};

export default connect;

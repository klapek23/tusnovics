import sortArray from '@/src/utils/sortArray';

export default function sortWithPriorities(items, priorities, defaultOrder) {
  const itemsGroupedByManufacturer = items.reduce((previous, current) => {
    const { manufacturer } = current;
    if (previous[manufacturer]) {
      return {
        ...previous,
        [manufacturer]: [...previous[manufacturer], current],
      };
    }
    return { ...previous, [manufacturer]: [current] };
  }, {});

  const orderedItemsGroupedByManufacturer = Object.keys(
    itemsGroupedByManufacturer,
  )
    .sort((a, b) => {
      const parsedA = a.trim().toLocaleLowerCase();
      const parsedB = b.trim().toLocaleLowerCase();
      let aIndex = priorities.indexOf(parsedA);
      let bIndex = priorities.indexOf(parsedB);
      const isAInArray = aIndex !== -1;
      const isBInArray = bIndex !== -1;

      if (isAInArray && !isBInArray) {
        return -1;
      }
      if (isBInArray && !isAInArray) {
        return 1;
      }
      if (!isAInArray && !isBInArray) {
        return 0;
      }
      if (isAInArray && isBInArray) {
        aIndex = priorities.indexOf(a.trim().toLocaleLowerCase());
        bIndex = priorities.indexOf(b.trim().toLocaleLowerCase());

        return aIndex < bIndex ? -1 : 1;
      }

      return -1;
    })
    .reduce((acc, key) => {
      acc[key] = itemsGroupedByManufacturer[key];
      return acc;
    }, {});

  let unorderedItems = [];
  const sortedPriorities = Object.keys(
    orderedItemsGroupedByManufacturer,
  ).reduce((previous, current) => {
    const isInArray = priorities.includes(current.trim().toLocaleLowerCase());
    if (isInArray) {
      return [...previous, ...orderedItemsGroupedByManufacturer[current]];
    }
    unorderedItems = [
      ...unorderedItems,
      ...orderedItemsGroupedByManufacturer[current],
    ];
    return [...previous];
  }, []);

  const sortedNotPriorities = sortArray(unorderedItems, defaultOrder);

  return [...sortedPriorities, ...sortedNotPriorities];
}

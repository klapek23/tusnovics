import errorMessages from '@/src/utils/errorMessages';
import { isEmail } from '@/src/utils/validators';

const validateCaptcha = async (captchaCode, serverCaptcha) => {
  if (!captchaCode) {
    return errorMessages.required;
  }

  if (Number(captchaCode) !== Number(serverCaptcha)) {
    return errorMessages.invalidCaptcha;
  }

  return null;
};

const validateForm = async (form, serverCaptcha) => {
  const errors = {};
  const formFields = Object.keys(form);

  const captchaError = await validateCaptcha(form.captcha, serverCaptcha);

  if (captchaError) {
    errors.captcha = captchaError;
  }

  formFields.forEach((field) => {
    const value = form[field];
    switch (field) {
      case 'name':
      case 'company':
      case 'message':
      case 'rodo':
        if (!value) {
          errors[field] = errorMessages.required;
        }
        break;
      case 'email':
        if (!value) {
          errors[field] = errorMessages.required;
        }
        if (!isEmail(value)) {
          errors[field] = errorMessages.invalidEmail;
        }
        break;
      case field.startsWith('inquiry_'):
        if (!value) {
          errors[field] = errorMessages.required;
        }
        break;
      default:
        break;
    }
  });

  return Object.keys(errors).length > 0 ? errors : null;
};

export default validateForm;

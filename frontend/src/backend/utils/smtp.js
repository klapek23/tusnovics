import nodemailer from 'nodemailer';
import ejs from 'ejs';

export const createTransporter = (smtp) =>
  nodemailer.createTransport({
    host: smtp.host,
    port: smtp.port,
    auth: {
      user: smtp.smtpEmail,
      pass: smtp.smtpPass,
    },
  });

export const sendEmail = ({
  sendFromEmail,
  sendFromName,
  sendToEmail,
  subject,
  formData,
  templatePath,
  transporter,
  attachments,
}) => {
  const from =
    sendFromName && sendFromEmail
      ? `${sendFromName} <${sendFromEmail}>`
      : `${sendFromName || sendFromEmail}`;
  ejs.renderFile(templatePath, formData, (err, html) => {
    if (err) {
      console.log(err);
      return false;
    }

    const message = {
      from,
      to: `${sendToEmail}`,
      subject,
      html,
      replyTo: from,
    };

    if (attachments) {
      message.attachments = attachments;
    }

    return new Promise((resolve, reject) => {
      transporter.sendMail(message, (error, info) => {
        if (error) {
          console.log(error);
        } else {
          console.log(info);
        }
        return error ? reject(error) : resolve(info);
      });
    });
  });
};

export const handleValidatedForm = async (
  res,
  form,
  createSubject,
  emailTemplate,
  errors,
  recipients,
  attachments,
  smtp,
  callback,
) => {
  if (!errors) {
    try {
      const transporter = createTransporter(smtp);
      sendEmail({
        sendFromEmail: smtp.smtpEmail,
        sendFromName: smtp.smtpName,
        sendToEmail: recipients,
        subject: createSubject(form.email),
        formData: form,
        templatePath: emailTemplate,
        transporter,
        attachments,
      });

      if (callback) {
        callback(res, transporter);
      } else {
        res.json({ success: true });
      }
    } catch (e) {
      console.log(e);
      res.json({ success: false, errors: [e] });
    }
  } else {
    res.json({ success: false, errors });
  }
};

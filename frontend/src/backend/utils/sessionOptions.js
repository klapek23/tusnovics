import config from '@/config';

const sessionOptions = {
  password: config.auth.cookie.password,
  cookieName: config.auth.cookie.name,
  cookieOptions: {
    secure: config.auth.cookie.secure,
    maxAge: config.auth.cookie.maxAge,
  },
};

export default sessionOptions;

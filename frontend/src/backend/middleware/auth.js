export default async function authMiddleware(req, res, next) {
  if (req.session && req.session.user) {
    next();
  } else {
    res.status(403);
    res.send('Unauthorized');
  }
}

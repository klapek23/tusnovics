import formidable from 'formidable';

const form = formidable({ multiples: true }); // multiples means req.files will be an array
form.uploadDir = './static/uploads';
form.keepExtensions = true;

export default async function parseMultipartForm(req, res, next) {
  const contentType = req.headers['content-type'];
  if (contentType && contentType.startsWith('multipart/form-data')) {
    form.parse(req, (err, fields, files) => {
      if (!err) {
        req.body = fields; // sets the body field in the request object
        req.files = files; // sets the files field in the request object
      } else {
        console.log(err);
      }
      next(); // continues to the next middleware or to the route
    });
  } else {
    next();
  }
}

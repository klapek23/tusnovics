import getCrmMaterialsOnDemandFormFields from './models/crm-materials-on-demand-form';
import TsvcForm from './TsvcForm';

const TsvcCrmMaterialsOnDemand = () => {
  const formFields = getCrmMaterialsOnDemandFormFields();

  return (
    <TsvcForm
      title="Wyślij zapytanie"
      fields={formFields}
      url="/api/materials-on-demand-form"
    />
  );
};

export default TsvcCrmMaterialsOnDemand;

import React from 'react';
import * as yup from 'yup';
import { emailRegex } from '@/src/utils/validators';
import errorMessages from '@/src/utils/errorMessages';

const defaultSelectItems = [
  '5 - Bardzo dobra',
  '4 - Dobra',
  '3 - Przeciętna',
  '2 - Zła',
  '1 - Bardzo zła',
];

const requiredValidatorFn = (value) =>
  !value ? 'To pole jest wymagane' : null;

function getInquiryFormData() {
  return [
    {
      defaultValue: '',
      type: 'select',
      name: 'inquiry_01',
      items: defaultSelectItems,
      fieldProps: {
        required: true,
        label: '1. Jakość współpracy z naszą firmą?',
      },
      validationSchema: yup.string().required(errorMessages.required),
    },
    {
      defaultValue: '',
      type: 'select',
      name: 'inquiry_02',
      items: defaultSelectItems,
      fieldProps: {
        required: true,
        label: '2. Jakość otrzymanych informacji na temat naszych produktów?',
      },
      validationSchema: yup.string().required(errorMessages.required),
    },
    {
      defaultValue: '',
      type: 'select',
      name: 'inquiry_03',
      items: defaultSelectItems,
      fieldProps: {
        required: true,
        label: '3. Czas oczekiwania na ofertę?',
      },
      validationSchema: yup.string().required(errorMessages.required),
    },
    {
      defaultValue: '',
      type: 'select',
      name: 'inquiry_04',
      items: [
        '5 - Bardzo krótki',
        '4 - Krótki',
        '3 - Przeciętny',
        '2 - Długi',
        '1 - Bardzo długi',
      ],
      fieldProps: {
        required: true,
        label: '4. Terminowość dostawy zamówienia?',
      },
      validationSchema: yup.string().required(errorMessages.required),
    },
    {
      defaultValue: '',
      type: 'select',
      name: 'inquiry_05',
      items: defaultSelectItems,
      fieldProps: {
        required: true,
        label: '5. Jakość otrzymanego towaru?',
      },
      validationSchema: yup.string().required(errorMessages.required),
    },
    {
      defaultValue: '',
      type: 'select',
      name: 'inquiry_06',
      items: [...defaultSelectItems, '0 - Nie dotyczy'],
      fieldProps: {
        required: true,
        label:
          '6. Jakość szkolenia z zakresu obsługi urządzeń (dot. zakupów w dziale Aparatury)?',
      },
      validationSchema: yup.string().required(errorMessages.required),
    },
    {
      defaultValue: '',
      type: 'select',
      name: 'inquiry_07',
      items: [...defaultSelectItems, '0 - Nie dotyczy'],
      fieldProps: {
        required: true,
        label:
          '7. Adekwatność oferty certyfikowanych materiałów odniesienia w stosunku do potrzeb (dot. zakupów w dziale CRM)?',
      },
      validationSchema: yup.string().required(errorMessages.required),
    },
    {
      defaultValue: '',
      type: 'select',
      name: 'inquiry_08',
      items: ['5 - Konkurencyjna', '3 - Odpowiednia', '1 - Zbyt wysoka'],
      fieldProps: {
        required: true,
        label:
          '8. Cenę oferowanego produktu w stosunku do jego parametrów technicznych?',
      },
      validationSchema: yup.string().required(errorMessages.required),
    },
  ];
}

export default function getInquiryFormFields() {
  const data = getInquiryFormData();
  return [
    {
      type: 'info',
      name: 'info_02',
      fieldProps: {
        content: () => <h3>Metryczka</h3>,
      },
    },
    {
      defaultValue: '',
      type: 'text',
      name: 'name',
      fieldProps: {
        required: true,
        label: 'Imię i nazwisko',
      },
      validationSchema: yup.string().required(errorMessages.required),
    },
    {
      defaultValue: '',
      type: 'text',
      name: 'company',
      fieldProps: {
        required: true,
        label: 'Firma',
      },
      validationSchema: yup.string().required(errorMessages.required),
    },
    {
      defaultValue: '',
      type: 'text',
      subtype: 'email',
      name: 'email',
      fieldProps: {
        required: true,
        label: 'Twój email',
      },
      validationSchema: yup
        .string()
        .required(errorMessages.required)
        .matches(emailRegex, errorMessages.invalidEmail),
    },

    {
      type: 'info',
      name: 'info_03',
      fieldProps: {
        content: () => (
          <div className="inquiry__section">
            <h3>Ankieta badania zadowolenia klienta</h3>
            <span>Jak oceniasz?</span>
          </div>
        ),
      },
    },

    ...data,

    {
      type: 'info',
      name: 'info_04',
      fieldProps: {
        content: () => (
          <div className="inquiry__section inquiry__section--no-padding-bottom">
            <h3>Dodatkowe uwagi</h3>
            <span>
              Będziemy wdzięczni za wszystkie (pozytywne i negatywne) uwagi,
              które pozwolą nam udoskonalić obsługę klientów TUSNOVICS
              INSTRUMENTS SP.Z O.O?
            </span>
          </div>
        ),
      },
    },

    {
      defaultValue: '',
      type: 'text',
      subtype: 'multiline',
      name: 'message',
      fieldProps: {
        required: true,
        label: 'Wiadomość',
        rowsMax: 4,
      },
      validationSchema: yup.string().required(errorMessages.required),
    },

    {
      type: 'info',
      name: 'info_05',
      fieldProps: {
        content: () => (
          <div className="inquiry__section">
            <span>
              Pani/Pana odpowiedzi pozostaną poufne i będą przetwarzane
              wyłącznie w postaci zbiorczych zestawien statystycznych.
            </span>
          </div>
        ),
      },
    },

    {
      defaultValue: false,
      type: 'checkbox',
      name: 'rodo',
      fieldProps: {
        required: true,
        label:
          '* Wyrażam zgodę na przetwarzanie moich danych osobowych do celów prowadzenia komunikacji mailowej. Podanie danych jest dobrowolne, ale niezbędne do przetworzenia zapytania. Zostałam/em poinformowana/y, że przysługuje mi prawo dostępu do swoich danych, możliwości ich poprawiania, żądania zaprzestania ich przetwarzania oraz usunięcia w dowolnym momencie. Dane osobowe nie są przekazywane poza EOG, ani nie zapadają na ich podstawie zautomatyzowane decyzje. Administratorem danych osobowych jest firma Tusnovics Instruments Sp. z o. o. z siedzibą w Krakowie, przy ul. Bociana 4a/49a',
      },
      validationSchema: yup.boolean().oneOf([true], errorMessages.required),
    },

    {
      defaultValue: '',
      type: 'captcha',
      name: 'captcha',
      fieldProps: {},
      validationSchema: yup.string().required(errorMessages.required),
    },
  ];
}

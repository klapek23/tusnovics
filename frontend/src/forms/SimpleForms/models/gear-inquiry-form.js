import React from 'react';
import * as yup from 'yup';
import { emailRegex, phoneRegex } from '@/src/utils/validators';
import errorMessages from '@/src/utils/errorMessages';

export default function getGearInquiryFormFields() {
  return [
    {
      defaultValue: '',
      type: 'text',
      name: 'name',
      fieldProps: {
        required: true,
        label: 'Imię i nazwisko',
      },
      validationSchema: yup.string().required(errorMessages.required),
    },
    {
      defaultValue: '',
      type: 'text',
      name: 'company',
      fieldProps: {
        required: true,
        label: 'Firma',
      },
      validationSchema: yup.string().required(errorMessages.required),
    },
    {
      defaultValue: '',
      type: 'text',
      subtype: 'email',
      name: 'email',
      fieldProps: {
        required: true,
        label: 'Twój email',
      },
      validationSchema: yup
        .string()
        .required(errorMessages.required)
        .matches(emailRegex, errorMessages.invalidEmail),
    },
    {
      defaultValue: '',
      type: 'text',
      subtype: 'phone',
      name: 'phone',
      fieldProps: {
        required: true,
        label: 'Telefon',
      },
      validationSchema: yup
        .string()
        .required(errorMessages.required)
        .matches(phoneRegex, errorMessages.invalidPhoneNumber),
    },
    {
      defaultValue: '',
      type: 'text',
      subtype: 'multiline',
      name: 'message',
      fieldProps: {
        required: true,
        label: 'Wiadomość',
        rowsMax: 4,
      },
      validationSchema: yup.string().required(errorMessages.required),
    },
    {
      type: 'info',
      fieldProps: {
        content: () => <p>* Pola oznaczne gwiazdką są wymagane</p>,
      },
    },
    {
      defaultValue: false,
      type: 'checkbox',
      name: 'rodo',
      fieldProps: {
        required: true,
        label:
          '* Wyrażam zgodę na przetwarzanie moich danych osobowych do celów prowadzenia komunikacji mailowej. Podanie danych jest dobrowolne, ale niezbędne do przetworzenia zapytania. Zostałam/em poinformowana/y, że przysługuje mi prawo dostępu do swoich danych, możliwości ich poprawiania, żądania zaprzestania ich przetwarzania oraz usunięcia w dowolnym momencie. Dane osobowe nie są przekazywane poza EOG, ani nie zapadają na ich podstawie zautomatyzowane decyzje. Administratorem danych osobowych jest firma Tusnovics Instruments Sp. z o. o. z siedzibą w Krakowie, przy ul. Bociana 4a/49a',
      },
      validationSchema: yup.boolean().oneOf([true], errorMessages.required),
    },
    {
      defaultValue: false,
      type: 'checkbox',
      name: 'newsletter',
      fieldProps: {
        label:
          'Chcę otrzymywać bezpłatny newsletter z informacjami o' +
          ' nowościach i produktach firmy.',
      },
    },
  ];
}

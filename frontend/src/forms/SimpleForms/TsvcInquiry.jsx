import React from 'react';

import getInquiryFormFields from './models/inquiry-form';
import TsvcForm from './TsvcForm';

export default function TsvcInquiry() {
  const formFields = getInquiryFormFields();

  return (
    <div className="inquiry">
      <TsvcForm fields={formFields} url="/api/inquiry-form" includeCaptcha />
    </div>
  );
}

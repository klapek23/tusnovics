import React from 'react';

import TsvcForm from './TsvcForm';
import { getContactFormFields } from './models/contact-form';

export default function TsvcContactForm() {
  const formFields = getContactFormFields();

  return (
    <div className="tsvc-contact-form">
      <TsvcForm
        title="Napisz do nas"
        fields={formFields}
        url="/api/contact-form"
      />
    </div>
  );
}

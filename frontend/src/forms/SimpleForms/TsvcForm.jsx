import React, { useState } from 'react';
import { useRouter } from 'next/router';
import TextField from '@material-ui/core/TextField';
import Checkbox from '@material-ui/core/Checkbox';
import FormGroup from '@material-ui/core/FormGroup';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import axios from 'axios';
import { Field, Form, Formik, useFormik } from 'formik';
import * as yup from 'yup';

import TsvcButton from '@/src/components/common/TsvcButton';
import { TsvcCheckboxLabel } from '@/src/forms/fields/TsvcCheckbox';
import TsvcFileUploader from '@/src/forms/fields/TsvcFileUploader';
import TsvcSelect from '@/src/forms/fields/TsvcSelect';
import TsvcCaptcha from '@/src/forms/fields/TsvcCaptcha';

export default function TsvcForm({
  fields,
  title,
  successPageLink,
  url = '/api/contact-form',
  hiddenFields = {},
  includeCaptcha = false,
}) {
  const router = useRouter();
  const [success, setSuccess] = useState(false);

  const defaultState = fields
    .filter(({ type }) => type !== 'info')
    .reduce(
      (prev, { name, defaultValue }) => ({
        ...prev,
        [name]: defaultValue,
      }),
      {},
    );

  const validationSchema = fields
    .filter(({ type }) => type !== 'info')
    .reduce(
      (prev, { name, validationSchema }) => ({
        ...prev,
        [name]: validationSchema,
      }),
      {},
    );

  const form = {
    initialValues: defaultState,
    validationSchema: yup.object().shape(validationSchema),
    onSubmit: async (values, { resetForm, setErrors }) => {
      const data = { ...values, ...hiddenFields };
      const formData = getFormData(data);
      const { data: result } = await sendData(formData, url);

      if (result && result.success) {
        resetForm();

        // redirect to custom succcess page
        if (successPageLink) {
          router.push(successPageLink.href, successPageLink.as);
        } else {
          // display default success "page"
          setSuccess(true);
          setTimeout(() => {
            setSuccess(false);
          }, 5000);
        }
      } else {
        setErrors(result?.errors);
      }
    },
  };

  const formik = useFormik(form);

  const getFormData = (data) => {
    let formData;
    const hasFileInput = fields.find(({ type }) => type === 'file');
    if (hasFileInput) {
      formData = new FormData();
      Object.keys(data).forEach((key) => {
        if (typeof data[key] === 'object' && !data[key].size) {
          formData.append(key, JSON.stringify(data[key]));
        } else {
          formData.append(key, data[key]);
        }
      });
    } else {
      formData = data;
    }

    return formData;
  };

  const sendData = async (formData, sendUrl) => {
    const haveFileInput = !!fields.find(({ type }) => type === 'file');
    try {
      const res = await axios({
        method: 'post',
        url: sendUrl,
        headers: {
          Accept: 'application/json',
          'Content-Type': haveFileInput
            ? 'multipart/form-data'
            : 'application/json',
        },
        data: formData,
      });
      return res;
    } catch (error) {
      return error;
    }
  };

  const renderFields = (errors, handleFileUpload) => {
    return fields.map(({ type, subtype, name, items, fieldProps }) => {
      const { required, label, rowsMax, content, subitems } = fieldProps;
      return (
        <div
          className={`tsvc-form__field-box ${
            type ? `tsvc-form__field-box--${type}` : ''
          } ${subtype ? `tsvc-form__field-box--${subtype}` : ''}`}
          key={`form-field__${name}`}
        >
          {type === 'text' && (
            <Field name={name}>
              {({ field, meta }) => {
                return (
                  <TextField
                    {...field}
                    className="tsvc-form__field"
                    label={label}
                    multiline={subtype === 'multiline'}
                    maxRows={rowsMax}
                    required={required}
                    error={(meta.touched || meta.value) && errors?.[name]}
                    helperText={(meta.touched || meta.value) && errors?.[name]}
                  />
                );
              }}
            </Field>
          )}

          {type === 'info' && content && content()}

          {type === 'checkbox' && !subtype && (
            <Field name={name}>
              {({ field, meta }) => (
                <FormControl
                  required
                  error={(meta.touched || meta.value) && errors?.[name]}
                >
                  <TsvcCheckboxLabel
                    control={<Checkbox {...field} />}
                    label={label}
                  />
                  <FormHelperText>
                    {(meta.touched || meta.value) && errors?.[name]}
                  </FormHelperText>
                </FormControl>
              )}
            </Field>
          )}

          {type === 'checkbox' && subtype === 'multi' && (
            <Field name={name}>
              {({ field, meta }) => {
                return (
                  <FormControl required error={meta.touched && errors?.[name]}>
                    <FormLabel component="legend">{label}</FormLabel>
                    <FormGroup>
                      {subitems &&
                        subitems.map(({ name: subname, label: sublabel }) => (
                          <TsvcCheckboxLabel
                            key={`form-input__${name}-${subname}`}
                            control={
                              <Checkbox
                                {...field}
                                value={subname}
                                inputProps={{ 'data-subname': subname }}
                              />
                            }
                            label={sublabel}
                          />
                        ))}
                    </FormGroup>
                    <FormHelperText>
                      {meta.touched && errors?.[name]}
                    </FormHelperText>
                  </FormControl>
                );
              }}
            </Field>
          )}

          {type === 'select' && (
            <Field name={name}>
              {({ field, meta }) => (
                <FormControl
                  required
                  error={(meta.touched || meta.value) && errors?.[name]}
                >
                  <label htmlFor={name} className="inquiry__label">
                    {label} *
                  </label>
                  <TsvcSelect
                    {...field}
                    id={name}
                    label={label}
                    items={items}
                    error={(meta.touched || meta.value) && errors?.[name]}
                  />
                  <FormHelperText>
                    {(meta.touched || meta.value) && errors?.[name]}
                  </FormHelperText>
                </FormControl>
              )}
            </Field>
          )}

          {type === 'file' && (
            <Field name={name}>
              {({ field, meta }) => {
                return (
                  <TsvcFileUploader
                    name={name}
                    selectedFile={meta.value}
                    onFileSelectError={(file) => handleFileUpload(name, file)}
                    onFileSelectSuccess={(file) => handleFileUpload(name, file)}
                  />
                );
              }}
            </Field>
          )}
        </div>
      );
    });
  };

  return (
    <div className="tsvc-form tsvc-content">
      <div className="row">
        <div className="col-xs-12">
          <article>
            <h4>{title}</h4>
          </article>
          <Formik {...form}>
            {({ errors, handleSubmit, setFieldValue }) => {
              const handleFileUpload = async (name, file) => {
                await setFieldValue(name, file);
              };

              return (
                <Form className="tsvc-form__form" noValidate autoComplete="off">
                  {!success && renderFields(errors, handleFileUpload)}

                  {!success && includeCaptcha && (
                    <div className="tsvc-form__field-box">
                      <Field name="captcha">
                        {({ field, meta }) => {
                          const captchaError =
                            (meta.touched || meta.value) && errors?.captcha;

                          return (
                            <FormControl required error={captchaError}>
                              <TsvcCaptcha
                                {...field}
                                error={captchaError}
                                helperText={captchaError}
                              />
                            </FormControl>
                          );
                        }}
                      </Field>
                    </div>
                  )}
                  <div className="tsvc-form__field-box tsvc-form__field-box--submit">
                    {success ? (
                      <h3>Wiadomość wysłana pomyślnie</h3>
                    ) : (
                      <TsvcButton onClick={handleSubmit} type="submit">
                        Wyślij
                      </TsvcButton>
                    )}
                  </div>
                </Form>
              );
            }}
          </Formik>
        </div>
      </div>
    </div>
  );
}

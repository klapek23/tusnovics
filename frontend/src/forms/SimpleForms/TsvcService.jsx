import React from 'react';

import TsvcForm from './TsvcForm';
import getServiceFormFields from './models/service-form';

export default function TsvcServiceForm() {
  const formFields = getServiceFormFields();

  return (
    <div className="tsvc-contact-form">
      <TsvcForm
        title="Zgłoszenie serwisowe"
        fields={formFields}
        url="/api/service-form"
      />
    </div>
  );
}

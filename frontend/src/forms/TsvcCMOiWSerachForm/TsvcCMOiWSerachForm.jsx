import React from 'react';
import TsvcCMOiWFormAutocomplete from './TsvcCMOiWFormAutocomplete';

export default function TsvcCMOiWSerachForm({ title, link }) {
  const submitLabel = 'Wyszukaj';
  const placeholder = 'CAS / Nazwa materiału / Numer katalogowy';

  const handleSubmit = (e) => {
    e.preventDefault();
  };

  return (
    <div className="tsvc-cmoiw-form tsvc-content">
      {title && (
        <article>
          <h3>{title}</h3>
        </article>
      )}
      <form
        className="tsvc-cmoiw-form__form"
        noValidate
        autoComplete="off"
        onSubmit={handleSubmit}
      >
        <div className="tsvc-cmoiw-form__field-box tsvc-cmoiw-form-form__field-box--text">
          <TsvcCMOiWFormAutocomplete
            name="value"
            title={placeholder}
            link={link}
            submitLabel={submitLabel}
          />
        </div>
      </form>
    </div>
  );
}

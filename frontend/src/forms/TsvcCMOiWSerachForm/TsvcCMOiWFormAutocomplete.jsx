import React, { useEffect, useState } from 'react';
import axios from 'axios';
import Chip from '@material-ui/core/Chip';
import { useRouter } from 'next/router';

import createKey from '@/src/utils/createKey';
import TsvcButton from '@/src/components/common/TsvcButton';
import TsvcLink from '@/src/components/common/TsvcLink';
import TsvcAutocomplete from '../fields/TsvcAutocomplete';

export default function TsvcCMOiWFormAutocomplete({
  name,
  title,
  submitLabel,
  link,
}) {
  let axiosFetchCancelToken;
  const submitUrl = (parsedValue) => `/api/crm/search-ac?search=${parsedValue}`;
  const minCharsForToFetch = 2;
  const router = useRouter();
  const [values, setValues] = useState([]);
  const [value, setValue] = useState('');
  const [suggestions, setSuggestions] = useState([]);

  const routeChangeHandler = () => {
    setValues([]);
    setValue('');
  };

  useEffect(() => {
    router.events.on('routeChangeStart', routeChangeHandler);
    return () => router.events.off('routeChangeStart', routeChangeHandler);
  }, [router]);

  const getSuggestions = async (inputValue) => {
    const parsedValue = inputValue.trim().toLowerCase();
    const escapedValue = encodeURIComponent(parsedValue);
    axiosFetchCancelToken = axios.CancelToken.source();
    const response = await axios.get(submitUrl(escapedValue), {
      cancelToken: axiosFetchCancelToken.token,
    });
    return response.data;
  };

  const handleSuggestionsFetchRequested = ({ reason, value: inputValue }) => {
    if (
      reason !== 'suggestion-selected' &&
      inputValue.length >= minCharsForToFetch
    ) {
      if (axiosFetchCancelToken) {
        axiosFetchCancelToken.cancel('Operation canceled due to new request.');
      }

      getSuggestions(inputValue)
        .then((result) => {
          const isOnList = result.find(
            ({ value }) =>
              value.trim().toLowerCase() === inputValue.trim().toLowerCase(),
          );
          const filterCurrentValue = ({ v }) =>
            v.trim().toLowerCase() !== inputValue.trim().toLowerCase();
          const finalSuggestions = isOnList
            ? [{ value: inputValue }, ...result.filter(filterCurrentValue)]
            : [{ value: inputValue }, ...result];
          setSuggestions(finalSuggestions || []);
        })
        .catch((error) => {
          console.log(error);
          setSuggestions([]);
        });
    }
  };

  const handleSuggestonsClearRequested = () => setSuggestions([]);

  const getSuggestionValue = (inputValue) => inputValue;

  const handleChange = (_, { newValue }) => {
    if (typeof newValue === 'string') {
      setValue(newValue);
    } else {
      setValues([...values, newValue.value]);
      setValue('');
    }
  };

  const handleDelete = (index) => {
    const filtered = values.filter((_, i) => index !== i);
    setValues(filtered);
  };

  const renderSuggestion = ({ value: suggestion }) => <span>{suggestion}</span>;

  const buildQuery = () =>
    values.length > 0
      ? { value: values.map((item) => encodeURIComponent(item)).join('|') }
      : { value: encodeURIComponent(value) };

  return (
    <>
      <label className="tsvc-cmoiw-form__label" htmlFor={name}>
        <span>{title}</span>
        <div className="tsvc-cmoiw-form__box">
          {values.length > 0 && (
            <div className="tsvc-cmoiw-form__selected">
              {values.map((value, i) => (
                <Chip
                  key={createKey(value)}
                  className="tsvc-cmoiw-form__chip"
                  label={value}
                  onDelete={() => handleDelete(i)}
                />
              ))}
            </div>
          )}

          <div className="tsvc-cmoiw-form__inputs">
            <TsvcAutocomplete
              id={name}
              suggestions={suggestions}
              onSuggestionsFetchRequested={handleSuggestionsFetchRequested}
              onSuggestionsClearRequested={handleSuggestonsClearRequested}
              getSuggestionValue={getSuggestionValue}
              onChange={handleChange}
              renderSuggestion={renderSuggestion}
              value={value}
              name={name}
              placeholder={title}
              required
            />
            <TsvcLink
              href={{
                pathname: link.href,
                query: buildQuery(),
              }}
              as={{
                pathname: link.as,
                query: buildQuery(),
              }}
            >
              <TsvcButton type="submit">{submitLabel}</TsvcButton>
            </TsvcLink>
          </div>
        </div>
      </label>
    </>
  );
}

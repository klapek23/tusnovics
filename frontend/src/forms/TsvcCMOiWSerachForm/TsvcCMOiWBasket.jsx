import React, { useState } from 'react';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { AnimatePresence, motion } from 'framer-motion';

import TsvcButton from '@/src/components/common/TsvcButton';
import TsvcLink from '@/src/components/common/TsvcLink';
import TsvcCMOiWList from './TsvcCMOiWSearchResults/TsvcCMOiWList';

export default function TsvcCMOiWBasket({
  title,
  subtitle,
  items,
  onRemoveClick,
  clearItems,
}) {
  const [open, setOpen] = useState(true);

  const toggleBasket = () => {
    setOpen(!open);
  };

  return (
    <div className="tsvc-cmoiw-basket">
      <div className="tsvc-cmoiw-basket__header">
        <h3>{title}</h3>
        <p>
          {subtitle} <strong>{items.length}</strong>
        </p>

        {items.length > 0 && (
          <div className="tsvc-cmoiw-basket__actions">
            {clearItems && (
              <button
                type="button"
                onClick={clearItems}
                className="tsvc-cmoiw-basket__button tsvc-cmoiw-basket__clear-button"
              >
                <DeleteForeverIcon /> Wyczyść listę
              </button>
            )}

            <button
              type="button"
              className={`tsvc-cmoiw-basket__button tsvc-cmoiw-basket__expand-button ${
                !open ? 'collapsed' : ''
              }`}
              onClick={toggleBasket}
            >
              {open ? 'Zwiń' : 'Rozwiń'}
              <ExpandMoreIcon className="icon" />
            </button>
          </div>
        )}
      </div>

      <AnimatePresence>
        {open && (
          <motion.div
            initial={{ height: 0 }}
            animate={{ height: 'auto' }}
            exit={{ height: 0 }}
            transition={{ duration: 0.7 }}
            className="tsvc-cmoiw-basket__content"
          >
            <TsvcCMOiWList
              items={items}
              selected={items}
              onRemoveClick={onRemoveClick}
              short={items.length >= 10}
            />
          </motion.div>
        )}
      </AnimatePresence>

      {items.length > 0 && (
        <div className="tsvc-cmoiw-basket__footer">
          <TsvcLink
            as="/oferta/crm/wyniki-wyszukiwania/1"
            href="/oferta/crm/wyniki-wyszukiwania/[step]"
          >
            <TsvcButton color="black">Zapytaj o wybrane pozycje</TsvcButton>
          </TsvcLink>
        </div>
      )}
    </div>
  );
}

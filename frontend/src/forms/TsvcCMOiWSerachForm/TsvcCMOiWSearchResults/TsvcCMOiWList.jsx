import React from 'react';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import Checkbox from '@material-ui/core/Checkbox';
import { TsvcCheckboxLabel } from '@/src/forms/fields/TsvcCheckbox';

export default function TsvcCMOiWList({
  items,
  selected,
  allSelected,
  onItemClick,
  onRemoveClick,
  onSelectAllClick,
  clearItems,
  renderFooter,
  short,
  className = '',
}) {
  const isItemSelected = (number) => {
    return !!selected.find(({ number: itemNumber }) => itemNumber === number);
  };

  const handleItemClicked = (number) => {
    onItemClick(number);
  };

  const handleSelectAllClicked = () => {
    onSelectAllClick();
  };

  const renderItem = (
    { _id, cas, number, material, manufacturer, description, extra_info },
    isSelected,
  ) => (
    <li className="tsvc-cmoiw-results-list__item" key={_id}>
      <div className="tsvc-cmoiw-results-list__item-actions">
        {onRemoveClick && (
          <DeleteForeverIcon
            className="tsvc-cmoiw-results-list__item-delete"
            onClick={() => onRemoveClick(number)}
          />
        )}
        {onItemClick && (
          <Checkbox
            name={`item-${number}`}
            value={isSelected}
            checked={isSelected}
            className="tsvc-cmoiw-results-list__item-check"
            onChange={() =>
              handleItemClicked({
                _id,
                cas,
                number,
                material,
                manufacturer,
                description,
                extra_info,
              })
            }
          />
        )}
      </div>
      <div className="tsvc-cmoiw-results-list__item-content">
        <div className="tsvc-cmoiw-results-list__item-content-row">
          <span className="tsvc-cmoiw-results-list__item-content-row-label">
            Numer katalogowy / CAS / Producent:
          </span>
          <span className="tsvc-cmoiw-results-list__item-content-row-value">
            {number || 'brak numeru'} / {cas || 'brak numeru CAS'} /{' '}
            {manufacturer}
          </span>
        </div>
        <div className="tsvc-cmoiw-results-list__item-content-row">
          <span className="tsvc-cmoiw-results-list__item-content-row-label">
            Materiał / Substancja:
          </span>
          <span className="tsvc-cmoiw-results-list__item-content-row-value">
            {material}
          </span>
        </div>
        {!short && description && (
          <div className="tsvc-cmoiw-results-list__item-content-row">
            <span className="tsvc-cmoiw-results-list__item-content-row-label">
              Opis:
            </span>
            <span
              className="tsvc-cmoiw-results-list__item-content-row-value"
              dangerouslySetInnerHTML={{ __html: description }}
            />
          </div>
        )}
        {!short && extra_info && (
          <div className="tsvc-cmoiw-results-list__item-content-row">
            <span className="tsvc-cmoiw-results-list__item-content-row-label">
              Dodatkowe informacje:
            </span>
            <span
              className="tsvc-cmoiw-results-list__item-content-row-value"
              dangerouslySetInnerHTML={{ __html: extra_info }}
            />
          </div>
        )}
      </div>
    </li>
  );

  return (
    <div className={`tsvc-cmoiw-results-list ${className}`}>
      <div className="tsvc-cmoiw-results-list__header">
        {items.length > 0 && onSelectAllClick && (
          <div className="tsvc-cmoiw-results-list__actions">
            {items.length > 0 && onSelectAllClick && (
              <TsvcCheckboxLabel
                control={
                  <Checkbox
                    name="allSelected"
                    value={allSelected}
                    checked={allSelected}
                    onChange={handleSelectAllClicked}
                  />
                }
                label="Zaznacz / odznacz wszystkie"
              />
            )}
          </div>
        )}
      </div>

      <ul className="tsvc-cmoiw-results-list__items">
        {items ? (
          items.map((item) => renderItem(item, isItemSelected(item.number)))
        ) : (
          <h4>Brak wyników</h4>
        )}
      </ul>

      {items.length > 0 && clearItems && (
        <div className="tsvc-cmoiw-results-list__footer">
          <div className="tsvc-cmoiw-results-list__actions">
            <button type="button" onClick={clearItems}>
              <DeleteForeverIcon /> Wyczyść listę
            </button>
          </div>
        </div>
      )}
      {renderFooter && (
        <div className="tsvc-cmoiw-results-list__bottom">{renderFooter()}</div>
      )}
    </div>
  );
}

import React, { useEffect, useState } from 'react';
import PageSection from '@/src/components/common/PageSection';
import TsvcCMOiWFormSearchResultsStep1 from './TsvcCMOiWFormSearchResultsStep1';
import TsvcCMOiWFormSearchResultsStep2 from './TsvcCMOiWFormSearchResultsStep2';
import TsvcCMOiWFormSearchResultsStep3 from './TsvcCMOiWFormSearchResultsStep3';

export default function TsvcCMOiWFormStepsContainer({ step }) {
  const SELECTED_ITEMS_KEY = 'tsvcCMOiWSelectedItems';
  const steps = {
    2: { title: 'Wyszukaj kolejne' },
    3: { title: 'Lista wybranych produktów' },
    4: { title: 'Zapytaj o wybrane produkty' },
    5: {
      title: 'Dziękujemy, zapytanie zostało wysłane',
      content:
        'Twoje zapytanie o wybrane produkty zostało do nas wysłane, a jego kopia powinna znaleźć się również w Twojej skrzynce e-mail. Postaramy się odpowiedzieć najszybciej jak to możliwe. W razie pytań zapraszamy do kontaktu.',
    },
  };

  const cachedItems =
    typeof window !== 'undefined'
      ? window.sessionStorage.getItem(SELECTED_ITEMS_KEY)
      : JSON.stringify([]);

  // eslint-disable-next-line no-nested-ternary
  const [selectedItems, setSelectedItems] = useState(
    cachedItems || JSON.stringify([]),
  );
  const parsedSelectedItems = JSON.parse(selectedItems);

  useEffect(() => {
    sessionStorage.setItem(
      SELECTED_ITEMS_KEY,
      JSON.stringify(parsedSelectedItems),
    );
  }, [selectedItems, parsedSelectedItems]);

  const handleRemoveClick = (number) => {
    const filtered = parsedSelectedItems.filter(
      ({ number: itemNumber }) => number !== itemNumber,
    );
    setSelectedItems(JSON.stringify(filtered));
  };

  const clearItems = () => {
    setSelectedItems(JSON.stringify([]));
  };

  return (
    <PageSection>
      {step === 1 && (
        <TsvcCMOiWFormSearchResultsStep1
          title={steps[3].title}
          selected={parsedSelectedItems}
          onRemoveClick={handleRemoveClick}
          clearItems={clearItems}
        />
      )}
      {step === 2 && (
        <TsvcCMOiWFormSearchResultsStep2
          title={steps[4].title}
          selected={parsedSelectedItems}
          onRemoveClick={handleRemoveClick}
          clearItems={clearItems}
        />
      )}
      {step === 3 && (
        <TsvcCMOiWFormSearchResultsStep3
          title={steps[5].title}
          content={steps[5].content}
        />
      )}
    </PageSection>
  );
}

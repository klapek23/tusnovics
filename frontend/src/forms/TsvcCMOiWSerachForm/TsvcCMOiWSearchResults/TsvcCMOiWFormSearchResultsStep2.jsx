import React from 'react';
import { isEmail, isPhone } from '@/src/utils/validators';
import TsvcForm from '@/src/forms/SimpleForms/TsvcForm';

export default function TsvcCMOiWFormSearchResultsStep2({ title, selected }) {
  const formFields = [
    {
      defaultValue: '',
      type: 'text',
      name: 'name',
      fieldProps: {
        required: true,
        label: 'Imię i nazwisko',
      },
      validationFn(value) {
        return !value ? 'To pole jest wymagane' : null;
      },
    },
    {
      defaultValue: '',
      type: 'text',
      name: 'company',
      fieldProps: {
        required: true,
        label: 'Firma',
      },
      validationFn(value) {
        return !value ? 'To pole jest wymagane' : null;
      },
    },
    {
      defaultValue: '',
      type: 'text',
      subtype: 'email',
      name: 'email',
      fieldProps: {
        required: true,
        label: 'Twój email',
      },
      validationFn(value) {
        if (!value) {
          return 'To pole jest wymagane';
        }
        if (!isEmail(value)) {
          return 'Podaj poprawny adres e-mail';
        }
        return null;
      },
    },
    {
      defaultValue: '',
      type: 'text',
      subtype: 'phone',
      name: 'phone',
      fieldProps: {
        required: true,
        label: 'Telefon',
      },
      validationFn(value) {
        if (!value) {
          return 'To pole jest wymagane';
        }
        if (!isPhone(value)) {
          return 'Podaj poprawny numer telefonu';
        }
        return null;
      },
    },
    {
      defaultValue: '',
      type: 'text',
      subtype: 'multiline',
      name: 'message',
      fieldProps: {
        required: true,
        label: 'Wiadomość',
        rowsMax: 4,
      },
      validationFn(value) {
        return !value ? 'To pole jest wymagane' : null;
      },
    },
    {
      type: 'info',
      fieldProps: {
        content: () => <p>* Pola oznaczne gwiazdką są wymagane</p>,
      },
    },
    {
      defaultValue: false,
      type: 'checkbox',
      name: 'rodo',
      fieldProps: {
        required: true,
        label:
          '* Wyrażam zgodę na przetwarzanie moich danych osobowych do celów prowadzenia komunikacji mailowej. Podanie danych jest dobrowolne, ale niezbędne do przetworzenia zapytania. Zostałam/em poinformowana/y, że przysługuje mi prawo dostępu do swoich danych, możliwości ich poprawiania, żądania zaprzestania ich przetwarzania oraz usunięcia w dowolnym momencie. Dane osobowe nie są przekazywane poza EOG, ani nie zapadają na ich podstawie zautomatyzowane decyzje. Administratorem danych osobowych jest firma Tusnovics Instruments Sp. z o. o. z siedzibą w Krakowie, przy ul. Bociana 4a/49a',
      },
      validationFn(value) {
        return !value ? 'To pole jest wymagane' : null;
      },
    },
    {
      defaultValue: false,
      type: 'checkbox',
      name: 'newsletter',
      fieldProps: {
        label:
          'Chcę otrzymywać bezpłatny newsletter z informacjami o' +
          ' nowościach i produktach firmy.',
      },
    },
  ];
  const header1 = 'Liczba wybranych pozycji:';
  const submitUrl = '/api/crm/form';
  const successPageLink = {
    as: '/oferta/crm/wyniki-wyszukiwania/3',
    href: '/oferta/crm/wyniki-wyszukiwania/[step]',
  };

  return (
    <div className="tsvc-cmoiw-form-results tsvc-content">
      <section className="tsvc-cmoiw-form-results__step tsvc-cmoiw-form-results__step--04">
        <div className="row">
          <div className="col-xs-12 col-lg-8 col-lg-offset-2">
            <div className="tsvc-cmoiw-form-results__step-header">
              <h2>{title}</h2>
            </div>
            <div className="tsvc-cmoiw-form-results__step-content">
              <div className="tsvc-cmoiw-form-results__content">
                <p>
                  {header1} <strong>{selected.length}</strong>
                </p>
                <TsvcForm
                  fields={formFields}
                  url={submitUrl}
                  successPageLink={successPageLink}
                  hiddenFields={{
                    selectedItems: selected.map(
                      ({ cas, number, material }) => ({
                        cas,
                        number,
                        material,
                      }),
                    ),
                  }}
                />
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}

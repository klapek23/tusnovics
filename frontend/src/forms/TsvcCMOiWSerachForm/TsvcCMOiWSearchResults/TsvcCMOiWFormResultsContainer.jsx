import React, { useCallback, useEffect, useState } from 'react';
import uniqBy from 'lodash/uniqBy';
import { useRouter } from 'next/router';
import PageSection from '@/src/components/common/PageSection';
import TsvcCMOiWFormResults from './TsvcCMOiWFormResults';

export default function TsvcCMOiWFormResultsContainer({
  title,
  items,
  totalItems,
  pathname,
  totalPages,
  query,
  currentPage,
  searchParams,
}) {
  const SELECTED_ITEMS_KEY = 'tsvcCMOiWSelectedItems';
  const router = useRouter();

  const cachedItems =
    typeof window !== 'undefined'
      ? window.sessionStorage.getItem(SELECTED_ITEMS_KEY)
      : JSON.stringify([]);

  const [selectedItems, setSelectedItems] = useState(
    cachedItems || JSON.stringify([]),
  );

  const parsedSelectedItems = JSON.parse(selectedItems);

  const checkAllSelected = useCallback(() => {
    if (parsedSelectedItems.length >= items.length) {
      const selected = parsedSelectedItems.map(({ _id }) => _id);
      const itemsIds = items.map(({ _id }) => _id);
      let allSelected = true;
      itemsIds.some((_itemId) => {
        if (!selected.includes(_itemId)) {
          allSelected = false;
        }
      });
      return allSelected;
    }
    return false;
  }, [items, parsedSelectedItems]);

  const [allSelected, setAllSelected] = useState(false);

  useEffect(() => {
    const routerChangeHandler = () => setAllSelected(checkAllSelected());

    router.events.on('routeChangeComplete', routerChangeHandler);
    return () => router.events.off('routeChangeComplete', routerChangeHandler);
  }, [router, checkAllSelected]);

  useEffect(() => {
    sessionStorage.setItem(
      SELECTED_ITEMS_KEY,
      JSON.stringify(parsedSelectedItems),
    );
    setAllSelected(checkAllSelected());
  }, [selectedItems, parsedSelectedItems, checkAllSelected]);

  const handleItemClick = ({ _id, ...item }) => {
    const alreadySelected = parsedSelectedItems.find(
      ({ _id: itemId }) => itemId === _id,
    );
    if (alreadySelected) {
      const filtered = parsedSelectedItems.filter(
        ({ _id: itemId }) => itemId !== _id,
      );
      setSelectedItems(JSON.stringify(filtered));
      setAllSelected(false);
    } else {
      setSelectedItems(
        JSON.stringify([...parsedSelectedItems, { _id, ...item }]),
      );
    }
  };

  const handleSelectAllClick = () => {
    if (allSelected) {
      const itemsIds = items.map(({ _id }) => _id);
      const filteredItems = parsedSelectedItems.filter(
        ({ _id }) => !itemsIds.includes(_id),
      );
      setSelectedItems(JSON.stringify(filteredItems));
    } else {
      const filteredItems = uniqBy(
        [...parsedSelectedItems, ...items],
        ({ _id }) => _id,
      );
      setSelectedItems(JSON.stringify(filteredItems));
    }
  };

  const handleRemoveClick = (number) => {
    const filtered = parsedSelectedItems.filter(
      ({ number: itemNumber }) => number !== itemNumber,
    );
    setSelectedItems(JSON.stringify(filtered));
  };

  const clearItems = () => {
    setSelectedItems(JSON.stringify([]));
  };

  return (
    <PageSection>
      <TsvcCMOiWFormResults
        title={title}
        items={items}
        totalItems={totalItems}
        pathname={pathname}
        query={query}
        totalPages={totalPages}
        searchParams={searchParams}
        currentPage={currentPage}
        selected={parsedSelectedItems}
        allSelected={allSelected}
        onItemClick={handleItemClick}
        onSelectAllClick={handleSelectAllClick}
        onRemoveClick={handleRemoveClick}
        clearItems={clearItems}
      />
    </PageSection>
  );
}

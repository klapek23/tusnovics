import React from 'react';
import createKey from '@/src/utils/createKey';
import TsvcLink from '@/src/components/common/TsvcLink';
import TsvcButton from '@/src/components/common/TsvcButton';
import TsvcAllegroPagination from '@/src/components/common/TsvcAllegroPagination';

import TsvcCMOiWBasket from '../TsvcCMOiWBasket';
import TsvcCMOiWList from './TsvcCMOiWList';
import TsvcCMOiWSerachForm from '../TsvcCMOiWSerachForm';

export default function TsvcCMOiWFormResults({
  title,
  items,
  totalItems,
  selected,
  allSelected,
  onItemClick,
  onSelectAllClick,
  onRemoveClick,
  clearItems,
  pathname,
  query,
  totalPages,
  currentPage,
  searchParams,
}) {
  const col1 = {
    subtitle: 'Liczba znalezionych wyników wyszukiwania dla ',
    header1: 'Zaznacz lub odznacz produkty, które chcesz dodać lub usunąć',
    noResultsText: 'BRAK WYNIKÓW WYSZUKIWANIA, WYSZUKAJ PONOWNIE',
    submitUrl: '/oferta/crm/wyniki-wyszukiwania',
    buttons: [],
  };
  const col2 = {
    subtitle: 'Lista wybranych produktów',
    header1: 'Liczba wybranych pozycji:',
  };
  const submitUrl = '/oferta/crm/wyniki-wyszukiwania';

  const handleItemClicked = (item) => {
    onItemClick(item);
  };

  const displayTwoColumns = true;

  const searchPhrase = searchParams.value
    ? decodeURIComponent(searchParams.value.split('|').join(', '))
    : [];

  // eslint-disable-next-line no-nested-ternary
  const layoutClassName = displayTwoColumns
    ? 'col-xs-12 col-lg-6'
    : items.length > 0
    ? 'col-xs-12'
    : 'col-xs-12 col-md-10 col-md-offset-1 col-xl-8 col-xl-offset-2';

  const renderList = () => (
    <div>
      <TsvcCMOiWList
        items={items}
        selected={selected}
        allSelected={allSelected}
        onItemClick={handleItemClicked}
        onSelectAllClick={onSelectAllClick}
        className={`${
          !displayTwoColumns ? 'tsvc-cmoiw-results-list--wide' : ''
        }`}
      />
      <TsvcAllegroPagination
        pathname={pathname}
        query={query}
        totalPages={totalPages}
        currentPage={currentPage}
      />
    </div>
  );

  const renderColumn1 = () => (
    <div className={layoutClassName}>
      <div className="tsvc-cmoiw-form-results__step-header">
        <h2>{title}</h2>
        <p>
          {col1.subtitle} <strong>{searchPhrase}</strong>: {totalItems}
        </p>
        {items.length > 0 ? (
          <h3>{col1.header1}</h3>
        ) : (
          <h3 className="no-results">{col1.noResultsText}</h3>
        )}
      </div>

      <div className="tsvc-cmoiw-form-results__step-content">
        {items.length > 0 && renderList()}
      </div>

      <div
        className={`tsvc-cmoiw-form-results__step-footer ${
          displayTwoColumns
            ? 'tsvc-cmoiw-form-results__step-footer--narrow'
            : ''
        }`}
      >
        {items.length > 0 && (
          <>
            {col1.buttons && col1.buttons.length > 0 && (
              <div>
                {col1.buttons.map(({ link, label, color, display }) =>
                  display ? (
                    <TsvcLink
                      as={link.as}
                      href={link.href}
                      target={link.target}
                      key={createKey(label)}
                    >
                      <TsvcButton color={color}>{label}</TsvcButton>
                    </TsvcLink>
                  ) : (
                    ''
                  ),
                )}
              </div>
            )}
          </>
        )}
      </div>
    </div>
  );

  const renderColumn2 = () => (
    <div className="col-xs-12 col-lg-5 col-lg-offset-1">
      <div className="tsvc-cmoiw-form-results-bg">
        <div className="tsvc-cmoiw-form-results-bg__content" />
        <div className="tsvc-cmoiw-form-results__content">
          <div className="tsvc-cmoiw-form-results__right-column-search">
            <TsvcCMOiWSerachForm
              title="Wyszukaj"
              link={{ href: submitUrl, as: submitUrl }}
            />
          </div>

          <TsvcCMOiWBasket
            title={col2.subtitle}
            subtitle={col2.header1}
            items={selected}
            onRemoveClick={onRemoveClick}
            clearItems={clearItems}
          />

          {col2.buttons && col2.buttons.length > 0 && (
            <div>
              {col2.buttons.map(({ link, label, color, display }) =>
                display ? (
                  <TsvcLink
                    as={link.as}
                    href={link.href}
                    target={link.target}
                    key={createKey(label)}
                  >
                    <TsvcButton color={color}>{label}</TsvcButton>
                  </TsvcLink>
                ) : (
                  ''
                ),
              )}
            </div>
          )}
        </div>
      </div>
    </div>
  );

  return (
    <div className="tsvc-cmoiw-form-results tsvc-content">
      <section className="tsvc-cmoiw-form-results__step tsvc-cmoiw-form-results__step--01">
        <div className="row">
          {renderColumn1()}
          {displayTwoColumns && renderColumn2()}
        </div>
      </section>
    </div>
  );
}

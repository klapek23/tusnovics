import React from 'react';
import createKey from '@/src/utils/createKey';
import TsvcButton from '@/src/components/common/TsvcButton';
import TsvcLink from '@/src/components/common/TsvcLink';

export default function TsvcCMOiWFormSearchResultsStep3({ title, content }) {
  const buttons = [
    {
      link: '/',
      label: 'Strona główna',
    },
    {
      link: '/kontakt',
      label: 'Kontakt',
      color: 'green',
    },
    {
      link: '/oferta',
      label: 'Oferta',
      color: 'blue',
    },
  ];
  return (
    <div className="tsvc-cmoiw-form-results tsvc-content">
      <section className="tsvc-cmoiw-form-results__step tsvc-cmoiw-form-results__step--05">
        <div className="row">
          <div className="col-xs-12 col-lg-8 col-lg-offset-2">
            <div className="tsvc-cmoiw-form-results__step-header">
              <h2>{title}</h2>
            </div>
            <div className="tsvc-cmoiw-form-results__step-content">
              <div className="tsvc-cmoiw-form-results__content">{content}</div>
            </div>
            <div className="tsvc-cmoiw-form-results__step-footer">
              {buttons && buttons.length > 0 && (
                <div>
                  {buttons.map(({ link, label, color }) => (
                    <TsvcLink
                      as={link}
                      href={link}
                      target={link.target}
                      key={createKey(label)}
                    >
                      <TsvcButton color={color}>{label}</TsvcButton>
                    </TsvcLink>
                  ))}
                </div>
              )}
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}

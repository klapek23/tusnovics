import React from 'react';
import createKey from '@/src/utils/createKey';
import TsvcCMOiWList from './TsvcCMOiWList';
import TsvcButton from '@/src/components/common/TsvcButton';
import TsvcLink from '@/src/components/common/TsvcLink';

export default function TsvcCMOiWFormSearchResultsStep1({
  title,
  selected,
  onRemoveClick,
  clearItems,
}) {
  const header1 = 'Liczba wybranych pozycji:';
  const buttons = [
    {
      link: {
        as: '/oferta/crm/wyniki-wyszukiwania',
        href: '/oferta/crm/wyniki-wyszukiwania',
      },
      label: 'Zachowaj listę i szukaj dalej',
      color: 'blue',
      display: true,
    },
    {
      link: {
        as: '/oferta/crm/wyniki-wyszukiwania/2',
        href: '/oferta/crm/wyniki-wyszukiwania/[step]',
      },
      label: 'Zapytaj o wybrane pozycje',
      color: 'black',
      display: selected.length > 0,
    },
  ];
  return (
    <div className="tsvc-cmoiw-form-results tsvc-content">
      <section className="tsvc-cmoiw-form-results__step tsvc-cmoiw-form-results__step--03">
        <div className="row">
          <div className="col-xs-12">
            <div className="tsvc-cmoiw-form-results__step-header">
              <h2>{title}</h2>
            </div>
            <div className="tsvc-cmoiw-form-results__step-content">
              <div className="tsvc-cmoiw-form-results__content">
                <p>
                  {header1} <strong>{selected.length}</strong>
                </p>
                <TsvcCMOiWList
                  items={selected}
                  selected={selected}
                  isRemovable
                  onRemoveClick={onRemoveClick}
                  clearItems={clearItems}
                />
              </div>
            </div>
            <div className="tsvc-cmoiw-form-results__step-footer">
              {buttons && buttons.length > 0 && (
                <div>
                  {buttons.map(({ link, label, color, display }) =>
                    display ? (
                      <TsvcLink
                        as={link.as}
                        href={link.href}
                        target={link.target}
                        key={createKey(label)}
                      >
                        <TsvcButton color={color}>{label}</TsvcButton>
                      </TsvcLink>
                    ) : (
                      ''
                    ),
                  )}
                </div>
              )}
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}

import React, { useRef } from 'react';
import Autosuggest from 'react-autosuggest';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const TsvcAutocomplete = ({
  id,
  value,
  name,
  placeholder,
  onChange,
  suggestions,
  onSuggestionsClearRequested,
  onSuggestionsFetchRequested,
  getSuggestionValue,
  renderSuggestion,
  alwaysRenderSuggestions,
  shouldRenderSuggestions,
  required,
  displayIcon = false,
}) => {
  const autosuggest = useRef();

  const handleOnClick = () => {
    autosuggest.current.input.focus();
  };

  return (
    <div className="tsvc-autocomplete">
      <Autosuggest
        id={id}
        suggestions={suggestions}
        onSuggestionsFetchRequested={onSuggestionsFetchRequested}
        onSuggestionsClearRequested={onSuggestionsClearRequested}
        getSuggestionValue={getSuggestionValue}
        renderSuggestion={renderSuggestion}
        alwaysRenderSuggestions={alwaysRenderSuggestions}
        shouldRenderSuggestions={shouldRenderSuggestions}
        inputProps={{
          value,
          name,
          placeholder,
          onChange,
          required,
        }}
        required={required}
        ref={autosuggest}
      />
      {displayIcon && <ExpandMoreIcon onClick={handleOnClick} />}
    </div>
  );
};

export default TsvcAutocomplete;

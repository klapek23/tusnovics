import { useState } from 'react';
import TextField from '@material-ui/core/TextField';

export default function TsvcCaptcha({ value, helperText, error, ...field }) {
  const [captchaImgKey, setCaptchaImgKey] = useState(null);

  const refreshCaptcha = async () => {
    setCaptchaImgKey(Math.floor(Math.random() * 1000));
  };

  return (
    <div className="tsvc-form__field-box tsvc-form__field-box--captcha">
      <img
        key={captchaImgKey}
        width={200}
        height={100}
        alt="captcha img"
        src={`/api/captcha?${captchaImgKey}`}
      />
      <div style={{ cursor: 'pointer' }} onClick={refreshCaptcha}>
        Odśwież captcha
      </div>
      <TextField
        {...field}
        className="tsvc-form__field"
        name="captcha"
        label="Captcha"
        required
        error={error}
        helperText={helperText}
      />
    </div>
  );
}

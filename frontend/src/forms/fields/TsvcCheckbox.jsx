import { withStyles } from '@material-ui/core/styles';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import React from 'react';

export const TsvcCheckboxLabel = withStyles({
  root: {
    alignItems: 'flex-start',
  },
  label: {
    fontSize: '14px',
    fontWeight: '400',
    marginTop: '8px',
  },
})((props) => <FormControlLabel {...props} />);

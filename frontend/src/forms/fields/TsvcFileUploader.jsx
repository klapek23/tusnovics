import React, { useRef } from 'react';

const TsvcFileUploader = ({
  name,
  selectedFile,
  onFileSelectError,
  onFileSelectSuccess,
}) => {
  const maxFileSize = 5120;
  const fileInput = useRef();

  const handleFileInput = (e) => {
    const file = e.target.files[0];
    const filesize = file ? (file.size / 1024 / 1024).toFixed(4) : 0;
    if (filesize > maxFileSize) {
      onFileSelectError({ error: 'File size cannot exceed more than 5MB' });
    } else {
      onFileSelectSuccess(file);
    }
  };

  const handleButtonClick = () =>
    fileInput.current && fileInput.current.click();

  return (
    <div className="tsvc-file-uploader">
      <input
        type="file"
        name={name}
        ref={fileInput}
        onChange={handleFileInput}
        className="tsvc-file-uploader__input"
      />
      <button
        type="button"
        onClick={handleButtonClick}
        className="tsvc-file-uploader__button"
      >
        Wybierz plik
      </button>
      <p className="tsvc-file-uploader__label">
        {selectedFile ? selectedFile.name : 'Nie wybrano pliku'}
      </p>
    </div>
  );
};

export default TsvcFileUploader;

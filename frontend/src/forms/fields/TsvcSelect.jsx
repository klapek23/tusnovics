import React from 'react';

import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { makeStyles } from '@material-ui/core/styles';

const TsvcSelect = ({ id, value, items, onChange, error }) => {
  const handleChange = (event) => {
    onChange(event);
  };

  const labelStyles = makeStyles({
    shrink: {
      visibility: 'hidden',
    },
  });

  const inputStyles = makeStyles({
    root: {
      backgroundColor: 'transparent !important',
    },
  });

  const labelClasses = labelStyles();
  const inputClasses = inputStyles();

  return (
    <FormControl fullWidth required error={!!error}>
      <InputLabel
        classes={{
          root: labelClasses.root,
          shrink: labelClasses.shrink,
        }}
      >
        {value ? '' : 'WYBIERZ'}
      </InputLabel>
      <Select
        id={id}
        value={value}
        name={id}
        onChange={handleChange}
        classes={{ root: inputClasses.root }}
      >
        {items.map((item, index) => (
          <MenuItem key={`${id}-answer-${index}`} value={item}>
            {item}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
};

export default TsvcSelect;

import React, { Component } from 'react';
import createKey from '@/src/utils/createKey';
import TsvcButton from '@/src/components/common/TsvcButton';
import TsvcLink from '@/src/components/common/TsvcLink';

export default class TsvcGearSearchFormResults extends Component {
  renderItem = ({ name, description, image, url, video, catalogs, pdf }) => (
    <li className="tsvc-gear-search-results__item" key={createKey(name)}>
      <div className="row">
        <div className="col-xs-12 col-md-5 col-lg-4">
          <div className="tsvc-gear-search-results__item-left">
            <img src={image} alt={name} />
            <div className="tsvc-gear-search-results__item-links">
              {url && (
                <a href={url} title={name} target="_blank" rel="noreferrer">
                  <TsvcButton small link color="green">
                    Strona www
                  </TsvcButton>
                </a>
              )}
              {pdf && (
                <a
                  href={pdf}
                  title="Katalog PDF"
                  target="_blank"
                  rel="noreferrer"
                >
                  <TsvcButton small link color="blue">
                    Katalog PDF
                  </TsvcButton>
                </a>
              )}
              {catalogs && (
                <a
                  key={createKey(catalogs)}
                  href={catalogs}
                  title="Katalog"
                  target="_blank"
                  rel="noreferrer"
                >
                  <TsvcButton small link color="blue">
                    Katalog
                  </TsvcButton>
                </a>
              )}
              {video && (
                <a href={video} title="Video" target="_blank" rel="noreferrer">
                  <TsvcButton small link color="black">
                    Video
                  </TsvcButton>
                </a>
              )}
            </div>
          </div>
        </div>
        <div className="col-xs-12 col-md-7 col-lg-8">
          <article className="tsvc-gear-search-results__item-right">
            <h4 dangerouslySetInnerHTML={{ __html: name }} />
            <p dangerouslySetInnerHTML={{ __html: description }} />
            <TsvcLink
              as={{
                pathname: '/oferta/aparatura/wyslij-zapytanie',
                query: { item: name },
              }}
              href={{
                pathname: '/oferta/aparatura/wyslij-zapytanie',
                query: { item: name },
              }}
            >
              <TsvcButton color="blue">Wyślij zapytanie</TsvcButton>
            </TsvcLink>
          </article>
        </div>
      </div>
    </li>
  );

  render() {
    const { title, items } = this.props;
    return (
      <div className="tsvc-gear-search-results tsvc-content">
        <h3>{title}</h3>
        <ul className="tsvc-gear-search-results__list">
          {items ? (
            items.map((item) => this.renderItem(item))
          ) : (
            <h4>Brak wyników</h4>
          )}
        </ul>
        <TsvcLink href="/oferta/aparatura" as="/oferta/aparatura">
          <TsvcButton color="black">Wyszukaj ponownie</TsvcButton>
        </TsvcLink>
      </div>
    );
  }
}

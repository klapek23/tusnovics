import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';

import TsvcButton from '@/src/components/common/TsvcButton';
import TsvcLink from '@/src/components/common/TsvcLink';

import TsvcGearSearchFormAutocomplete from './TsvcGearSearchFormAutocomplete';

const defaultForm = {
  manufacturer: '',
  norm: '',
  method: '',
};

export default function TsvcGearSearchForm({ title }) {
  const submitUrl = '/oferta/aparatura/wyniki-wyszukiwania';
  const submitLabel = 'Wyszukaj';

  const [form, setForm] = useState(defaultForm);
  const router = useRouter();

  const routeChangeHandler = () => {
    setForm(() => defaultForm);
  };

  useEffect(() => {
    router.events.on('routeChangeComplete', routeChangeHandler);

    return () => {
      router.events.off('routeChangeComplete', routeChangeHandler);
    };
  }, [router]);

  const handleManufacturerChange = (manufacturer) => {
    setForm({ ...form, manufacturer });
  };

  const handleNormChange = (norm) => {
    setForm({ ...form, norm });
  };

  const handleMethodChange = (method) => {
    setForm({ ...form, method });
  };

  const createQuery = (manufacturer, norm, method) => {
    const query = {
      ...(manufacturer && { manufacturer: encodeURIComponent(manufacturer) }),
      ...(norm && { norm: encodeURIComponent(norm) }),
      ...(method && { method: encodeURIComponent(method) }),
    };
    return query;
  };

  const { manufacturer, norm, method } = form;
  const query = createQuery(manufacturer, norm, method);

  return (
    <div className="tsvc-gear-search-form tsvc-content">
      <article>
        <h3>{title}</h3>
      </article>
      <form
        className="tsvc-gear-search-form__form"
        noValidate
        autoComplete="off"
      >
        <div className="tsvc-gear-search-form__field-box tsvc-gear-search-form__field-box--text">
          <TsvcGearSearchFormAutocomplete
            name="manufacturer"
            path="manufacturers"
            title="Producent"
            onChange={handleManufacturerChange}
            value={form.manufacturer}
          />
        </div>
        <div className="tsvc-gear-search-form__field-box tsvc-gear-search-form__field-box--text">
          <TsvcGearSearchFormAutocomplete
            name="norm"
            path="norms"
            title="Norma"
            onChange={handleNormChange}
            value={form.norm}
          />
        </div>
        <div className="tsvc-gear-search-form__field-box tsvc-gear-search-form__field-box--text">
          <TsvcGearSearchFormAutocomplete
            name="method"
            path="methods"
            title="Funkcja"
            onChange={handleMethodChange}
            value={form.method}
          />
        </div>
        <div className="tsvc-gear-search-form__field-box tsvc-gear-search-form__field-box--submit">
          <TsvcLink
            href={{ pathname: submitUrl, query }}
            as={{ pathname: submitUrl, query }}
          >
            <TsvcButton type="submit">{submitLabel}</TsvcButton>
          </TsvcLink>
        </div>
      </form>
    </div>
  );
}

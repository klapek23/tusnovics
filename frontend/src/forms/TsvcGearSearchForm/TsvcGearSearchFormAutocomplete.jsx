import React, { useState } from 'react';
import axios from 'axios';
import TsvcAutocomplete from '@/src/forms/fields/TsvcAutocomplete';

export default function TsvcGearSearchFormAutocomplete({
  name,
  title,
  value,
  onChange,
}) {
  const searchUrl = (parsedValue) => `/api/gear-search/${name}/${parsedValue}`;
  const [suggestions, setSuggestions] = useState([]);

  const getSuggestions = async (inputValue) => {
    const parsedValue = inputValue.trim().toLowerCase();
    const response = await axios.get(searchUrl(parsedValue));
    return response.data;
  };

  const handleSuggestionsFetchRequested = ({ reason, value: inputValue }) => {
    if (reason !== 'suggestion-selected') {
      getSuggestions(inputValue)
        .then((result) => {
          setSuggestions(result || []);
        })
        .catch((error) => {
          console.log(error);
          setSuggestions([]);
        });
    }
  };

  const handleSuggestonsClearRequested = () => {
    setSuggestions([]);
  };

  const getSuggestionValue = (inputValue) => {
    return inputValue;
  };

  const handleChange = (event, { newValue }) => {
    onChange(newValue);
  };

  const renderSuggestion = (suggestion) => {
    return <span>{suggestion}</span>;
  };

  return (
    <>
      <label className="tsvc-gear-search-form__label" htmlFor={name}>
        <span>{title}</span>
        <TsvcAutocomplete
          id={name}
          suggestions={suggestions}
          onSuggestionsFetchRequested={handleSuggestionsFetchRequested}
          onSuggestionsClearRequested={handleSuggestonsClearRequested}
          getSuggestionValue={getSuggestionValue}
          alwaysRenderSuggestions={false}
          shouldRenderSuggestions={() => true}
          onChange={handleChange}
          renderSuggestion={renderSuggestion}
          value={value}
          name={name}
          placeholder={title}
          displayIcon
        />
      </label>
    </>
  );
}

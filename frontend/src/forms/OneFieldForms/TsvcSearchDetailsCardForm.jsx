import React from 'react';
import { useRouter } from 'next/router';
import { Field } from 'formik';
import * as yup from 'yup';

import errorMessages from '@/src/utils/errorMessages';
import TsvcButton from '@/src/components/common/TsvcButton';

import TsvcOnefieldForm from './TsvcOnefieldForm';

export default function TsvcSearchDetailsCardForm({ value, black }) {
  const router = useRouter();

  const form = {
    initialValues: {
      catNumber: '',
    },
    validationSchema: yup.object().shape({
      catNumber: yup.string().required(errorMessages.required),
    }),
    onSubmit: async (values, { resetForm }) => {
      try {
        await router.push({
          pathname: '/oferta/crm/karty-charakterystyki',
          query: {
            search: values.catNumber,
          },
        });
        const resultsList = document.getElementById('msds-files-list');
        if (resultsList) {
          resultsList.scrollIntoView(true);
        }
        resetForm();
      } catch (e) {
        console.log(e);
      }
    },
  };

  return (
    <TsvcOnefieldForm
      title="Wyszukiwarka kart charakterystyki"
      form={form}
      black={black}
      renderField={() => (
        <Field
          type="text"
          name="catNumber"
          placeholder="Podaj numer katalogowy"
          className="tsvc-onefield-form__field"
        />
      )}
      renderSubmit={({ handleSubmit }) => (
        <TsvcButton color="white" type="submit" onClick={handleSubmit}>
          Szukaj
        </TsvcButton>
      )}
      renderErrors={(errors) => errors.catNumber}
    />
  );
}

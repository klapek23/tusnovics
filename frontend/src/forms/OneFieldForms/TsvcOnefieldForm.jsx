import React from 'react';
import { Form, Formik } from 'formik';

export default function TsvcOnefieldForm({
  title,
  black,
  form,
  renderField,
  renderSubmit,
  renderErrors,
}) {
  return (
    <div
      className={`tsvc-onefield-form ${
        black ? 'tsvc-onefield-form--black' : ''
      } tsvc-content`}
    >
      <div className="row">
        <div className="col-xs-12">
          <article>
            <h4>{title}</h4>
          </article>

          <Formik {...form}>
            {({ errors, handleSubmit }) => (
              <>
                <Form
                  className="tsvc-onefield-form__form"
                  noValidate
                  autoComplete="off"
                >
                  <div className="tsvc-onefield-form__field-box tsvc-onefield-form__field-box--text">
                    {renderField()}
                  </div>
                  <div className="tsvc-onefield-form__field-box tsvc-onefield-form__field-box--submit">
                    {renderSubmit({ handleSubmit })}
                  </div>
                </Form>

                {errors && (
                  <span className="tsvc-onefield-form__field-error">
                    {renderErrors(errors)}
                  </span>
                )}
              </>
            )}
          </Formik>
        </div>
      </div>
    </div>
  );
}

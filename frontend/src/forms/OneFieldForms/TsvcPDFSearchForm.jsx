import React, { useState } from 'react';
import { Field } from 'formik';
import * as yup from 'yup';

import TsvcButton from '@/src/components/common/TsvcButton';
import errorMessages from '@/src/utils/errorMessages';
import TsvcFilesList from '@/src/components/common/TsvcFilesList';

import TsvcOnefieldForm from './TsvcOnefieldForm';

export default function TsvcPDFSearchForm({ files }) {
  const [filesToShow, setFilesToShow] = useState();

  const form = {
    initialValues: {
      catNumber: '',
    },
    validationSchema: yup.object().shape({
      catNumber: yup.string().required(errorMessages.required),
    }),
    onSubmit: ({ catNumber }) => {
      const result = getFilesList(catNumber);
      setFilesToShow(result);
    },
  };

  const getFilesList = (value) => {
    const parsedValue = value.trim().toLowerCase();
    if (parsedValue.length === 0) {
      resolve([]);
    }
    const filteredFiles = files.filter(({ file }) => {
      return file.title.trim().toLowerCase().includes(parsedValue);
    });
    return filteredFiles;
  };

  const mappedFilesToShow = filesToShow?.map(({ file }) => ({
    name: file.title,
    path: file.url,
  }));

  return (
    <div className="tsvc-pdf-search-form tsvc-content">
      <TsvcOnefieldForm
        title="Wyszukiwarka kart charakterystyki"
        form={form}
        black
        renderField={() => (
          <Field
            type="text"
            name="catNumber"
            placeholder="Podaj numer katalogowy"
            className="tsvc-onefield-form__field"
          />
        )}
        renderSubmit={({ handleSubmit }) => (
          <TsvcButton color="white" type="submit" onClick={handleSubmit}>
            Szukaj
          </TsvcButton>
        )}
        renderErrors={(errors) => errors.catNumber}
      />

      {mappedFilesToShow && (
        <TsvcFilesList
          title="Wyniki wyszukiwania"
          subtitle="Liczba znalezionych wyników wyszukiwania:"
          noResultContent={() => (
            <p>
              Nie znaleziono dokumentu.{' '}
              <strong>
                Napisz do nas:{' '}
                <a
                  href="mailto:marketing@tusnovics.pl"
                  title="marketing@tusnovics.pl"
                >
                  marketing@tusnovics.pl
                </a>
              </strong>
            </p>
          )}
          downloadLabel="POBIERZ"
          items={mappedFilesToShow}
        />
      )}
    </div>
  );
}

import React from 'react';
import axios from 'axios';
import * as yup from 'yup';
import { Field } from 'formik';
import { BsX } from 'react-icons/bs';
import TsvcButton from '@/src/components/common/TsvcButton';
import errorMessages from '@/src/utils/errorMessages';

import TsvcOnefieldForm from './TsvcOnefieldForm';

export default function TsvcLoginForm({ onUserLogin, onOutsideClick, onCloseClick }) {
  const submitUrl = '/api/auth/login';
  const form = {
    initialValues: {
      password: '',
    },
    validationSchema: yup.object().shape({
      password: yup.string().required(errorMessages.required),
    }),
    onSubmit: async (values, { setErrors }) => {
      const { data } = await sendData(values.password);
      if (data?.ok) {
        onUserLogin(data)
      } else {
        setErrors({ password: errorMessages.invalidPassword });
      }
    },
  };

  async function sendData(password) {
    try {
      const res = await axios({
        method: 'post',
        url: submitUrl,
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        data: {
          password,
        },
      });
      return res;
    } catch (error) {
      return error;
    }
  }

  const handleOutsideClick = () => {
    onOutsideClick();
  };

  const handleCloseClick = () => {
    onCloseClick();
  };

  return (
    <div className="tsvc-login-form">
      <div className="tsvc-login-form__overlay" onClick={handleOutsideClick} />
      <div className="tsvc-login-form__content">
        <BsX
          className="tsvc-login-form__close-button"
          onClick={handleCloseClick}
        />
        <TsvcOnefieldForm
          title="Zaloguj się, aby móc pobierać pliki"
          form={form}
          renderField={() => (
            <Field
              type="password"
              name="password"
              placeholder="Podaj hasło"
              className="tsvc-onefield-form__field"
            />
          )}
          renderSubmit={({ handleSubmit }) => (
            <TsvcButton color="white" type="submit" onClick={handleSubmit}>
              Zaloguj się
            </TsvcButton>
          )}
          renderErrors={(errors) => errors.password}
          black
        />
      </div>
    </div>
  );
}

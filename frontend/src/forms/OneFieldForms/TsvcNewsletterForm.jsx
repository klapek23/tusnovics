import React, { useState } from 'react';
import axios from 'axios';
import { Field } from 'formik';
import * as yup from 'yup';

import errorMessages from '@/src/utils/errorMessages';
import TsvcButton from '@/src/components/common/TsvcButton';
import TsvcOnefieldForm from './TsvcOnefieldForm';

export default function TsvcNewsletterForm() {
  const submitUrl = '/api/newsletter-form';
  const successMessageTimeout = 5000;

  const [success, setSuccess] = useState(false);
  const form = {
    initialValues: {
      email: '',
    },
    validationSchema: yup.object().shape({
      email: yup
        .string()
        .required(errorMessages.required)
        .email(errorMessages.invalidEmail),
    }),
    onSubmit: async (values, { setErrors }) => {
      const { data } = await sendData(values.email);
      if (data.success) {
        setSuccess(true);
        setTimeout(() => {
          setSuccess(false);
        }, successMessageTimeout);
      } else {
        setErrors({ email: data.errors.email });
      }
    },
  };

  async function sendData(email) {
    try {
      const res = await axios({
        method: 'post',
        url: submitUrl,
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        data: {
          email,
        },
      });
      return res;
    } catch (error) {
      return error;
    }
  }

  return (
    <div className="tsvc-newsletter-form tsvc-content">
      {success ? (
        <h4>Wiadomość wysłana pomyślnie</h4>
      ) : (
        <TsvcOnefieldForm
          title="Zapisz sie do newslettera"
          form={form}
          renderField={() => (
            <Field
              type="email"
              name="email"
              placeholder="Podaj adres email"
              className="tsvc-onefield-form__field"
            />
          )}
          renderSubmit={({ handleSubmit }) => (
            <TsvcButton color="white" type="submit" onClick={handleSubmit}>
              Zapisz się
            </TsvcButton>
          )}
          renderErrors={(errors) => errors.email}
        />
      )}
    </div>
  );
}

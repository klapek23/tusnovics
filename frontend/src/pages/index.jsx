import React from 'react';
import Image from 'next/image';
import TsvcSlider from '@/src/components/TsvcSlider';
import TsvcButton from '@/src/components/common/TsvcButton';
import TsvcLink from '@/src/components/common/TsvcLink';
import TsvcContent01 from '@/src/components/common/TsvcContent01';
import TsvcContent02 from '@/src/components/common/TsvcContent02';
import TsvcContactBox from '@/src/components/TsvcContactBox';
import TsvcContentBoxes from '@/src/components/common/TsvcContentBoxes';
import PageSection from '@/src/components/common/PageSection';
import TsvcContactForm from '@/src/forms/SimpleForms/TsvcContactForm';
import shuffle from '@/src/utils/arrayShuffle';
import wp from '@/src/utils/wp-api-client';

export async function getServerSideProps() {
  try {
    const [page, posts] = await Promise.all([
      wp
        .pages()
        .slug('home-page')
        .embed()
        .then((data) => {
          return data[0];
        }),
      wp.posts().embed(),
    ]);

    const randomizedSlides = shuffle(page.acf.slider || []);
    const offer = {
      title: page.acf.offer_section.title,
      items: page.acf.offer_section.items.map(({ link, ...item }) => ({
        ...item,
        link: {
          ...link,
          as: link.url,
          href: link.url,
        },
        label: link.title,
      })),
    };

    const lastNews = {
      ...posts[0],
      image: {
        url: posts[0]._embedded['wp:featuredmedia'][0].source_url,
        alt: posts[0]._embedded['wp:featuredmedia'][0].alt_text,
      },
    };

    return {
      props: {
        page: {
          ...page,
          acf: {
            ...page.acf,
            slider: randomizedSlides,
          },
        },
        offer,
        lastNews,
      },
    };
  } catch (err) {
    console.log(err);
  }

  return {};
}

const Index = ({ lastNews, page, offer }) => {
  const aboutUs = page.acf.content01;
  const contactBox = page.acf.contactbox;

  return (
    <>
      <TsvcSlider>
        {page.acf.slider.map(({ bg, caption, link }) => (
          <React.Fragment key={bg.id}>
            <Image src={bg.url} alt={bg.alt} layout="fill" unoptimized />
            <div className="tsvc-slider__caption">
              <div className="tsvc-slider__caption-center">
                {caption ? (
                  <h1 dangerouslySetInnerHTML={{ __html: caption }} />
                ) : null}
                {link ? (
                  <TsvcLink as={link.url} href={link.url} target={link.target}>
                    <TsvcButton>{link.title}</TsvcButton>
                  </TsvcLink>
                ) : null}
              </div>
            </div>
          </React.Fragment>
        ))}
      </TsvcSlider>

      <div style={{ backgroundColor: '#ffffff' }}>
        <PageSection>
          <TsvcContentBoxes
            title={offer.title}
            items={offer.items}
            animated
            invertedAnimation
          />
        </PageSection>
      </div>

      <div style={{ backgroundColor: '#e2f7f8' }}>
        <PageSection>
          <TsvcContent01
            title="Aktualności"
            subtitle={lastNews.title.rendered}
            extra={new Date(lastNews.date).toLocaleDateString('pl-PL', {
              day: '2-digit',
              month: 'long',
              year: 'numeric',
            })}
            image={lastNews.image}
            link={lastNews.link}
          >
            <div
              dangerouslySetInnerHTML={{
                __html: lastNews.excerpt.rendered,
              }}
            />
          </TsvcContent01>
        </PageSection>
      </div>

      <div style={{ backgroundImage: `url(${aboutUs.bg.url})` }}>
        <PageSection>
          <TsvcContent02 title={aboutUs.title} link={aboutUs.link} noPadding>
            <div dangerouslySetInnerHTML={{ __html: aboutUs.content }} />
          </TsvcContent02>
        </PageSection>
      </div>

      <div style={{ backgroundColor: '#dde9f4' }}>
        <PageSection>
          <div className="row">
            <div className="col-xs-12 col-md-6 col-lg-offset-1 col-lg-5 col-xl-offset-1 col-xl-4">
              <TsvcContactBox
                title={contactBox.title}
                subtitle={contactBox.subtitle}
                link={contactBox.link}
              >
                <div dangerouslySetInnerHTML={{ __html: contactBox.content }} />
              </TsvcContactBox>
            </div>
            <div className="col-xs-12 col-md-6 col-lg-offset-1 col-lg-5 col-xl-offset-1 col-xl-4">
              <TsvcContactForm />
            </div>
          </div>
        </PageSection>
      </div>
    </>
  );
};

export default Index;

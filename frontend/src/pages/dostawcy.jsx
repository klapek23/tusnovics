import React from 'react';
import TsvcMainBanner from '@/src/components/common/TsvcMainBanner';
import TsvcBreadcrumbs from '@/src/components/common/TsvcBreadcrumbs';
import PageSection from '@/src/components/common/PageSection';
import TsvcSuppliersList from '@/src/components/TsvcSuppliersList';
import TsvcRawContent from '@/src/components/common/TsvcRawContent';
import TsvcSeoContent, {
  getSeoContentFromPage,
} from '@/src/components/TsvcSeoContent/TsvcSeoContent';
import wp from '@/src/utils/wp-api-client';

export async function getServerSideProps() {
  try {
    const [page] = await Promise.all([
      wp
        .pages()
        .slug('dostawcy')
        .embed()
        .then((data) => {
          return data[0];
        }),
    ]);

    return { props: { page } };
  } catch (err) {
    console.log(err);
  }

  return {};
}

export default function Suppliers({ breadcrumbs, page }) {
  const banner = page.acf.main_banner;
  const suppliers = page.acf.dostawcy;
  const seoContent = getSeoContentFromPage(page);

  return (
    <>
      <TsvcMainBanner image={banner.image} title={banner.title} />

      <PageSection className="breadcrumbs">
        <TsvcBreadcrumbs items={breadcrumbs} />
      </PageSection>

      {seoContent && (
        <PageSection smallPaddingBottom bg="#f1f6fb">
          <TsvcRawContent>
            <TsvcSeoContent {...seoContent} />
          </TsvcRawContent>
        </PageSection>
      )}

      <PageSection>
        <TsvcSuppliersList title={suppliers.title} items={suppliers.items} />
      </PageSection>
    </>
  );
}

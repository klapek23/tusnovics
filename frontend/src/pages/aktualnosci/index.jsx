import React from 'react';
import TsvcMainBanner from '@/src/components/common/TsvcMainBanner';
import TsvcBreadcrumbs from '@/src/components/common/TsvcBreadcrumbs';
import PageSection from '@/src/components/common/PageSection';
import TsvcNewsList from '@/src/components/TsvcNewsList';
import TsvcSeoContent, {
  getSeoContentFromPage,
} from '@/src/components/TsvcSeoContent/TsvcSeoContent';
import TsvcRawContent from '@/src/components/common/TsvcRawContent';
import { convertWPNews } from '@/src/utils/news';
import wp from '@/src/utils/wp-api-client';

const perPage = 3;

export async function getServerSideProps({ req, query }) {
  const langHeader = req.headers['accept-language'] ?? 'pl';
  const lang = langHeader.split(',')[0];
  const currentPage = query.page ? parseInt(query.page, 10) : 1;

  try {
    const [page, news] = await Promise.all([
      wp
        .pages()
        .slug('aktualnosci')
        .embed()
        .then((data) => {
          return data[0];
        }),
      wp.posts().perPage(perPage).page(currentPage).embed(),
    ]);

    return {
      props: {
        page,
        news: convertWPNews(news, lang),
        totalPages: news._paging.totalPages,
        currentPage,
      },
    };
  } catch (err) {
    console.log(err);
  }

  return {};
}

export default function News({
  breadcrumbs,
  page,
  news,
  totalPages,
  currentPage,
}) {
  const banner = page.acf.main_banner;
  const seoContent = getSeoContentFromPage(page);

  return (
    <>
      <TsvcMainBanner
        image={banner.image}
        title={banner.title}
        subtitle={banner.subtitle}
      />

      <PageSection className="breadcrumbs" bg="#f1f6fb">
        <TsvcBreadcrumbs items={breadcrumbs} />
      </PageSection>

      <TsvcNewsList
        title={page.title.rendered}
        items={news}
        currentPage={currentPage}
        totalPages={totalPages}
        pagination
      />

      {seoContent && (
        <PageSection smallPaddingBottom bg="#f1f6fb">
          <TsvcRawContent>
            <TsvcSeoContent {...seoContent} />
          </TsvcRawContent>
        </PageSection>
      )}
    </>
  );
}

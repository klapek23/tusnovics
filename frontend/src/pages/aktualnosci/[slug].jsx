import React from 'react';
import TsvcMainBanner from '@/src/components/common/TsvcMainBanner';
import TsvcBreadcrumbs from '@/src/components/common/TsvcBreadcrumbs';
import PageSection from '@/src/components/common/PageSection';
import TsvcRawContent from '@/src/components/common/TsvcRawContent';
import { convertWPNews, convertDate } from '@/src/utils/news';
import TsvcShortNewsList from '@/src/components/TsvcShortNewsList';
import TsvcButton from '@/src/components/common/TsvcButton';
import wp from '@/src/utils/wp-api-client';
import { useRouter } from 'next/router';

export async function getServerSideProps({ req, query }) {
  const langHeader = req.headers['accept-language'] ?? 'pl';
  const lang = langHeader.split(',')[0];

  const { slug } = query;
  try {
    const page = await wp
      .posts()
      .slug(slug)
      .embed()
      .then((data) => {
        return data[0];
      });

    const news = await wp.posts().exclude(page.id).perPage(3).embed();

    return {
      props: {
        page: { ...page, date: convertDate(page.date, lang) },
        news: convertWPNews(news, lang),
      },
    };
  } catch (err) {
    console.log(err);
  }

  return {};
}

export default function NewsSingle({ page, news, breadcrumbs }) {
  const banner = page.acf.main_banner;
  const seoContent = page.acf && page.acf.seo_content;
  const router = useRouter();

  const handleBackClick = () => {
    router.back();
  };

  return (
    <>
      <TsvcMainBanner
        image={banner.image}
        title={banner.title}
        subtitle={banner.subtitle}
      />

      <PageSection className="breadcrumbs">
        <TsvcBreadcrumbs items={breadcrumbs} />
      </PageSection>

      {seoContent && (
        <PageSection noPaddingBottom>
          <TsvcRawContent>
            <article dangerouslySetInnerHTML={{ __html: seoContent }} />
          </TsvcRawContent>
        </PageSection>
      )}

      <PageSection>
        <div className="row">
          <div className="col-xs-12 col-md-7 col-lg-7">
            <TsvcRawContent title={page.title.rendered}>
              <p>{page.date}</p>
              <article
                style={{ marginBottom: '60px' }}
                dangerouslySetInnerHTML={{ __html: page.content.rendered }}
              />
              <TsvcButton onClick={handleBackClick} color="black">
                Wróć
              </TsvcButton>
            </TsvcRawContent>
          </div>
          <div className="col-xs-12 col-md-4 col-md-offset-1 col-lg-4">
            <TsvcShortNewsList title="Ostatnie wpisy" items={news} short />
          </div>
        </div>
      </PageSection>
    </>
  );
}

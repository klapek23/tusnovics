import React from 'react';
import axios from 'axios';
import TsvcGearSearchForm from '@/src/forms/TsvcGearSearchForm/TsvcGearSearchForm';
import TsvcGearSearchFormResults from '@/src/forms/TsvcGearSearchForm/TsvcGearSearchFormResults';
import TsvcMainBanner from '@/src/components/common/TsvcMainBanner';
import TsvcBreadcrumbs from '@/src/components/common/TsvcBreadcrumbs';
import TsvcButton from '@/src/components/common/TsvcButton';
import TsvcLink from '@/src/components/common/TsvcLink';
import PageSection from '@/src/components/common/PageSection';
import TsvcRawContent from '@/src/components/common/TsvcRawContent';
import TsvcSeoContent, {
  getSeoContentFromPage,
} from '@/src/components/TsvcSeoContent/TsvcSeoContent';
import wp from '@/src/utils/wp-api-client';
import Config from '@/config';

export async function getServerSideProps({ query, req }) {
  let searchResults = [];
  const searchQuery = query
    ? Object.keys(query)
        .map((key) => `${key}=${query[key]}`)
        .join('&')
    : undefined;
  const ssr = !!req;
  try {
    searchResults = ssr
      ? await axios.get(`/api/gear-search?${searchQuery}`, {
          baseURL: Config.frontendUrl,
        })
      : await axios.get(`/api/gear-search?${searchQuery}`);
    const [page] = await Promise.all([
      wp
        .pages()
        .slug('aparatura-wyniki-wyszukiwania')
        .embed()
        .then((data) => {
          return data[0];
        }),
    ]);

    return { props: { page, searchResults: searchResults.data || [] } };
  } catch (err) {
    console.log(err);
  }

  return {};
}

export default function GearSearchResults({
  page,
  breadcrumbs,
  searchResults,
}) {
  const banner = page.acf.main_banner;
  const seoContent = getSeoContentFromPage(page);

  return (
    <>
      <TsvcMainBanner
        image={banner.image}
        title={`${banner.title}`}
        subtitle={banner.subtitle}
      />

      <PageSection className="breadcrumbs">
        <TsvcBreadcrumbs items={breadcrumbs} />
      </PageSection>

      <PageSection>
        <div className="row">
          <div className="col-xs-12 col-lg-7">
            <TsvcGearSearchFormResults
              title="Wyniki wyszukiwania"
              items={searchResults.items}
            />
          </div>

          <div className="col-xs-12 col-lg-4 col-lg-offset-1">
            <div className="tsvc-gear-search-bg">
              <div className="tsvc-gear-search-bg__content" />
              <div className="tsvc-gear-search__content">
                <TsvcGearSearchForm title="Wyszukaj" />
                <div style={{ marginTop: '30px' }}>
                  <TsvcLink href="/oferta/aparatura" as="/oferta/aparatura">
                    <TsvcButton color="blue">Wyświetl kategorie</TsvcButton>
                  </TsvcLink>
                </div>
              </div>
            </div>
          </div>
        </div>
      </PageSection>

      {seoContent && (
        <PageSection smallPaddingBottom bg="#f1f6fb">
          <TsvcRawContent>
            <TsvcSeoContent {...seoContent} />
          </TsvcRawContent>
        </PageSection>
      )}
    </>
  );
}

import React from 'react';
import TsvcMainBanner from '@/src/components/common/TsvcMainBanner';
import TsvcBreadcrumbs from '@/src/components/common/TsvcBreadcrumbs';
import TsvcContentBoxes from '@/src/components/common/TsvcContentBoxes';
import TsvcGearSearchForm from '@/src/forms/TsvcGearSearchForm/TsvcGearSearchForm';
import PageSection from '@/src/components/common/PageSection';
import TsvcManufacturers from '@/src/components/common/TsvcManufactuers';
import TsvcRawContent from '@/src/components/common/TsvcRawContent';
import TsvcSeoContent, {
  getSeoContentFromPage,
} from '@/src/components/TsvcSeoContent/TsvcSeoContent';
import wp from '@/src/utils/wp-api-client';

export async function getServerSideProps() {
  try {
    const [page, manufacturersPage] = await Promise.all([
      wp
        .pages()
        .slug('aparatura')
        .embed()
        .then((data) => {
          return data[0];
        }),
      wp
        .pages()
        .slug('aparatura-producenci')
        .then((data) => {
          return data[0];
        }),
    ]);

    const boxes = {
      title: page.acf.aparatura_categories.title,
      items: page.acf.aparatura_categories.categories.map(
        ({ slug, ...item }) => ({
          ...item,
          link: {
            as: `/oferta/aparatura/${slug}`,
            href: '/oferta/aparatura/[category]',
            title: item.label,
          },
          title: item.title ?? null,
        }),
      ),
    };

    return {
      props: {
        page,
        boxes,
        manufacturers: manufacturersPage.acf.manufacturers.items,
      },
    };
  } catch (err) {
    console.log(err);
  }

  return {};
}

export default function Gears({ breadcrumbs, page, manufacturers, boxes }) {
  const banner = page.acf.main_banner;
  const seoContent = getSeoContentFromPage(page);

  return (
    <>
      <TsvcMainBanner
        image={banner.image}
        title={banner.title}
        subtitle={banner.subtitle}
      />

      <PageSection className="breadcrumbs" bg="#f1f6fb">
        <TsvcBreadcrumbs items={breadcrumbs} />
      </PageSection>

      <PageSection bg="#f1f6fb" smallPaddingTop>
        <div className="row">
          <div className="col-xs-12 col-md-8 col-md-offset-2">
            <TsvcGearSearchForm title="Wyszukaj" />
          </div>
        </div>
      </PageSection>

      {seoContent && (
        <PageSection smallPaddingBottom bg="#f1f6fb">
          <TsvcRawContent>
            <TsvcSeoContent {...seoContent} />
          </TsvcRawContent>
        </PageSection>
      )}

      <PageSection>
        <TsvcContentBoxes title={boxes.title} items={boxes.items} animated />
      </PageSection>

      <PageSection bg="#f1f6fb">
        <TsvcManufacturers
          title="Producenci"
          items={manufacturers}
          url="/oferta/aparatura/producenci"
          buttonLabel="Zobacz więcej"
        />
      </PageSection>
    </>
  );
}

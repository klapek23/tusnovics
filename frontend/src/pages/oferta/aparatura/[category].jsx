import React from 'react';
import axios from 'axios';
import TsvcGearSearchForm from '@/src/forms/TsvcGearSearchForm/TsvcGearSearchForm';
import TsvcGearSearchFormResults from '@/src/forms/TsvcGearSearchForm/TsvcGearSearchFormResults';
import TsvcMainBanner from '@/src/components/common/TsvcMainBanner';
import TsvcBreadcrumbs from '@/src/components/common/TsvcBreadcrumbs';
import TsvcButton from '@/src/components/common/TsvcButton';
import TsvcLink from '@/src/components/common/TsvcLink';
import PageSection from '@/src/components/common/PageSection';
import TsvcRawContent from '@/src/components/common/TsvcRawContent';
import TsvcSeoContent, {
  getCategorySeoContent,
} from '@/src/components/TsvcSeoContent/TsvcSeoContent';
import wp from '@/src/utils/wp-api-client';
import Config from '@/config';

export async function getServerSideProps({ query, req }) {
  let searchResults = [];
  const ssr = !!req;
  const { category } = query;
  try {
    if (ssr) {
      searchResults = await axios.get(`/api/gear-search?category=${category}`, {
        baseURL: Config.frontendUrl,
      });
    } else {
      searchResults = await axios.get(`/api/gear-search?category=${category}`);
    }
    const [page] = await Promise.all([
      wp
        .pages()
        .slug('aparatura')
        .embed()
        .then((data) => {
          return data[0];
        }),
    ]);

    const fullCategory = page.acf.aparatura_categories.categories.find(
      (cat) => cat.slug === category,
    );

    return {
      props: {
        category: fullCategory,
        page: {
          ...page,
          acf: {
            ...page.acf,
            seo: fullCategory.seo,
          },
        },
        searchResults: searchResults.data,
      },
    };
  } catch (err) {
    console.log(err);
  }

  return {};
}

export default function GearCategory({
  category,
  page,
  breadcrumbs,
  searchResults,
}) {
  const seoContent = getCategorySeoContent(category);

  return (
    <>
      <TsvcMainBanner image={category.banner} title={category.banner_text} />

      <PageSection className="breadcrumbs">
        <TsvcBreadcrumbs items={breadcrumbs} />
      </PageSection>

      <PageSection>
        <div className="row">
          <div className="col-xs-12 col-lg-7">
            <TsvcGearSearchFormResults
              title={`Produkty w kategorii ${category.label}`}
              items={searchResults.items}
            />
          </div>

          <div className="col-xs-12 col-lg-4 col-lg-offset-1">
            <div className="tsvc-gear-search-bg">
              <div className="tsvc-gear-search-bg__content" />
              <div className="tsvc-gear-search__content">
                <TsvcGearSearchForm title="Wyszukaj" />
                <div style={{ marginTop: '30px' }}>
                  <TsvcLink href="/oferta/aparatura" as="/oferta/aparatura">
                    <TsvcButton color="blue">Wyświetl kategorie</TsvcButton>
                  </TsvcLink>
                </div>
              </div>
            </div>
          </div>
        </div>
      </PageSection>

      {seoContent && (
        <PageSection smallPaddingBottom bg="#f1f6fb">
          <TsvcRawContent>
            <TsvcSeoContent {...seoContent} />
          </TsvcRawContent>
        </PageSection>
      )}
    </>
  );
}

import React from 'react';
import TsvcMainBanner from '@/src/components/common/TsvcMainBanner';
import TsvcBreadcrumbs from '@/src/components/common/TsvcBreadcrumbs';
import PageSection from '@/src/components/common/PageSection';
import TsvcForm from '@/src/forms/SimpleForms/TsvcForm';
import TsvcRawContent from '@/src/components/common/TsvcRawContent';
import getGearInquiryFormFields from '@/src/forms/SimpleForms/models/gear-inquiry-form';
import wp from '@/src/utils/wp-api-client';

export async function getServerSideProps({ query }) {
  try {
    const [page] = await Promise.all([
      wp
        .pages()
        .slug('aparatura-wyslij-zapytanie')
        .then((data) => {
          return data[0];
        }),
    ]);

    return { props: { page, item: query.item } };
  } catch (err) {
    console.log(err);
  }

  return {};
}

export default function GearSendInquiry({ breadcrumbs, page, item }) {
  const banner = page.acf.main_banner;
  const seoContent = page.acf && page.acf.seo_content;
  const formFields = getGearInquiryFormFields();

  return (
    <>
      <TsvcMainBanner
        image={banner.image}
        title={banner.title}
        subtitle={banner.subtitle}
      />

      <PageSection className="breadcrumbs">
        <TsvcBreadcrumbs items={breadcrumbs} />
      </PageSection>

      {seoContent && (
        <PageSection noPaddingBottom>
          <TsvcRawContent>
            <article dangerouslySetInnerHTML={{ __html: seoContent }} />
          </TsvcRawContent>
        </PageSection>
      )}

      <PageSection smallPaddingTop>
        <div className="row">
          <div className="col-xs-12 col-md-8 col-md-offset-2">
            <h3 dangerouslySetInnerHTML={{ __html: item }} />
            <TsvcForm
              title="Wyślij zapytanie"
              fields={formFields}
              url="/api/gear-form"
              hiddenFields={{
                item: item.replace(/<[^>]*>?/gm, ''),
              }}
            />
          </div>
        </div>
      </PageSection>
    </>
  );
}

import React from 'react';
import TsvcMainBanner from '@/src/components/common/TsvcMainBanner';
import TsvcBreadcrumbs from '@/src/components/common/TsvcBreadcrumbs';
import PageSection from '@/src/components/common/PageSection';
import TsvcManufacturersList from '@/src/components/common/TsvcManufacturersList';
import TsvcRawContent from '@/src/components/common/TsvcRawContent';
import wp from '@/src/utils/wp-api-client';
import TsvcSeoContent, {
  getSeoContentFromPage,
} from '@/src/components/TsvcSeoContent/TsvcSeoContent';

export async function getServerSideProps() {
  try {
    const [page] = await Promise.all([
      wp
        .pages()
        .slug('aparatura-producenci')
        .then((data) => {
          return data[0];
        }),
    ]);

    const manufacturers = page.acf.manufacturers.items.map(
      ({ image, ...rest }) => ({
        ...rest,
      }),
    );

    return { props: { page, manufacturers } };
  } catch (err) {
    console.log(err);
  }

  return {};
}

export default function GearManufacturers({
  breadcrumbs,
  page,
  manufacturers,
}) {
  const banner = page.acf.main_banner;
  const seoContent = getSeoContentFromPage(page);

  return (
    <>
      <TsvcMainBanner image={banner.image} title={banner.title} />

      <PageSection className="breadcrumbs">
        <TsvcBreadcrumbs items={breadcrumbs} />
      </PageSection>

      {seoContent && (
        <PageSection smallPaddingBottom bg="#f1f6fb">
          <TsvcRawContent>
            <TsvcSeoContent {...seoContent} />
          </TsvcRawContent>
        </PageSection>
      )}

      <TsvcManufacturersList items={manufacturers} />
    </>
  );
}

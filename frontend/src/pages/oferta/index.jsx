import React from 'react';
import TsvcMainBanner from '@/src/components/common/TsvcMainBanner';
import TsvcContent03 from '@/src/components/common/TsvcContent03';
import TsvcBreadcrumbs from '@/src/components/common/TsvcBreadcrumbs';
import TsvcContentBoxes from '@/src/components/common/TsvcContentBoxes';
import PageSection from '@/src/components/common/PageSection';
import TsvcRawContent from '@/src/components/common/TsvcRawContent';
import TsvcSeoContent, {
  getSeoContentFromPage,
} from '@/src/components/TsvcSeoContent/TsvcSeoContent';
import wp from '@/src/utils/wp-api-client';

export async function getServerSideProps() {
  try {
    const [page] = await Promise.all([
      wp
        .pages()
        .slug('oferta')
        .embed()
        .then((data) => {
          return data[0];
        }),
    ]);

    const offer = {
      title: page.acf.offer_section.title,
      items: page.acf.offer_section.items.map(({ link, ...item }) => ({
        ...item,
        link: {
          ...link,
          as: link.url,
          href: link.url,
        },
        label: link.title,
      })),
    };

    return { props: { page, offer } };
  } catch (err) {
    console.log(err);
  }

  return {};
}

export default function Offer({ page, offer, breadcrumbs }) {
  const banner = page.acf.main_banner;
  const seoContent = getSeoContentFromPage(page);

  return (
    <>
      <TsvcMainBanner image={banner.image} title={banner.title} />

      <PageSection className="breadcrumbs">
        <TsvcBreadcrumbs items={breadcrumbs} />
      </PageSection>

      <PageSection>
        <TsvcContentBoxes
          title={offer.title}
          items={offer.items}
          animated
          invertedAnimation
        />
      </PageSection>

      {seoContent && (
        <PageSection smallPaddingBottom bg="#f1f6fb">
          <TsvcRawContent>
            <TsvcSeoContent {...seoContent} />
          </TsvcRawContent>
        </PageSection>
      )}

      <PageSection>
        <TsvcContent03 title={page.acf.content03.title}>
          <div
            dangerouslySetInnerHTML={{ __html: page.acf.content03.content }}
          />
        </TsvcContent03>
      </PageSection>
    </>
  );
}

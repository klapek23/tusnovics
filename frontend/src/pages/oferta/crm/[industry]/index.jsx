import React from 'react';
import TsvcMainBanner from '@/src/components/common/TsvcMainBanner';
import TsvcBreadcrumbs from '@/src/components/common/TsvcBreadcrumbs';
import TsvcContentBoxes from '@/src/components/common/TsvcContentBoxes';
import TsvcCMOiWSerachForm from '@/src/forms/TsvcCMOiWSerachForm/TsvcCMOiWSerachForm';
import PageSection from '@/src/components/common/PageSection';
import slugify from '@/src/utils/slugify';
import TsvcLink from '@/src/components/common/TsvcLink';
import TsvcButton from '@/src/components/common/TsvcButton';
import TsvcRawContent from '@/src/components/common/TsvcRawContent';
import TsvcSeoContent, {
  getSeoContentFromPage,
} from '@/src/components/TsvcSeoContent/TsvcSeoContent';
import wp from '@/src/utils/wp-api-client';

export async function getServerSideProps({ query }) {
  try {
    const { industry } = query;
    const [page] = await Promise.all([
      wp
        .cmoiw()
        .slug(industry)
        .embed()
        .then((data) => {
          return data[0];
        }),
    ]);

    const boxes = {
      title: 'Kategorie',
      items: page.acf.industry_categories.items.map(({ ...item }) => ({
        ...item,
        link: {
          as: `/oferta/crm/${industry}/${slugify(item.title)}`,
          href: `/oferta/crm/[industry]/[category]`,
          title: item.title,
        },
      })),
    };

    return { props: { page, boxes } };
  } catch (err) {
    console.log(err);
  }

  return {};
}

export default function CRMIndustry({ breadcrumbs, page, boxes }) {
  const banner = page.acf.main_banner;
  const seoContent = getSeoContentFromPage(page);

  return (
    <>
      <TsvcMainBanner image={banner.image} title={banner.title} />

      <PageSection className="breadcrumbs" bg="#f1f6fb">
        <TsvcBreadcrumbs items={breadcrumbs} />
      </PageSection>

      <PageSection bg="#f1f6fb" smallPaddingTop>
        <div className="row">
          <div className="col-xs-12 col-md-10 col-md-offset-1 col-xl-8 col-xl-offset-2">
            <TsvcCMOiWSerachForm
              title="Wyszukaj"
              link={{
                href: '/oferta/crm/wyniki-wyszukiwania',
                as: '/oferta/crm/wyniki-wyszukiwania',
              }}
            />
          </div>
        </div>
      </PageSection>

      <PageSection>
        <TsvcContentBoxes
          title={boxes.title}
          items={boxes.items}
          narrow
          renderFooter={() => (
            <TsvcLink as="/oferta/crm" href="/oferta/crm" title="Wróć">
              <TsvcButton>Wróć</TsvcButton>
            </TsvcLink>
          )}
        />
      </PageSection>

      {seoContent && (
        <PageSection smallPaddingBottom bg="#f1f6fb">
          <TsvcRawContent>
            <TsvcSeoContent {...seoContent} />
          </TsvcRawContent>
        </PageSection>
      )}
    </>
  );
}

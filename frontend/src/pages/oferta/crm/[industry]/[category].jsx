import React from 'react';
import Head from 'next/head';
import TsvcMainBanner from '@/src/components/common/TsvcMainBanner';
import TsvcBreadcrumbs from '@/src/components/common/TsvcBreadcrumbs';
import TsvcCMOiWSerachForm from '@/src/forms/TsvcCMOiWSerachForm/TsvcCMOiWSerachForm';
import PageSection from '@/src/components/common/PageSection';
import slugify from '@/src/utils/slugify';
import TsvcContent04 from '@/src/components/common/TsvcContent04';
import TsvcRawContent from '@/src/components/common/TsvcRawContent';
import TsvcSeoContent, {
  getSeoContentFromPage,
} from '@/src/components/TsvcSeoContent/TsvcSeoContent';
import wp from '@/src/utils/wp-api-client';

export async function getServerSideProps({ query }) {
  try {
    const { industry, category } = query;
    const [page] = await Promise.all([
      wp
        .cmoiw()
        .slug(industry)
        .embed()
        .then((data) => {
          return data[0];
        }),
    ]);

    const categoryData = page.acf.industry_categories.items.find(
      ({ title }) => slugify(title) === category,
    );

    return { props: { page, industry, category: categoryData } };
  } catch (err) {
    console.log(err);
  }

  return {};
}

export default function CRMIndustryCategory({
  category,
  industry,
  breadcrumbs,
  page,
}) {
  const banner = page.acf.main_banner;
  const { seo } = category;
  const seoContent = getSeoContentFromPage(page);

  return (
    <>
      {seo && (
        <Head>
          <title>{seo.title}</title>
          <meta name="description" content={seo.description} />
          <meta name="keywords" content={seo.keywords} />
        </Head>
      )}

      <TsvcMainBanner image={banner.image} title={category.title} />

      <PageSection className="breadcrumbs" bg="#f1f6fb">
        <TsvcBreadcrumbs items={breadcrumbs} />
      </PageSection>

      <PageSection bg="#f1f6fb" smallPaddingTop>
        <div className="row">
          <div className="col-xs-12 col-md-10 col-md-offset-1 col-xl-8 col-xl-offset-2">
            <TsvcCMOiWSerachForm
              title="Wyszukaj"
              link={{
                href: '/oferta/crm/wyniki-wyszukiwania',
                as: '/oferta/crm/wyniki-wyszukiwania',
              }}
            />
          </div>
        </div>
      </PageSection>

      <PageSection>
        <TsvcContent04
          title={category.title}
          content={category.description}
          bg_color_1={category.bg_color_1}
          bg_color_2={category.bg_color_2}
          buttons={category.catalogs}
          link={{
            as: `/oferta/crm/${industry}`,
            href: `/oferta/crm/[industry]`,
            title: 'Wróć',
          }}
        />
      </PageSection>

      {seoContent && (
        <PageSection smallPaddingBottom bg="#f1f6fb">
          <TsvcRawContent>
            <TsvcSeoContent {...seoContent} />
          </TsvcRawContent>
        </PageSection>
      )}
    </>
  );
}

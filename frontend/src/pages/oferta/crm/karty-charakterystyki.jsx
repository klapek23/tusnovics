import React, { useState } from 'react';
import axios from 'axios';

import Config from '@/config';
import wp from '@/src/utils/wp-api-client';

import getUser from '@/src/utils/getUser';
import TsvcSearchDetailsCardForm from '@/src/forms/OneFieldForms/TsvcSearchDetailsCardForm';
import TsvcLoginForm from '@/src/forms/OneFieldForms/TsvcLoginForm';
import TsvcMainBanner from '@/src/components/common/TsvcMainBanner';
import TsvcBreadcrumbs from '@/src/components/common/TsvcBreadcrumbs';
import PageSection from '@/src/components/common/PageSection';
import TsvcMSDSResults from '@/src/components/TsvcMSDSResults';
import TsvcRawContent from '@/src/components/common/TsvcRawContent';
import TsvcSeoContent, {
  getSeoContentFromPage,
} from '@/src/components/TsvcSeoContent/TsvcSeoContent';

async function fetchFiles(searchQuery, ssr) {
  const searchUrl = (parsedValue) => `/api/crm/msds?search=${parsedValue}`;
  const parsedValue = searchQuery
    ? searchQuery.trim().toLowerCase()
    : undefined;
  if (parsedValue) {
    return ssr
      ? axios.get(searchUrl(parsedValue), { baseURL: Config.frontendUrl })
      : axios.get(searchUrl(parsedValue));
  }

  return Promise.resolve({ data: [] });
}

export async function getServerSideProps({ query, req, res }) {
  const { search } = query;
  const ssr = !!req;

  try {
    const [page, files, user] = await Promise.all([
      wp
        .pages()
        .slug('karty-charakterystyki')
        .embed()
        .then((data) => {
          return data[0];
        }),
      fetchFiles(search, ssr),
      getUser({ req, res }),
    ]);

    return { props: { page, files: files.data, user: user ?? null, search } };
  } catch (err) {
    console.log(err);
  }

  return {};
}

export default function CRMMSDS({ breadcrumbs, page, search, files, user }) {
  const banner = page.acf.main_banner;
  const seoContent = getSeoContentFromPage(page);
  const [requestedFile, setRequestedFile] = useState(() => ({}));
  const [showLoginForm, setShowLoginForm] = useState(false);
  const [loggedIn, setLoggedIn] = useState(!!user);

  const handleLoginClick = (path, name) => {
    setShowLoginForm(true);
    setRequestedFile({ path, name });
  };

  const handleUserLogin = async (userData) => {
    if (userData?.ok) {
      setLoggedIn(true);
      setShowLoginForm(false);

      const response = await axios.get('/api/msds', {
        params: { ...requestedFile },
        responseType: 'blob',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      });
      const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement('a');
      link.href = url;
      link.setAttribute('download', requestedFile.name);
      document.body.appendChild(link);
      link.click();
      link.remove();
    }
  };

  const handleUserLoginClose = () => {
    setShowLoginForm(false);
  };

  return (
    <>
      <TsvcMainBanner image={banner.image} title={banner.title} />

      <PageSection className="breadcrumbs" bg="#f1f6fb">
        <TsvcBreadcrumbs items={breadcrumbs} />
      </PageSection>

      {seoContent && (
        <PageSection smallPaddingBottom>
          <TsvcRawContent>
            <TsvcSeoContent {...seoContent} />
          </TsvcRawContent>
        </PageSection>
      )}

      <PageSection bg="#f1f6fb" smallPaddingTop>
        <div className="row">
          <div className="col-xs-12 col-md-10 col-md-offset-1 col-xl-8 col-xl-offset-2">
            <div className="tsvc-msds-form tsvc-content">
              <article>
                <h3>Wyszukaj</h3>
              </article>
              <TsvcSearchDetailsCardForm value={search} black />
            </div>
          </div>
        </div>
      </PageSection>

      <PageSection>
        <div className="row">
          <div className="col-xs-12 col-md-10 col-md-offset-1 col-xl-8 col-xl-offset-2">
            <TsvcMSDSResults
              items={files}
              loggedIn={loggedIn}
              onLoginClick={handleLoginClick}
            />
            {showLoginForm && (
              <TsvcLoginForm
                onUserLogin={handleUserLogin}
                onOutsideClick={handleUserLoginClose}
                onCloseClick={handleUserLoginClose}
              />
            )}
          </div>
        </div>
      </PageSection>
    </>
  );
}

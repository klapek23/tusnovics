import React from 'react';
import axios from 'axios';
import Config from '@/config';
import TsvcMainBanner from '@/src/components/common/TsvcMainBanner';
import TsvcBreadcrumbs from '@/src/components/common/TsvcBreadcrumbs';
import PageSection from '@/src/components/common/PageSection';
import TsvcCMOiWFormResultsContainer from '@/src/forms/TsvcCMOiWSerachForm/TsvcCMOiWSearchResults/TsvcCMOiWFormResultsContainer';
import TsvcRawContent from '@/src/components/common/TsvcRawContent';
import TsvcSeoContent, {
  getSeoContentFromPage,
} from '@/src/components/TsvcSeoContent/TsvcSeoContent';
import wp from '@/src/utils/wp-api-client';

async function fetchItems(url, searchQuery, limit, skip, ssr, type) {
  const escapedQuery = encodeURIComponent(searchQuery);
  const fullUrl = type
    ? `${url}/${type}/${escapedQuery}?skip=${skip}&limit=${limit}`
    : `${url}/search?search=${escapedQuery}&skip=${skip}&limit=${limit}`;
  return ssr
    ? axios.get(fullUrl, { baseURL: Config.frontendUrl })
    : axios.get(fullUrl);
}

export async function getServerSideProps({ req, query }) {
  const { type, value } = query;
  const currentPage = query.page || 1;
  const ssr = !!req;
  const limit = 10;
  const skip = (currentPage - 1) * limit;

  try {
    const [page, searchResults] = await Promise.all([
      wp
        .pages()
        .slug('cmoiw-wyniki-wyszukiwania')
        .embed()
        .then((data) => {
          return data[0];
        }),
      fetchItems('/api/crm', value, limit, skip, ssr, type),
    ]);

    const searchParams = { type: type ?? null, value: value ?? null };
    const totalPages = Math.ceil(searchResults.data.total / limit);

    return {
      props: {
        page,
        searchResults: searchResults.data ?? [],
        searchParams,
        totalPages,
        currentPage,
      },
    };
  } catch (err) {
    console.log(err);
  }

  return {};
}

export default function CRMSearchResults({
  page,
  breadcrumbs,
  searchResults,
  searchParams,
  currentPage,
  totalPages,
}) {
  const banner = page.acf.main_banner;
  const seoContent = getSeoContentFromPage(page);

  const searchQuery = {
    ...(searchParams && searchParams.type && { type: searchParams.type }),
    ...(searchParams && searchParams.value && { value: searchParams.value }),
  };

  return (
    <>
      <TsvcMainBanner
        image={banner.image}
        title={`${banner.title}`}
        subtitle={banner.subtitle}
      />

      <PageSection className="breadcrumbs" smallPaddingTop>
        <TsvcBreadcrumbs items={breadcrumbs} />
      </PageSection>

      <TsvcCMOiWFormResultsContainer
        title="Wyniki wyszukiwania"
        items={searchResults.items}
        totalItems={searchResults.total}
        searchParams={searchParams}
        pathname="/oferta/crm/wyniki-wyszukiwania"
        query={searchQuery}
        totalPages={totalPages}
        currentPage={currentPage}
      />

      {seoContent && (
        <PageSection smallPaddingBottom bg="#f1f6fb">
          <TsvcRawContent>
            <TsvcSeoContent {...seoContent} />
          </TsvcRawContent>
        </PageSection>
      )}
    </>
  );
}

import React from 'react';
import TsvcMainBanner from '@/src/components/common/TsvcMainBanner';
import TsvcBreadcrumbs from '@/src/components/common/TsvcBreadcrumbs';
import PageSection from '@/src/components/common/PageSection';
import TsvcCMOiWFormStepsContainer from '@/src/forms/TsvcCMOiWSerachForm/TsvcCMOiWSearchResults/TsvcCMOiWFormStepsContainer';
import TsvcRawContent from '@/src/components/common/TsvcRawContent';
import wp from '@/src/utils/wp-api-client';

export async function getServerSideProps({ query }) {
  const { step } = query;

  try {
    const [page] = await Promise.all([
      wp
        .pages()
        .slug('cmoiw-wyniki-wyszukiwania')
        .embed()
        .then((data) => {
          return data[0];
        }),
    ]);

    return { props: { page, step: step ?? 1 } };
  } catch (err) {
    console.log(err);
  }

  return {};
}

export default function CRMSearchResultsStep({ page, breadcrumbs, step }) {
  const banner = page.acf.main_banner;
  const seoContent = page.acf && page.acf.seo_content;

  return (
    <>
      <TsvcMainBanner
        image={banner.image}
        title={`${banner.title}`}
        subtitle={banner.subtitle}
      />

      <PageSection className="breadcrumbs" smallPaddingTop>
        <TsvcBreadcrumbs items={breadcrumbs} />
      </PageSection>

      {seoContent && (
        <PageSection noPaddingBottom>
          <TsvcRawContent>
            <article dangerouslySetInnerHTML={{ __html: seoContent }} />
          </TsvcRawContent>
        </PageSection>
      )}

      <TsvcCMOiWFormStepsContainer
        title="Wyniki wyszukiwania"
        step={parseInt(step, 10)}
      />
    </>
  );
}

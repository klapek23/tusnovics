import React, { useEffect } from 'react';
import TsvcMainBanner from '@/src/components/common/TsvcMainBanner';
import TsvcBreadcrumbs from '@/src/components/common/TsvcBreadcrumbs';
import TsvcContentBoxes from '@/src/components/common/TsvcContentBoxes';
import TsvcCMOiWSerachForm from '@/src/forms/TsvcCMOiWSerachForm/TsvcCMOiWSerachForm';
import PageSection from '@/src/components/common/PageSection';
import TsvcManufacturers from '@/src/components/common/TsvcManufactuers';
import TsvcRawContent from '@/src/components/common/TsvcRawContent';
import TsvcSeoContent, {
  getSeoContentFromPage,
} from '@/src/components/TsvcSeoContent/TsvcSeoContent';
import wp from '@/src/utils/wp-api-client';

export const localStorageKey = 'tsvcCMOiWSelectedItems';

export async function getServerSideProps() {
  try {
    const [page, manufacturers] = await Promise.all([
      wp
        .pages()
        .slug('crm')
        .embed()
        .then((data) => {
          return data[0];
        }),
      wp.manufacturers().perPage(100),
    ]);

    const boxes = {
      title: page.acf.cmoiw_industries.title,
      items: page.acf.cmoiw_industries.industries.map(
        ({ slug, custom_slug_on, custom_slug, ...item }) => {
          return {
            ...item,
            link: {
              as: custom_slug_on
                ? `/oferta/crm/${custom_slug}`
                : `/oferta/crm/${slug}`,
              href: custom_slug_on
                ? `/oferta/crm/${slug}`
                : '/oferta/crm/[industry]',
              title: item.label,
            },
            title: item.title ?? null,
          };
        },
      ),
    };

    const mappedManufacturers = manufacturers
      .filter(({ acf }) => !!acf.show_in_bottom_bar)
      .map(({ title, content, acf }) => ({
        title: title.rendered,
        content: content.rendered,
        ...acf,
      }));

    return { props: { page, manufacturers: mappedManufacturers, boxes } };
  } catch (err) {
    console.log(err);
  }

  return {};
}

export default function CRM({ breadcrumbs, page, manufacturers, boxes }) {
  useEffect(() => {
    window.sessionStorage.removeItem(localStorageKey);
  }, []);

  const banner = page.acf.main_banner;
  const seoContent = getSeoContentFromPage(page);

  return (
    <>
      <TsvcMainBanner image={banner.image} title={banner.title} />

      <PageSection className="breadcrumbs" bg="#f1f6fb">
        <TsvcBreadcrumbs items={breadcrumbs} />
      </PageSection>

      <PageSection bg="#f1f6fb" smallPaddingTop>
        <div className="row">
          <div className="col-xs-12 col-md-10 col-md-offset-1 col-xl-8 col-xl-offset-2">
            <TsvcCMOiWSerachForm
              title="Wyszukaj"
              link={{
                href: '/oferta/crm/wyniki-wyszukiwania',
                as: '/oferta/crm/wyniki-wyszukiwania',
              }}
            />
          </div>
        </div>
      </PageSection>

      {seoContent && (
        <PageSection smallPaddingBottom>
          <TsvcRawContent>
            <TsvcSeoContent {...seoContent} />
          </TsvcRawContent>
        </PageSection>
      )}

      <PageSection>
        <TsvcContentBoxes title={boxes.title} items={boxes.items} animated />
      </PageSection>

      <PageSection bg="#f1f6fb">
        <TsvcManufacturers
          title="Producenci"
          items={manufacturers}
          url="/oferta/crm/producenci"
          buttonLabel="Zobacz więcej"
        />
      </PageSection>
    </>
  );
}

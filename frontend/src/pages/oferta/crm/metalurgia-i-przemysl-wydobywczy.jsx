import React from 'react';
import TsvcCMOiWSerachForm from '@/src/forms/TsvcCMOiWSerachForm/TsvcCMOiWSerachForm';
import TsvcMainBanner from '@/src/components/common/TsvcMainBanner';
import TsvcBreadcrumbs from '@/src/components/common/TsvcBreadcrumbs';
import PageSection from '@/src/components/common/PageSection';
import TsvcContent04 from '@/src/components/common/TsvcContent04';
import TsvcRawContent from '@/src/components/common/TsvcRawContent';
import TsvcSeoContent, {
  getSeoContentFromPage,
} from '@/src/components/TsvcSeoContent/TsvcSeoContent';
import wp from '@/src/utils/wp-api-client';

export async function getServerSideProps() {
  try {
    const [page] = await Promise.all([
      wp
        .pages()
        .slug('metalurgia-i-przemysl-wydobywczy')
        .embed()
        .then((data) => {
          return data[0];
        }),
    ]);

    return { props: { page } };
  } catch (err) {
    console.log(err);
  }

  return {};
}

export default function CRMMetallurgy({ breadcrumbs, page }) {
  const banner = page.acf.main_banner;
  const seoContent = getSeoContentFromPage(page);

  return (
    <>
      <TsvcMainBanner image={banner.image} title={banner.title} />

      <PageSection className="breadcrumbs" bg="#f1f6fb">
        <TsvcBreadcrumbs items={breadcrumbs} />
      </PageSection>

      {seoContent && (
        <PageSection smallPaddingBottom bg="#f1f6fb">
          <TsvcRawContent>
            <TsvcSeoContent {...seoContent} />
          </TsvcRawContent>
        </PageSection>
      )}

      <PageSection bg="#f1f6fb" smallPaddingTop>
        <div className="row">
          <div className="col-xs-12 col-md-10 col-md-offset-1 col-xl-8 col-xl-offset-2">
            <TsvcCMOiWSerachForm
              title="Wyszukaj"
              link={{
                href: '/oferta/crm/wyniki-wyszukiwania',
                as: '/oferta/crm/wyniki-wyszukiwania',
              }}
            />
          </div>
        </div>
      </PageSection>

      <PageSection>
        <TsvcContent04
          title={page.title.rendered}
          content={page.content.rendered}
          buttons={page.acf.cmoiw_custom.catalogs}
          bg_color_1={page.acf.cmoiw_custom.bg_color_1}
          bg_color_2={page.acf.cmoiw_custom.bg_color_2}
          link={{ as: `/oferta/crm`, href: `/oferta/crm`, title: 'Wróć' }}
        />
      </PageSection>
    </>
  );
}

import React from 'react';
import TsvcMainBanner from '@/src/components/common/TsvcMainBanner';
import TsvcBreadcrumbs from '@/src/components/common/TsvcBreadcrumbs';
import PageSection from '@/src/components/common/PageSection';
import TsvcManufacturersList from '@/src/components/common/TsvcManufacturersList';
import TsvcRawContent from '@/src/components/common/TsvcRawContent';
import TsvcSeoContent, {
  getSeoContentFromPage,
} from '@/src/components/TsvcSeoContent/TsvcSeoContent';
import wp from '@/src/utils/wp-api-client';

export async function getServerSideProps() {
  try {
    const [page, manufacturers] = await Promise.all([
      wp
        .pages()
        .slug('cmoiw-producenci')
        .then((data) => {
          return data[0];
        }),
      wp.manufacturers().orderby('title').order('asc').perPage(100),
    ]);

    const mappedManufacturers = manufacturers.map(
      ({ title, content, acf }) => ({
        title: title.rendered,
        content: content.rendered,
        ...acf,
      }),
    );

    return { props: { page, manufacturers: mappedManufacturers } };
  } catch (err) {
    console.log(err);
  }

  return {};
}

export default function CRMManufacturers({ breadcrumbs, page, manufacturers }) {
  const banner = page.acf.main_banner;
  const seoContent = getSeoContentFromPage(page);

  return (
    <>
      <TsvcMainBanner image={banner.image} title={banner.title} />

      <PageSection className="breadcrumbs">
        <TsvcBreadcrumbs items={breadcrumbs} />
      </PageSection>

      {seoContent && (
        <PageSection smallPaddingBottom bg="#f1f6fb">
          <TsvcRawContent>
            <TsvcSeoContent {...seoContent} />
          </TsvcRawContent>
        </PageSection>
      )}

      <TsvcManufacturersList items={manufacturers} />
    </>
  );
}

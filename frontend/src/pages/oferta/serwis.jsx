import React from 'react';
import TsvcMainBanner from '@/src/components/common/TsvcMainBanner';
import TsvcBreadcrumbs from '@/src/components/common/TsvcBreadcrumbs';
import PageSection from '@/src/components/common/PageSection';
import TsvcRawContent from '@/src/components/common/TsvcRawContent';
import TsvcBusinessCard from '@/src/components/common/TsvcBusinessCard';
import TsvcSeoContent, {
  getSeoContentFromPage,
} from '@/src/components/TsvcSeoContent/TsvcSeoContent';
import wp from '@/src/utils/wp-api-client';
import TsvcServiceForm from '@/src/forms/SimpleForms/TsvcService';

export async function getServerSideProps() {
  try {
    const [page] = await Promise.all([
      wp
        .pages()
        .slug('serwis')
        .embed()
        .then((data) => {
          return data[0];
        }),
    ]);

    return { props: { page } };
  } catch (err) {
    console.log(err);
  }

  return {};
}

export default function Service({ page, breadcrumbs }) {
  const banner = page.acf.main_banner;
  const seoContent = getSeoContentFromPage(page);
  const formTitle = 'Zgłoszenie serwisowe';
  const formSubmitUrl = '/api/service-form';

  return (
    <>
      <TsvcMainBanner
        image={banner.image}
        title={`${banner.title}`}
        subtitle={banner.subtitle}
      />

      <PageSection className="breadcrumbs">
        <TsvcBreadcrumbs items={breadcrumbs} />
      </PageSection>

      {seoContent && (
        <PageSection smallPaddingBottom bg="#f1f6fb">
          <TsvcRawContent>
            <TsvcSeoContent {...seoContent} />
          </TsvcRawContent>
        </PageSection>
      )}

      <PageSection smallPaddingTop>
        <div className="row">
          <div className="col-xs-12 col-lg-6">
            <TsvcRawContent title={page.title.rendered}>
              <article
                dangerouslySetInnerHTML={{ __html: page.content.rendered }}
              />
            </TsvcRawContent>

            <TsvcBusinessCard items={page.acf.business_card.items} />
          </div>

          <div className="col-xs-12 col-lg-5 col-lg-offset-1">
            <div className="tsvc-gear-search-bg">
              <div className="tsvc-gear-search-bg__content" />
              <div className="tsvc-gear-search__content">
                <TsvcServiceForm />
              </div>
            </div>
          </div>
        </div>
      </PageSection>
    </>
  );
}

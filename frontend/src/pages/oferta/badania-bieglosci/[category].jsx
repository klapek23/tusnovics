import React from 'react';
import TsvcMainBanner from '@/src/components/common/TsvcMainBanner';
import TsvcBreadcrumbs from '@/src/components/common/TsvcBreadcrumbs';
import PageSection from '@/src/components/common/PageSection';
import TsvcContent04 from '@/src/components/common/TsvcContent04';
import TsvcRawContent from '@/src/components/common/TsvcRawContent';
import TsvcSeoContent, {
  getSeoContentFromPage,
} from '@/src/components/TsvcSeoContent/TsvcSeoContent';
import wp from '@/src/utils/wp-api-client';

export async function getServerSideProps({ query }) {
  try {
    const { category } = query;
    const [page] = await Promise.all([
      wp
        .bb_categories()
        .slug(category)
        .embed()
        .then((data) => {
          return data[0];
        }),
    ]);

    return { props: { page, category } };
  } catch (err) {
    console.log(err);
  }

  return {};
}

export default function ResearchCategory({ breadcrumbs, page }) {
  const banner = page.acf.main_banner;
  const seoContent = getSeoContentFromPage(page);

  return (
    <>
      <TsvcMainBanner image={banner.image} title={banner.title} />

      <PageSection className="breadcrumbs">
        <TsvcBreadcrumbs items={breadcrumbs} />
      </PageSection>

      {seoContent && (
        <PageSection smallPaddingBottom bg="#f1f6fb">
          <TsvcRawContent>
            <TsvcSeoContent {...seoContent} />
          </TsvcRawContent>
        </PageSection>
      )}

      <PageSection>
        <TsvcContent04
          title={page.title.rendered}
          content={page.content.rendered}
          bg_color_1={page.acf.badania_bieglosci_category.bg_color_1}
          bg_color_2={page.acf.badania_bieglosci_category.bg_color_2}
          buttons={page.acf.badania_bieglosci_category.catalogs}
          link={{
            as: '/oferta/badania-bieglosci',
            href: '/oferta/badania-bieglosci',
            title: 'Wróć',
          }}
        />
      </PageSection>
    </>
  );
}

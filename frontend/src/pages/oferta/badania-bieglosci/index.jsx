import React from 'react';
import TsvcMainBanner from '@/src/components/common/TsvcMainBanner';
import TsvcBreadcrumbs from '@/src/components/common/TsvcBreadcrumbs';
import TsvcContentBoxes from '@/src/components/common/TsvcContentBoxes';
import PageSection from '@/src/components/common/PageSection';
import TsvcManufacturers from '@/src/components/common/TsvcManufactuers';
import TsvcRawContent from '@/src/components/common/TsvcRawContent';
import TsvcSeoContent, {
  getSeoContentFromPage,
} from '@/src/components/TsvcSeoContent/TsvcSeoContent';
import wp from '@/src/utils/wp-api-client';

export async function getServerSideProps() {
  try {
    const [page, manufacturers] = await Promise.all([
      wp
        .pages()
        .slug('badania-bieglosci')
        .embed()
        .then((data) => {
          return data[0];
        }),
      wp.manufacturers().perPage(100),
    ]);

    const mappedManufacturers = manufacturers
      .filter(({ acf }) => !!acf.show_in_bottom_bar)
      .map(({ title, content, acf }) => ({
        title: title.rendered,
        content: content.rendered,
        ...acf,
      }));

    const boxes = {
      title: page.acf['badania_bieglosci_-_categories'].title,
      items: page.acf['badania_bieglosci_-_categories'].categories.map(
        ({ slug, ...item }) => ({
          ...item,
          link: {
            as: `/oferta/badania-bieglosci/${slug}`,
            href: '/oferta/badania-bieglosci/[category]',
            title: item.label,
          },
          title: item.title ?? null,
        }),
      ),
    };

    return { props: { page, manufacturers: mappedManufacturers, boxes } };
  } catch (err) {
    console.log(err);
  }

  return {};
}

export default function Research({ breadcrumbs, page, manufacturers, boxes }) {
  const banner = page.acf.main_banner;
  const seoContent = getSeoContentFromPage(page);

  return (
    <>
      <TsvcMainBanner image={banner.image} title={banner.title} />

      <PageSection className="breadcrumbs">
        <TsvcBreadcrumbs items={breadcrumbs} />
      </PageSection>

      {seoContent && (
        <PageSection smallPaddingBottom bg="#f1f6fb">
          <TsvcRawContent>
            <TsvcSeoContent {...seoContent} />
          </TsvcRawContent>
        </PageSection>
      )}

      <PageSection>
        <TsvcContentBoxes title={boxes.title} items={boxes.items} animated />
      </PageSection>

      <PageSection bg="#f1f6fb">
        <TsvcManufacturers
          title="Producenci"
          items={manufacturers}
          url="/oferta/badania-bieglosci/producenci"
          buttonLabel="Zobacz więcej"
        />
      </PageSection>
    </>
  );
}

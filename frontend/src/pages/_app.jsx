import React from 'react';
import { ThemeProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';

import wp from '@/src/utils/wp-api-client';
import theme from '@/src/utils/theme';
import config from '@/config';
import createBreadcrumbs from '@/src/utils/breadcrumbs';

import Head from '@/src/components/Header';
import Layout from '@/src/components/Layout';
import CookiesNotice from '@/src/components/CookiesNotice';
import TsvcSpinner from '@/src/components/common/TsvcSpinner';

import 'flexboxgrid2/flexboxgrid2.css';
import 'react-awesome-slider/dist/styles.css';
import '@/src/styles/external/wp-core-styles.css';
import '@/src/styles/external/hamburgers.min.css';
import '@/src/styles/style.scss';

const stripHtml = (text) => (text ? text.replace(/<[^>]*>?/gm, '') : '');

export default function MyApp({
  Component,
  pageProps,
  headerMenu,
  footerMenu,
  themeOptions,
  cookiesSettings,
  router,
}) {
  const seo =
    pageProps?.page?.acf && pageProps?.page?.acf?.seo
      ? pageProps.page.acf.seo
      : {};

  const pageTitle = `${config.title} - ${
    pageProps?.page?.title.rendered
      ? pageProps.page.title.rendered
          .replace(/<[^>]*>?/gm, '')
          .replace('&#038;', '&')
      : ''
  }`;

  const ogTags = {
    appId: themeOptions.fbAppId || '',
    title: stripHtml(pageProps.page.title.rendered),
    description: stripHtml(
      pageProps?.page?.excerpt
        ? pageProps.page.excerpt.rendered
        : `${pageProps.page.content.rendered.substring(0, 150)}...`,
    ),
    type: pageProps?.pagetype === 'post' ? 'article' : 'website',
    image:
      pageProps?.page?._embedded && pageProps.page._embedded['wp:featuredmedia']
        ? pageProps.page._embedded['wp:featuredmedia'][0]
        : '',
    locale: 'pl_PL',
    url: `https://tusnovics.pl${router.pathname}`,
  };

  const breadcrumbs = createBreadcrumbs(router.pathname, router.asPath);

  return (
    <>
      <Head seo={seo} ogTags={ogTags} pageTitle={pageTitle} />
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <Layout
          headerMenu={headerMenu}
          pathname={router.pathname}
          footer={{
            leftColumn: themeOptions.footerLeftColumn,
            copy: themeOptions.footerCopy,
            footerMenu,
          }}
        >
          <Component {...pageProps} breadcrumbs={breadcrumbs} />
        </Layout>
        <TsvcSpinner />
        {cookiesSettings && <CookiesNotice {...cookiesSettings} />}
      </ThemeProvider>
    </>
  );
}

MyApp.getInitialProps = async () => {
  const [headerMenu, footerMenu, themeOptions, cookiesSettings] =
    await Promise.all([
      wp.menus().id('header-menu'),
      wp.menus().id('footer-menu'),
      wp.themeOptions(),
      wp.cookiesSettings(),
    ]);

  return { headerMenu, themeOptions, footerMenu, cookiesSettings };
};

import React from 'react';
import TsvcMainBanner from '@/src/components/common/TsvcMainBanner';
import TsvcBreadcrumbs from '@/src/components/common/TsvcBreadcrumbs';
import PageSection from '@/src/components/common/PageSection';
import TsvcRawContent from '@/src/components/common/TsvcRawContent';
import TsvcInquiry from '@/src/forms/SimpleForms/TsvcInquiry';
import TsvcSeoContent, {
  getSeoContentFromPage,
} from '@/src/components/TsvcSeoContent/TsvcSeoContent';
import wp from '@/src/utils/wp-api-client';

export async function getServerSideProps() {
  try {
    const [page] = await Promise.all([
      wp
        .pages()
        .slug('ankieta')
        .embed()
        .then((data) => {
          return data[0];
        }),
    ]);

    return { props: { page } };
  } catch (err) {
    console.log(err);
  }

  return {};
}

export default function Questionnairre({ breadcrumbs, page }) {
  const banner = page.acf.main_banner;
  const seoContent = getSeoContentFromPage(page);

  return (
    <>
      <TsvcMainBanner image={banner.image} title={banner.title} />

      <PageSection className="breadcrumbs" bg="rgb(241, 246, 251)">
        <TsvcBreadcrumbs items={breadcrumbs} />
      </PageSection>

      {seoContent && (
        <PageSection smallPaddingBottom bg="#f1f6fb">
          <TsvcRawContent>
            <TsvcSeoContent {...seoContent} />
          </TsvcRawContent>
        </PageSection>
      )}

      <PageSection smallPaddingTop noPaddingBottom bg="rgb(241, 246, 251)">
        <div className="row">
          <div className="col-xs-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
            <TsvcRawContent title="Ankieta zadowolenia klienta">
              <p>
                Zapraszamy do udziału w Ankiecie zadowolenia klienta. Wybierz
                odpowiedzi oraz wpisz swoje uwagi.
              </p>
              <p>* Pola oznaczne gwiazdką są wymagane</p>
            </TsvcRawContent>
          </div>
        </div>
      </PageSection>

      <PageSection smallPaddingTop>
        <div className="row">
          <div className="col-xs-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
            <TsvcInquiry />
          </div>
        </div>
      </PageSection>
    </>
  );
}

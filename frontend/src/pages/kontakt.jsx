import React from 'react';
import TsvcMainBanner from '@/src/components/common/TsvcMainBanner';
import TsvcBreadcrumbs from '@/src/components/common/TsvcBreadcrumbs';
import PageSection from '@/src/components/common/PageSection';
import TsvcContactForm from '@/src/forms/SimpleForms/TsvcContactForm';
import TsvcContactBox from '@/src/components/TsvcContactBox';
import TsvcContactSections from '@/src/components/TsvcContactSections';
import TsvcRawContent from '@/src/components/common/TsvcRawContent';
import TsvcSeoContent, {
  getSeoContentFromPage,
} from '@/src/components/TsvcSeoContent/TsvcSeoContent';
import wp from '@/src/utils/wp-api-client';

export async function getServerSideProps() {
  try {
    const [page] = await Promise.all([
      wp
        .pages()
        .slug('kontakt')
        .embed()
        .then((data) => {
          return data[0];
        }),
    ]);

    return { props: { page } };
  } catch (err) {
    console.log(err);
  }

  return {};
}

export default function Contact({ breadcrumbs, page }) {
  const banner = page.acf.main_banner;
  const contactBox = page.acf.contactbox;
  const contactSections = page.acf.contact_sections;
  const cmoiwContactSections = page.acf.cmoiw_contact_sections;
  const seoContent = getSeoContentFromPage(page);

  return (
    <>
      <TsvcMainBanner image={banner.image} title={banner.title} />

      <PageSection className="breadcrumbs">
        <TsvcBreadcrumbs items={breadcrumbs} />
      </PageSection>

      <PageSection smallPaddingTop>
        <div className="row">
          <div className="col-xs-12 col-md-6 col-lg-offset-1 col-lg-5 col-xl-offset-1 col-xl-4">
            <TsvcContactBox title="Kontakt" subtitle={contactBox.subtitle}>
              <div dangerouslySetInnerHTML={{ __html: contactBox.content }} />
            </TsvcContactBox>
          </div>
          <div className="col-xs-12 col-md-6 col-lg-offset-1 col-lg-5 col-xl-offset-1 col-xl-4">
            <TsvcContactForm />
          </div>
        </div>
      </PageSection>

      {seoContent && (
        <PageSection smallPaddingBottom>
          <TsvcRawContent>
            <TsvcSeoContent {...seoContent} />
          </TsvcRawContent>
        </PageSection>
      )}

      <PageSection bg="#f1f6fb">
        <div className="row">
          <div className="col-xs-12 col-lg-offset-1 col-lg-10">
            <TsvcContactSections
              title={contactSections.title}
              items={contactSections.items}
            />

            <TsvcContactSections
              title={cmoiwContactSections.title}
              items={cmoiwContactSections.items}
              smallTitle
            />
          </div>
        </div>
      </PageSection>
    </>
  );
}

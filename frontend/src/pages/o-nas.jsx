import React from 'react';
import TsvcMainBanner from '@/src/components/common/TsvcMainBanner';
import TsvcBreadcrumbs from '@/src/components/common/TsvcBreadcrumbs';
import PageSection from '@/src/components/common/PageSection';
import TsvcRawContent from '@/src/components/common/TsvcRawContent';
import TsvcAboutSections from '@/src/components/TsvcAboutSections';
import TsvcBlueBar from '@/src/components/common/TsvcBlueBar';
import TsvcSeoContent, {
  getSeoContentFromPage,
} from '@/src/components/TsvcSeoContent/TsvcSeoContent';
import wp from '@/src/utils/wp-api-client';

export async function getServerSideProps() {
  try {
    const [page] = await Promise.all([
      wp
        .pages()
        .slug('o-nas')
        .embed()
        .then((data) => {
          return data[0];
        }),
    ]);

    return { props: { page } };
  } catch (err) {
    console.log(err);
  }

  return {};
}

export default function AboutUs({ page, breadcrumbs }) {
  const { main_banner: banner, sections, bottom_bar: bottomBar } = page.acf;
  const seoContent = getSeoContentFromPage(page);

  return (
    <>
      <TsvcMainBanner image={banner.image} title={banner.title} />

      <PageSection className="breadcrumbs">
        <TsvcBreadcrumbs items={breadcrumbs} />
      </PageSection>

      {seoContent && (
        <PageSection smallPaddingBottom bg="#f1f6fb">
          <TsvcRawContent>
            <TsvcSeoContent {...seoContent} />
          </TsvcRawContent>
        </PageSection>
      )}

      <PageSection smallPaddingTop noPaddingBottom>
        <div className="row">
          <div className="col-xs-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
            <TsvcRawContent>
              <article
                dangerouslySetInnerHTML={{ __html: page.content.rendered }}
              />
            </TsvcRawContent>
          </div>
        </div>
      </PageSection>

      <TsvcAboutSections sections={sections.items} />

      <TsvcBlueBar text={bottomBar.text} link={bottomBar.link} />
    </>
  );
}

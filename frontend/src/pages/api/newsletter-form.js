import validateForm from '@/src/backend/utils/validate-tsvc-form';

const mailchimpHttpClient = require('@mailchimp/mailchimp_marketing');

const MAILCHIMP_NEWSLETTER_LIST_ID = '2a026ea4fa';
const MAILCHIMP_NEWSLETTER_STATUS = 'subscribed';

export default async (req, res) => {
  // Validate fields
  const errors = validateForm(req.body);

  if (!errors) {
    mailchimpHttpClient.setConfig({
      apiKey: process.env.MAILCHIMP_API_KEY,
      server: process.env.MAILCHIMP_SERVER_PREFIX,
    });

    try {
      await mailchimpHttpClient.lists.addListMember(
        MAILCHIMP_NEWSLETTER_LIST_ID,
        {
          email_address: req.body.email,
          status: MAILCHIMP_NEWSLETTER_STATUS,
        },
      );

      res.json({ success: true });
    } catch (e) {
      console.error(e);
      res.json({ success: false, errors: { e } });
    }
  } else {
    res.json({ success: false, errors });
  }
};

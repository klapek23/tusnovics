import path from 'node:path';
import { handleValidatedForm } from '@/src/backend/utils/smtp';
import validateForm from '@/src/backend/utils/validate-tsvc-form';
import Config from '@/config';

const { smtp } = Config;
const basePath = path.join('./src/emails');
const emailTemplate = `${basePath}/contact-form.ejs`;

export default async (req, res) => {
  const errors = validateForm(req.body);

  return handleValidatedForm(
    res,
    req.body,
    (email) =>
      `Nowa wiadomość od ${email} - materiały produkowane na zamówienie`,
    emailTemplate,
    errors,
    ['inquiry@tusnovics.pl'], // ['klapek23@gmail.com']
    [],
    smtp.office,
  );
};

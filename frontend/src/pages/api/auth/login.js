import { withIronSessionApiRoute } from 'iron-session/next';
import sessionOptions from '@/src/backend/utils/sessionOptions';
import wp from '@/src/utils/wp-api-client';

export default withIronSessionApiRoute(async function loginRoute(req, res) {
  try {
    const user = await wp
      .login()
      .create({ username: 'tusnclient', password: req.body.password });

    req.session.user = user;
    await req.session.save();
    res.send({ ok: true });
  } catch (e) {
    res.status(e.response.status);
    res.send({ ok: false, error: e.message });
  }
}, sessionOptions);

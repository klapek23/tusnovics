import { withIronSessionApiRoute } from 'iron-session/next';
import sessionOptions from '@/src/backend/utils/sessionOptions';

export default withIronSessionApiRoute(function userRoute(req, res) {
  res.send({ user: req.session.user });
}, sessionOptions);

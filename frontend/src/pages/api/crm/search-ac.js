import uniqBy from 'lodash/uniqBy';
import sortBy from 'lodash/sortBy';
import connect from '@/src/backend/utils/mongodb';
import escapeRegExp from '@/src/utils/escapeRegex';

export default async (req, res) => {
  let results;
  const { query } = req;
  let { search } = query;
  search = search.toLowerCase();
  const inputLength = search ? search.length : 0;

  if (inputLength === 0) {
    return res.json([]);
  }

  const escapedSearch = escapeRegExp(search);

  try {
    const { db, client } = await connect();
    const collection = db.collection('cmoiws');

    const getItems = (type) => async () => {
      const options = {
        // projection: { _id: 0 }
      };
      const regexp = new RegExp(`.*${escapedSearch}.*`, 'gmi');
      const items = await collection
        .find({ [type]: { $regex: regexp } }, options)
        .toArray();
      return items.map((item) => ({
        type,
        value: item[type],
      }));
    };

    const getCASes = getItems('cas');
    const getNumbers = getItems('number');
    const getMaterials = getItems('material');

    const casItems = await getCASes();
    const numbers = await getNumbers();
    const materials = await getMaterials();

    results = [...casItems, ...numbers, ...materials];

    results = uniqBy(results, ({ value }) => value.trim());
    results = sortBy(results, [({ value }) => value]);

    client.close();
  } catch (error) {
    console.log(error);
  }

  return res.json(results);
};

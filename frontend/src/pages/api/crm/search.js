import connect from '@/src/backend/utils/mongodb';
import escapeRegExp from '@/src/utils/escapeRegex';
import sortWithPriorities from '@/src/backend/utils/crm-sorting';

export default async (req, res) => {
  const priorities = [
    'accustandard',
    'era - a waters company',
    'lipomed',
    'chiron',
    'rofa',
    'cannon',
    'paragon',
    'pharmaffiliates',
    'edqm',
    'usp',
    'undefined',
    'scpscience',
    'chemservice',
  ];

  const defaultOrder = ['cas', 'number', 'material'];

  let results;
  const { query } = req;
  const { limit, skip } = query;
  let { search } = query;
  search = decodeURIComponent(search.toLowerCase());
  const inputLength = search && search !== 'undefined' ? search.length : 0;

  if (inputLength === 0) {
    return res.json({
      items: [],
      total: 0,
    });
  }

  const searchArray = search
    .split('|')
    .map((value) => escapeRegExp(value).trim());
  const multisearch = searchArray.join('|');

  try {
    const { db } = await connect();
    const collection = db.collection('cmoiws');

    const getItems = async () => {
      const options = {
        // projection: { _id: 0 }
        sort: ['cas', 'number', 'material'],
        // ...(!!limit && { limit: parseInt(limit, 10) }),
        // ...(!!skip && { skip: parseInt(skip, 10) }),
      };

      if (searchArray.length > 1) {
        const cursor = collection.find(
          {
            $and: searchArray.map((item) => ({
              material: {
                $regex: new RegExp(`.*${item}.*`, 'gmi'),
              },
            })),
          },
          options,
        );

        const items = await cursor.toArray();
        const total = await cursor.count();

        const finalItems = sortWithPriorities(items, priorities, defaultOrder);

        return {
          items: finalItems.slice(
            parseInt(skip, 10),
            parseInt(skip, 10) + parseInt(limit, 10),
          ),
          total,
        };
      }

      const regexp = new RegExp(`.*${multisearch}.*`, 'gmi');
      const cursor = collection.find(
        {
          $or: [
            { cas: { $regex: regexp } },
            { number: { $regex: regexp } },
            { material: { $regex: regexp } },
          ],
        },
        options,
      );
      const items = await cursor.toArray();
      const total = await cursor.count();

      const finalItems = sortWithPriorities(items, priorities, defaultOrder);

      return {
        items: finalItems.slice(
          parseInt(skip, 10),
          parseInt(skip, 10) + parseInt(limit, 10),
        ),
        total,
      };
    };

    results = await getItems();

    // client.close();
  } catch (error) {
    console.log(error);
  }

  return res.json({
    items: results.items,
    total: results.total,
  });
};

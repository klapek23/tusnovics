import connect from '@/src/backend/utils/mongodb';
import escapeRegExp from '@/src/utils/escapeRegex';
import sortWithPriorities from '@/src/backend/utils/crm-sorting';

export default async (req, res) => {
  const priorities = [
    'accustandard',
    'era - a waters company',
    'lipomed',
    'chiron',
    'rofa',
    'cannon',
    'paragon',
    'pharmaffiliates',
    'edqm',
    'usp',
    'undefined',
    'scpscience',
    'chemservice',
  ];
  const defaultOrder = ['cas', 'number', 'material'];

  let results;
  const { query } = req;
  const { type, value, limit, skip } = query;
  const inputLength = value ? value.length : 0;

  if (inputLength === 0) {
    return res.json({
      items: [],
      total: 0,
    });
  }

  const escapedSearch = escapeRegExp(value).trim();

  try {
    const { db, client } = await connect();
    const collection = db.collection('cmoiws');

    const getItems = async (type) => {
      const options = {
        // projection: { _id: 0 }
        sort: [type],
        // ...(!!limit && { limit: parseInt(limit, 10) }),
        // ...(!!skip && { skip: parseInt(skip, 10) })
      };
      const regexp = new RegExp(`.*${escapedSearch}.*`, 'gmi');
      const cursor = collection.find({ [type]: { $regex: regexp } }, options);
      const items = await cursor.toArray();
      const total = await cursor.count();
      return {
        items,
        total,
      };
    };

    results = await getItems(type);

    results.items = sortWithPriorities(
      results.items,
      priorities,
      defaultOrder,
    ).slice(parseInt(skip, 10), parseInt(skip, 10) + parseInt(limit, 10));

    client.close();
  } catch (error) {
    console.log(error);
  }

  return res.json({
    items: results.items,
    total: results.total,
  });
};

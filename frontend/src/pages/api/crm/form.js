import path from 'path';
import { handleValidatedForm, sendEmail } from '@/src/backend/utils/smtp';
import validateForm from '@/src/backend/utils/validate-tsvc-form';
import Config from '@/config';

const { smtp } = Config;
const basePath = path.join('./src/emails');
const emailTemplate = `${basePath}/crm-form.ejs`;

export default async (req, res) => {
  const errors = validateForm(req.body);

  try {
    await handleValidatedForm(
      res,
      req.body,
      (email) => `Nowa wiadomość od ${email} - formularz CMOiW`,
      emailTemplate,
      errors,
      ['inquiry@tusnovics.pl'], // ['klapek23@gmail.com']
      [],
      smtp.office,
      async (res, transporter) => {
        try {
          await sendEmail({
            sendFromEmail: smtp.office.smtpEmail,
            sendFromName: smtp.office.smtpName,
            sendToEmail: [req.body.email], // ['klapek23@gmail.com']
            subject: 'Tusnovics - potwierdzenie wysłania zapytania',
            formData: req.body,
            templatePath: emailTemplate,
            transporter,
          });

          return res.json({ success: true });
        } catch (e) {
          // console.log(e);
        }
      },
    );
  } catch (e) {
    console.log(e);
    return res.json({ success: false, errors: e });
  }
};

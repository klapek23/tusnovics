import sortBy from 'lodash/sortBy';
import dirTree from 'directory-tree';

export default async (req, res) => {
  let results;
  const { query } = req;
  const { search } = query;
  const inputLength = search ? search.length : 0;

  if (inputLength === 0) {
    return res.json([]);
  }

  try {
    results = dirTree('./src/karty-charakterystyki').children;

    // filter files
    results = results.filter(({ name }) =>
      name.trim().toLowerCase().includes(search.trim().toLowerCase()),
    );

    results = sortBy(results, [({ name }) => name]);
  } catch (error) {
    console.log(error);
  }

  return res.json(results);
};

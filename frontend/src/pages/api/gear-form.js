import path from 'node:path';
import { handleValidatedForm, sendEmail } from '@/src/backend/utils/smtp';
import validateForm from '@/src/backend/utils/validate-tsvc-form';
import Config from '@/config';

const { smtp } = Config;
const basePath = path.join('./src/emails');
const emailTemplate = `${basePath}/gear-form.ejs`;

export default async (req, res) => {
  const errors = validateForm(req.body);

  return handleValidatedForm(
    res,
    req.body,
    (email) => `Nowa wiadomość od ${email} - aparatura`,
    emailTemplate,
    errors,
    ['office@tusnovics.pl'], // ['klapek23@gmail.com']
    [],
    smtp.office,
    (res, transporter) => {
      sendEmail({
        sendFromEmail: smtp.office.smtpEmail,
        sendFromName: smtp.office.smtpName,
        sendToEmail: [req.body.email], // ['klapek23@gmail.com']
        subject: 'Tusnovics - potwierdzenie wysłania zapytania',
        formData: req.body,
        templatePath: emailTemplate,
        transporter,
      });

      res.json({ success: true });
    },
  );
};

import Captchapng from 'captchapng2';
import { withIronSessionApiRoute } from 'iron-session/next';
import sessionOptions from '@/src/backend/utils/sessionOptions';

export default withIronSessionApiRoute(async function captchaRoute(req, res) {
  const rand = parseInt(Math.random() * 9000 + 1000, 10);
  const png = new Captchapng(200, 100, rand);

  req.session.captcha = rand;
  await req.session.save();

  res.writeHead(200, { 'Content-Type': 'image/png' });
  res.end(png.getBuffer());
}, sessionOptions);

import connect from '@/src/backend/utils/mongodb';
import escapeRegExp from '@/src/utils/escapeRegex';

export default async (req, res) => {
  let results;
  const { query } = req;
  const { manufacturer, norm, method, category } = query;

  if (
    (!manufacturer || manufacturer.length === 0) &&
    (!norm || norm.length === 0) &&
    (!method || method.length === 0) &&
    (!category || category.length === 0)
  ) {
    return res.json({
      items: [],
      total: 0,
    });
  }

  try {
    const { db, client } = await connect();
    const collection = db.collection('gears');

    const getItems = async () => {
      const dbQuery = [];
      if (manufacturer) {
        dbQuery.push({
          manufacturer: escapeRegExp(manufacturer).trim(),
        });
      }
      if (norm) {
        dbQuery.push({
          norm: escapeRegExp(norm).trim(),
        });
      }
      if (method) {
        dbQuery.push({
          method: escapeRegExp(method).trim(),
        });
      }
      if (category) {
        dbQuery.push({
          category: escapeRegExp(category).trim(),
        });
      }

      const cursor = collection.aggregate([
        {
          $project: {
            name: '$name',
            description: '$description',
            url: '$url',
            catalogs: '$catalogs',
            video: '$video',
            pdf: '$pdf',
            image: '$image',
            manufacturer: '$manufacturer',
            norm: '$norm',
            method: '$method',
            category: '$category',
          },
        },
        { $match: { $and: dbQuery } },
      ]);
      const items = await cursor.toArray();
      const total = 20;

      return {
        items,
        total,
      };
    };

    results = await getItems();

    client.close();
  } catch (error) {
    console.log(error);
  }
  return res.json(results);
};

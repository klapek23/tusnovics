import sortArray from '@/src/utils/sortArray';
import connect from '@/src/backend/utils/mongodb';
import escapeRegExp from '@/src/utils/escapeRegex';

export default async (req, res) => {
  let results;
  const { query } = req;
  const { key, value } = query;
  const inputLength = value ? value.length : 0;

  if (inputLength === 0) {
    return res.json([]);
  }

  const escapedSearch = escapeRegExp(value).trim();

  try {
    const { db, client } = await connect();
    const collection = db.collection('gears');

    const getItems = (type) => {
      const regexp = new RegExp(`^.*${escapedSearch}.*`, 'gmi');
      return collection
        .aggregate([
          { $project: { [type]: `$${type}` } },
          { $unwind: `$${type}` },
          { $match: { [type]: { $regex: regexp } } },
        ])
        .toArray();
    };

    results = await getItems(key);
    results = results.map((item) => item[key]);

    client.close();
  } catch (error) {
    console.log(error);
  }

  results = [...new Set(results)];
  results = sortArray(results);
  return res.json(results);
};

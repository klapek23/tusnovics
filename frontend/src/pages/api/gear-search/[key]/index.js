import sortArray from '@/src/utils/sortArray';
import connect from '@/src/backend/utils/mongodb';

export default async (req, res) => {
  let results;
  const { query } = req;
  const { key } = query;

  try {
    const { db, client } = await connect();
    const collection = db.collection('gears');

    const getItems = (type) => {
      const regexp = new RegExp(`^.*`, 'gmi');
      return collection
        .aggregate([
          { $project: { [type]: `$${type}` } },
          { $unwind: `$${type}` },
          { $match: { [type]: { $regex: regexp } } },
        ])
        .toArray();
    };

    results = await getItems(key);
    results = results.map((item) => item[key]);

    client.close();
  } catch (error) {
    console.log(error);
  }

  results = results.map((item) => item.trim());
  results = results.filter((item) => !!item);
  results = [...new Set(results)];
  results.sort((a,b) => a.localeCompare(b, 'en', { numeric: true, sensitivity: 'base' }));
  return res.json(results);
};

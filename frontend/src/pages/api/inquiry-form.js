import path from 'node:path';
import { withIronSessionApiRoute } from 'iron-session/next';
import { handleValidatedForm } from '@/src/backend/utils/smtp';
import validateForm from '@/src/backend/utils/validate-inquiry-form';
import sessionOptions from '@/src/backend/utils/sessionOptions';
import Config from '@/config';

const { smtp } = Config;
const basePath = path.join('./src/emails');
const emailTemplate = `${basePath}/inquiry-form.ejs`;

export default withIronSessionApiRoute(async function inquiryFormRoute(
  req,
  res,
) {
  const { session } = req;
  const errors = await validateForm(req.body, session.captcha);

  return handleValidatedForm(
    res,
    req.body,
    (email) => `Nowa wiadomość od ${email} - ankieta zadowolenia`,
    emailTemplate,
    errors,
    ['office@tusnovics.pl'], // ['office@tusnovics.pl'] // ['klapek23@gmail.com']
    [],
    smtp.inquiry,
  );
},
sessionOptions);

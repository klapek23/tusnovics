import { withIronSessionApiRoute } from 'iron-session/next';
import path from 'node:path';
import fs from 'fs';

import sessionOptions from '@/src/backend/utils/sessionOptions';
import runMiddleware from '@/src/backend/middleware';
import authMiddleware from '@/src/backend/middleware/auth';

export default withIronSessionApiRoute(async (req, res) => {
  await runMiddleware(req, res, authMiddleware);

  const { name } = req.query;
  const filePath = path.resolve(`./src/karty-charakterystyki/${name}`);
  const file = fs.createReadStream(filePath);
  const stat = fs.statSync(filePath);

  res.setHeader('Content-Type', 'application/pdf');
  res.setHeader('Content-Length', stat.size);
  res.setHeader('Content-Disposition', `attachment; filename=${name}`);
  file.pipe(res);
}, sessionOptions);

import path from 'node:path';
import { handleValidatedForm } from '@/src/backend/utils/smtp';
import validateForm from '@/src/backend/utils/validate-tsvc-form';
import parseMultipartForm from '@/src/backend/middleware/multipart-form-parser';
import Config from '@/config';

const { smtp } = Config;
const basePath = path.join('./src/emails');
const emailTemplate = `${basePath}/service-form.ejs`;

// Helper method to wait for a middleware to execute before continuing
// And to throw an error when an error happens in a middleware
function runMiddleware(req, res, fn) {
  return new Promise((resolve, reject) => {
    fn(req, res, (result) => {
      if (result instanceof Error) {
        return reject(result);
      }

      return resolve(result);
    });
  });
}

export default async (req, res) => {
  // Run the middleware
  await runMiddleware(req, res, parseMultipartForm);

  // Validate fields
  const form = req.body;
  const { file } = req.files;
  const errors = validateForm(form);

  form.service_type = form.service_type
    ? Object.entries(JSON.parse(form.service_type))
        .filter(([_, value]) => !!value)
        .map(([_, value]) => value)
        .join(', ')
    : '';

  console.log(file);

  return handleValidatedForm(
    res,
    form,
    (email) => `Nowa wiadomość od ${email} - formularz serwisowy`,
    emailTemplate,
    errors,
    ['office@tusnovics.pl'], // ['klapek23@gmail.com'],
    file
      ? [
          {
            filename: file.name,
            path: file.path,
          },
        ]
      : [],
    smtp.inquiry,
  );
};

export const config = {
  api: {
    bodyParser: false,
  },
};

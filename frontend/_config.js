import isSsr from '@/src/utils/ssr';

const wpUrl = process.env.NEXT_PUBLIC_WORDPRESS_URL;
const wpContainerUrl = process.env.NEXT_PUBLIC_WORDPRESS_CONTAINER_URL;
const frontendUrl = process.env.NEXT_PUBLIC_FRONTEND_CONTAINER_URL;
const title = process.env.NEXT_PUBLIC_WORDPRESS_TITLE;
const authCookieName = process.env.FRONTEND_AUTH_COOKIE_NAME;
const authCookiePassword = process.env.FRONTEND_AUTH_COOKIE_PASSWORD;

const apiUrl = isSsr() ? `${wpContainerUrl}/wp-json` : `${wpUrl}/wp-json`;

const config = {
  apiUrl,
  wpUrl,
  frontendUrl,
  title,
  smtp: {
    inquiry: {
      host: 'smtp.office365.com', // 'fakesmtp',
      port: 587, // 1025
      smtpPass: 'inquiry',
      smtpEmail: 'inquiry@tusnovics.pl',
      smtpName: 'Tusnovics',
      adminEmail: 'inquiry@tusnovics.pl',
    },
    office: {
      host: 'smtp.office365.com', // 'fakesmtp',
      port: 587, // 1025
      smtpPass: 'Spuz8STe',
      smtpEmail: 'office@tusnovics.pl',
      smtpName: 'Tusnovics',
      adminEmail: 'office@tusnovics.pl',
    },
  },
  auth: {
    cookie: {
      name: authCookieName,
      password: authCookiePassword,
      secure: true,
      maxAge: undefined,
    },
  },
};

export default config;

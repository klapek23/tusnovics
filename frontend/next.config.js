module.exports = {
  swcMinify: true,
  output: 'standalone',
  images: {
    domains: ['wordpress.tusnovics.localhost'],
  },
};

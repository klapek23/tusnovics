/* eslint-disable global-require */
module.exports = {
  plugins: [
    ['postcss-easy-import', { prefix: '_' }], // keep this first
    'autoprefixer',
  ],
};

![WordPress + React Starter Kit](frontend/static/images/wordpress-plus-react-header.png)

[Postlight](https://postlight.com)'s Headless WordPress + React Starter Kit is an automated toolset that will spin up three things:

You can read all about it in [this handy introduction](https://postlight.com/trackchanges/introducing-postlights-wordpress-react-starter-kit).

**What's inside:**

1.  A WordPress backend that serves its data via the [WP REST API](https://developer.wordpress.org/rest-api/).
2.  A Frontend website project based on [NextJS](https://nextjs.org/) with React powered with WP REST API. 
3.  Another Frontend project, admin panel for managing CRM and Gear search database, with client side rendering, written in Vue and Typescript, bundled with Vite.
3.  BFF for admin panel for manging CRM and Gear search DB, based on ExpressJS with Typescript and Vite as bundler.
4.  MongoDB for CRM and Gear search. BFF use Mongoose to manage it. 
5.  SQL DB for Wordpress.
6.  NGINX which acts as reverse proxy for different services.
7.  FakeSMTP (only for development) which allows to fake email sending.
8.  Everything connected together with Docker and Docker Compose (separate compose files for production and development).
9.  PHPMyAdmin adn MongoExpress for easy managing both DBs. 

Let's get started.

## Install

_Prerequisite:_ Before you begin, you need [Docker](https://www.docker.com) installed. On Linux, you might need to install [docker-compose](https://docs.docker.com/compose/install/#install-compose) separately.

Docker Compose builds and starts few containers: 
- `db-headless`

- `wp-headless` 

- `frontend`

- `phpmyadmin`

- `nginx`

- `fakesmtp`

- `mongodb` 

- `mongo-express`

- `mongo-admin`

- `mongo-admin-api`

    docker-compose up -d

**Wait a few minutes** for Docker to build the services for the first time. After the initial build, startup should only take a few seconds.

You can follow the Docker output to see build progress and logs:

    docker-compose logs -f

Alternatively, you can use some useful Docker tools like Kitematic and/or VSCode Docker plugin to follow logs, start / stop / remove containers and images.

Once the containers are running, you can visit the all frontends and backend WordPress admin in your browser.

## Frontend website

This project contains a website ([https://tusnovics.pl](https://tusnovics.pl)).

The `frontend` container exposes on host `frontend.tusnovics.localhost` and `localhost:3000` (second one might not work because of CORS).

Techs used:
- NextJS (12)
- React (17)
- JavaScript (TypeScript is coming soon...)
- SWC for bundling
- MaterialUI
- WPAPI for communication with Wordpress headless backend
- Axios for communication with NextJS API

You can follow the `yarn start` output by running docker-compose `logs` command followed by the container name. For example:

    docker-compose logs -f frontend --tail=200

If you need to restart that process, restart the container:

    docker-compose restart frontend

## Wordpress Headless Backend

This project contains a website ([https://wordpress.tusnovics.pl](https://wordpress.tusnovics.pl)).

The `wp-headless` container exposes on host `wordpress.tusnovics.localhost`.

- Dashboard: [http://wordpress.tusnovics.localhost/wp-admin](http://localhost:8080/wp-admin) (default credentials `postlight`/`postlight`)
- REST API: [http://wordpress.tusnovics.localhost/wp-json](http://localhost:8080/wp-json)

This container includes some development tools:

    docker exec wp-headless composer --help
    docker exec wp-headless phpcbf --help
    docker exec wp-headless phpcs --help
    docker exec wp-headless phpunit --help
    docker exec wp-headless wp --info

Apache/PHP logs are available via `docker-compose logs -f --tail=200 wp-headless`.

### Extend the REST and GraphQL APIs

At this point you can start setting up custom fields in the WordPress admin, and if necessary, creating [custom REST API endpoints](https://developer.wordpress.org/rest-api/extending-the-rest-api/adding-custom-endpoints/) in the Postlight Headless WordPress Starter theme.

The primary theme code is located in `wordpress/wp-content/themes/postlight-headless-wp`.

### REST JWT Authentication

To give WordPress users the ability to sign in via the frontend app, use something like the [WordPress Salt generator](https://api.wordpress.org/secret-key/1.1/salt/) to generate a secret for JWT, then define it in `wp-config.php`

For the REST API:

    define('JWT_AUTH_SECRET_KEY', 'your-secret-here');

Make sure to read the [JWT REST](https://github.com/Tmeister/wp-api-jwt-auth) documentation for more info.

## Wordpress database (SQL / mariadb)

The `db-headless` container exposes MySQL on host port `3307`:

    mysql -uwp_headless -pwp_headless -h127.0.0.1 -P3307 wp_headless

You can also run a mysql shell on the container:

    docker exec db-headless mysql -hdb-headless -uwp_headless -pwp_headless wp_headless    
    
To manage this DB you can use PHPMyAdmin which is one of Docker-compose services. Should be available at http://localhost:8181.
    
## Mongo Admin frontend

This project contains a frontend admin panel ([https://mongo-admin.tusnovics.pl](https://mongo-admin.tusnovics.pl/)).

The `mongo-admin` container exposes on host `mongo-admin.tusnovics.localhost`.

Techs used:
- Vue (3)
- TypeScript
- Vite for bundling
- Vuetify
- Axios for communication with BFF

You can follow the `yarn start` output by running docker-compose `logs` command followed by the container name. For example:

    docker-compose logs -f mongo-admin --tail=200

If you need to restart that process, restart the container:

    docker-compose restart mongo-admin
    
## Mongo Admin BFF

This project contains a frontend admin panel ([https://mongo-admin-api.tusnovics.pl](https://mongo-admin-api.tusnovics.pl/)).

The `mongo-admin-api` container exposes on host `mongo-admin-api.tusnovics.localhost`.

Techs used:
- NodeJS
- TypeScript
- Vite for bundling
- ExpressJS
- Mongoose
- Some tools to work with Amazon S3 (manage images)

You can follow the `yarn start` output by running docker-compose `logs` command followed by the container name. For example:

    docker-compose logs -f mongo-admin-api --tail=200

If you need to restart that process, restart the container:

    docker-compose restart mongo-admin-api

## MongoDB for CRM and Gear search

The `mongodb` container exposes MySQL on host port `27017`. 
    
To manage this DB you can use MongoExpress which is one of Docker-compose services. Should be available at http://localhost:8081.

## NGINX

TBD...

## Linting

TBD...


## Hosting

Most WordPress hosts don't also host Node applications, so when it's time to go live, you will need to find a hosting service for the frontend.

That's why we've packaged the frontend app in a Docker container, which can be deployed to a hosting provider with Docker support like Amazon Web Services or Google Cloud Platform. For a fast, easier alternative, check out [Now](https://zeit.co/now).

## Troubleshooting Common Errors

**Docker Caching**

In some cases, you need to delete some containers images (not only the container) and rebuild it.

**CORS errors**

If you have deployed your WordPress install and are having CORS issues be sure to update `/wordpress/wp-content/themes/postlight-headless-wp/inc/frontend-origin.php` with your frontend origin URL.

See anything else you'd like to add here? Please send a pull request!

---

🔬 A Labs project from your friends at [Postlight](https://postlight.com). Happy coding!

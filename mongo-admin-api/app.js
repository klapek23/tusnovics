const express = require("express");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const cors = require("cors");

require("dotenv").config();

require("./config/database").connect();

const authRouter = require("./routes/auth");
const cmoiwRouter = require("./routes/cmoiw");
const gearRouter = require("./routes/gear");

const auth = require("./middleware/auth");

const app = express();

const corsWhitelist = [
  "https://mongo-admin.tusnovics.pl",
  "https://www.mongo-admin.tusnovics.pl",
  "http://mongo-admin.tusnovics.pl",
  "http://www.mongo-admin.tusnovics.pl",
  "http://mongo-admin.tusnovics.localhost",
  "https://mongo-admin.tusnovics.localhost",
];

const corsOrigin = function (origin, callback) {
  if (corsWhitelist.indexOf(origin) !== -1) {
    callback(null, true);
  } else {
    callback(new Error("Not allowed by CORS"));
  }
};

app.use(express.static(__dirname + '/public'));

app.use(
  cors({
    origin: corsOrigin,
  })
);
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());

app.use("/auth", authRouter);
app.use("/cmoiw", auth, cmoiwRouter);
app.use("/gear", auth, gearRouter);

module.exports = app;

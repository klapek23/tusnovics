const express = require("express");
const axios = require("axios");
const jwt = require("jsonwebtoken");

const User = require("../model/user");
const router = express.Router();

const TOKEN_KEY =
  'h#^Kwaz)Xs|I/GpTlWtQBPMm3Sj:"A#zR/^l.|FMZEtgaEqMCvQy34S`wDXCSE>';

router.get("/", function (req, res, next) {
  res.status(200).json({ status: "ok" });
});

/*
router.post("/register", async function (req, res, next) {
  try {
    // Get user input
    const { name, email, password } = req.body;

    // Validate user input
    if (!(email && password && name)) {
      res.status(400).send("All input is required");
    }

    // check if user already exist
    // Validate if user exist in our database
    const oldUser = await User.findOne({ email });

    if (oldUser) {
      return res.status(409).json(oldUser);
    }

    //Encrypt user password
    const encryptedPassword = await bcrypt.hash(password, 10);

    // Create user in our database
    const user = await User.create({
      name,
      email: email.toLowerCase(), // sanitize: convert email to lowercase
      password: encryptedPassword,
    });

    // Create token
    const token = jwt.sign({ user_id: user._id, email }, TOKEN_KEY, {
      expiresIn: "2h",
    });
    // save user token
    user.token = token;

    // return new user
    res.status(201).json(user);
  } catch (err) {
    console.log(err);
  }
});
*/

router.post("/login", async function (req, res, next) {
  // Get user input
  const { username, password } = req.body;

  // Validate user input
  if (!(username && password)) {
    res.status(400).send("All input is required");
  }

  try {
    // Validate if user exist in Wordpress
    const { data: session } = await axios.post(
      "http://wp-headless:8080/wp-json/custom/login",
      {
        username,
        password,
      }
    );
    const user = {
      username: session.data.user_login,
      email: session.data.user_email,
      token: session.token,
    };

    //if (user && (await bcrypt.compare(password, user.password))) {
    if (user) {
      // Create token
      const token = jwt.sign(user, TOKEN_KEY, {
        expiresIn: "24h",
      });

      // save user token
      user.token = token;

      // user
      res.status(200).json(user);
    }

    res.status(400).json({ error: "Invalid Credentials" });
  } catch (err) {
    console.log(err);
    if (err.response.status === 403) {
      res.status(400).json({ error: "Invalid Credentials" });
    }

    res.status(500).json({ error: err.toString() });
  }
});

router.get("/user", async function (req, res, next) {
  try {
    const token = req.headers["x-access-token"];
    const user = jwt.verify(token, TOKEN_KEY, null, null);
    res.status(200).json(user);
  } catch (err) {
    console.log(err);
    res.status(401).json({ error: err.toString() });
  }
});

module.exports = router;

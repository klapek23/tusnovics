const express = require("express");
const router = express.Router();
const multer = require("multer");
const ObjectID = require("mongoose").Types.ObjectId;
const { xlsx2Json } = require("../utils");

const CMOIW = require("../model/cmoiw");

const storage = multer.memoryStorage();
const upload = multer({ storage });

router.get("/", async function (req, res, next) {
  try {
    const { itemsPerPage, page, sortBy, search } = req.query;
    const skip = page > 0 ? (page - 1) * itemsPerPage : 0;
    const sort = sortBy.map((sortItem) => [
      sortItem.key,
      sortItem.order === "asc" ? 1 : -1,
    ]);
    const cleanSearch = search.replace(/[|&;$%@"<>()+,]/g, "");
    const findItems = {
      $or: [
        { number: new RegExp("^" + cleanSearch, "i") },
        { material: new RegExp("^" + cleanSearch, "i") },
        { cas: new RegExp("^" + cleanSearch, "i") },
        { manufacturer: new RegExp("^" + cleanSearch, "i") },
      ],
    };
    const items = await CMOIW.find(findItems)
      .sort(sort)
      .skip(skip)
      .limit(itemsPerPage);
    const count = await CMOIW.find(findItems).count();
    res.status(200).json({ items, count });
  } catch (err) {
    console.log(err);
    res.status(500).json({ error: err.toString() });
  }
});

router.get("/:id", async function (req, res, next) {
  try {
    const _id = req.params.id;
    const item = await CMOIW.findOne({ _id: ObjectID(_id) });
    res.status(200).json(item);
  } catch (err) {
    console.log(err);
    res.status(500).json({ error: err.toString() });
  }
});

router.post("/", async function (req, res, next) {
  try {
    const _id = new ObjectID().toString();
    const item = await CMOIW.create({ ...req.body.data, _id });
    res.status(200).json(item);
  } catch (err) {
    console.log(err);
    res.status(500).json({ error: err.toString() });
  }
});

router.post(
  "/batch",
  upload.single("data[file]"),
  async function (req, res, next) {
    try {
      const decorateWithId = (data) =>
        data.map((item) => ({ ...item, _id: new ObjectID().toString() }));
      const data = decorateWithId(xlsx2Json(req.file));
      await CMOIW.insertMany(data);
      res.status(200).json(data);
    } catch (err) {
      console.log(err);
      res.status(500).json({ error: err.toString() });
    }
  }
);

router.put("/:id", async function (req, res, next) {
  try {
    const _id = req.params.id;
    const { data } = req.body;
    await CMOIW.findOneAndUpdate({ _id: ObjectID(_id) }, data);
    const item = await CMOIW.findOne({ _id: ObjectID(_id) });
    res.status(200).json(item);
  } catch (err) {
    console.log(err);
    res.status(500).json({ error: err.toString() });
  }
});

router.delete("/", async function (req, res, next) {
  try {
    const { ids } = req.body;
    const deleted = await CMOIW.deleteMany({ _id: { $in: ids } });
    res.status(200).json({ deleted });
  } catch (err) {
    console.log(err);
    res.status(500).json({ error: err.toString() });
  }
});

module.exports = router;

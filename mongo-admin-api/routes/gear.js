const express = require("express");
const router = express.Router();
const ObjectID = require("mongoose").Types.ObjectId;
const multer = require("multer");
const multerS3 = require("multer-s3");
const s3Client = require("../config/s3").s3Client;

const Gear = require("../model/gear");

const upload = multer({
  storage: multerS3({
    s3: s3Client,
    bucket: "aparatura",
    contentType: multerS3.AUTO_CONTENT_TYPE,
    contentDisposition: "inline",
    metadata: function (req, file, cb) {
      cb(null, { fieldName: file.fieldname });
    },
    key: function (req, file, cb) {
      cb(null, `${req.params.id}/${file.originalname}`);
    },
  }),
});

router.get("/", async function (req, res, next) {
  try {
    const { itemsPerPage, page, sortBy, search } = req.query;
    const skip = page > 0 ? (page - 1) * itemsPerPage : 0;
    const sort = sortBy.map((sortItem) => [
      sortItem.key,
      sortItem.order === "asc" ? 1 : -1,
    ]);
    const cleanSearch = search.replace(/[|&;$%@"<>()+,]/g, "");
    const findItems = {
      $or: [
        { name: new RegExp(cleanSearch, "ig") },
        { norm: new RegExp("^" + cleanSearch, "i") },
        { method: new RegExp("^" + cleanSearch, "i") },
        { manufacturer: new RegExp("^" + cleanSearch, "i") },
        { category: new RegExp("^" + cleanSearch, "i") },
      ],
    };
    const items = await Gear.find(findItems)
      .sort(sort)
      .skip(skip)
      .limit(itemsPerPage > 0 ? itemsPerPage : count);
    const count = await Gear.find(findItems).count();
    res.status(200).json({ items, count });
  } catch (err) {
    console.log(err);
    res.status(500).json({ error: err.toString() });
  }
});

router.get("/:id", async function (req, res, next) {
  try {
    const _id = req.params.id;
    const item = await Gear.findOne({ _id });
    res.status(200).json(item);
  } catch (err) {
    console.log(err);
    res.status(500).json({ error: err.toString() });
  }
});

router.post(
  "/",
  upload.fields([
    { name: "data[image]", maxCount: 1 },
    { name: "data[pdf]", maxCount: 1 },
  ]),
  async function (req, res, next) {
    const getFileByKey = (requestKey, modelKey) =>
      req.files && req.files[requestKey]
        ? req.files[requestKey][0].location
        : req[modelKey];

    try {
      const { data } = req.body;
      const image = getFileByKey("data[image]", "image");
      const pdf = getFileByKey("data[pdf]", "pdf");

      const _id = new ObjectID().toString();
      const item = await Gear.create({ ...req.body.data, image, pdf, _id });
      res.status(200).json(item);
    } catch (err) {
      console.log(err);
      res.status(500).json({ error: err.toString() });
    }
  }
);

router.put(
  "/:id",
  upload.fields([
    { name: "data[image]", maxCount: 1 },
    { name: "data[pdf]", maxCount: 1 },
  ]),
  async function (req, res, next) {
    const getFileByKey = (requestKey, modelKey) =>
      req.files && req.files[requestKey]
        ? req.files[requestKey][0].location
        : req.body.data[modelKey];
    try {
      const { data } = req.body;
      const image = getFileByKey("data[image]", "image");
      const pdf = getFileByKey("data[pdf]", "pdf") ?? null;
      const _id = req.params.id;
      await Gear.findOneAndUpdate({ _id }, { ...data, image, pdf });
      const item = await Gear.findOne({ _id });
      res.status(200).json(item);
    } catch (err) {
      console.log(err);
      res.status(500).json({ error: err.toString() });
    }
  }
);

router.delete("/", async function (req, res, next) {
  try {
    const { ids } = req.body;
    const deleted = await Gear.deleteMany({ _id: { $in: ids } });
    res.status(200).json({ deleted });
  } catch (err) {
    console.log(err);
    res.status(500).json({ error: err.toString() });
  }
});

module.exports = router;

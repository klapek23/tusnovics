const xlsx = require("xlsx");

function xlsx2Json(xlsxFile) {
  const file = xlsx.read(xlsxFile.buffer, { type: "buffer" });
  let parsedData = [];

  // Grab the sheet info from the file
  const sheetNames = file.SheetNames;
  const totalSheets = sheetNames.length;

  // Loop through sheets
  for (let i = 0; i < totalSheets; i++) {
    // Convert to json using xlsx
    const tempData = xlsx.utils.sheet_to_json(file.Sheets[sheetNames[i]]);

    // Add the sheet's json to our data array
    parsedData.push(...tempData);
  }

  return parsedData;
}

module.exports = { xlsx2Json };

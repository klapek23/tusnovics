const mongoose = require("mongoose");

const cmoiwSchema = new mongoose.Schema({
  _id: { type: mongoose.Mixed, required: true },
  number: { type: String, default: null, required: true },
  material: { type: String, default: null, required: true },
  description: { type: String, default: null },
  cas: { type: String, default: null, required: true },
  manufacturer: { type: String, default: null },
  extra_info: { type: String, default: null },
});

module.exports = mongoose.model("cmoiw", cmoiwSchema);

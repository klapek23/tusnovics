const mongoose = require("mongoose");

const gearSchema = new mongoose.Schema({
  _id: { type: String, required: true },
  name: { type: String, default: null, required: true },
  description: { type: String, default: null },
  norm: { type: [String], required: true },
  method: { type: [String], required: true },
  manufacturer: { type: String, required: true, default: null },
  url: { type: String, default: null },
  catalogs: { type: String },
  video: { type: String, default: null },
  pdf: { type: String, default: null },
  image: { type: String, required: true, default: null },
  category: { type: [String], required: true },
});

module.exports = mongoose.model("gear", gearSchema);

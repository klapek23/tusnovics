const S3Client = require("@aws-sdk/client-s3").S3Client;
const region = process.env.S3_REGION;

// Create an Amazon S3 service client object.
const s3Client = new S3Client({
  region,
  credentials: {
    accessKeyId: process.env.S3_ACCESS_KEY,
    secretAccessKey: process.env.S3_ACCESS_SECRET,
  },
});

exports.s3Client = s3Client;

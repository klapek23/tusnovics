const mongoose = require("mongoose");

const MONGO_URI = 'mongodb://klapek23:TakV8n33Cmmx@mongodb:27017/tusnovics?authSource=admin'

exports.MONGO_URI = MONGO_URI;

exports.connect = () => {
  // Connecting to the database
  mongoose
    .connect(MONGO_URI, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
    .then(() => {
      console.log("Successfully connected to database");
    })
    .catch((error) => {
      console.log("database connection failed. exiting now...");
      console.error(error);
      process.exit(1);
    });
};

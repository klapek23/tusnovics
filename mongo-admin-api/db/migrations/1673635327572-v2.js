const mongoose = require("mongoose");
const Gear = require("../../model/gear");

require("dotenv").config();

const MONGO_URI = process.env.MONGODB_CONNECTION_STRING;

async function up() {
  await mongoose.connect(MONGO_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });

  await Gear.updateMany({}, [
    {
      $replaceWith: {
        $mergeObjects: [
          "$$ROOT",
          {
            method: {
              $map: {
                input: "$method",
                as: "item",
                in: { $trim: { input: "$$item" } },
              },
            },
          },
        ],
      },
    },
  ]);
}

async function down() {}

module.exports = { up, down };

const mongoose = require("mongoose");
const Gear = require("../../model/gear");

require("dotenv").config();

const MONGO_URI = process.env.MONGODB_CONNECTION_STRING;

async function up() {
  await mongoose.connect(MONGO_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });

  await Gear.updateMany({}, [
    {
      $replaceWith: {
        $mergeObjects: ["$$ROOT", { norm: { $split: ["$norm", ";"] } }],
      },
    },
    {
      $replaceWith: {
        $mergeObjects: ["$$ROOT", { method: { $split: ["$method", ";"] } }],
      },
    },
    {
      $replaceWith: {
        $mergeObjects: ["$$ROOT", { category: { $split: ["$category", ";"] } }],
      },
    },
  ]);
}

async function down() {
  await mongoose.connect(MONGO_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });

  await Gear.updateMany({}, [
    {
      $replaceWith: {
        $mergeObjects: [
          "$$ROOT",
          {
            norm: {
              $reduce: {
                input: "$norm",
                initialValue: "",
                in: { $concat: ["$$value", ";", "$$this"] },
              },
            },
          },
        ],
      },
    },
    {
      $replaceWith: {
        $mergeObjects: [
          "$$ROOT",
          {
            method: {
              $reduce: {
                input: "$method",
                initialValue: "",
                in: { $concat: ["$$value", ";", "$$this"] },
              },
            },
          },
        ],
      },
    },
    {
      $replaceWith: {
        $mergeObjects: [
          "$$ROOT",
          {
            category: {
              $reduce: {
                input: "$category",
                initialValue: "",
                in: { $concat: ["$$value", ";", "$$this"] },
              },
            },
          },
        ],
      },
    },
  ]);
}

module.exports = { up, down };

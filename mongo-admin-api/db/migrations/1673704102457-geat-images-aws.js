const mongoose = require("mongoose");
const Gear = require("../../model/gear");

require("dotenv").config();

const MONGO_URI = process.env.MONGODB_CONNECTION_STRING;

async function up() {
  await mongoose.connect(MONGO_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });

  await Gear.updateMany({}, [
    {
      $replaceWith: {
        $mergeObjects: [
          "$$ROOT",
          {
            image: {
              $replaceAll: {
                input: "$image",
                find: "/static/tsvc-gear-search/files/",
                replacement: "https://aparatura.s3.eu-central-1.amazonaws.com/",
              },
            },
            pdf: {
              $replaceAll: {
                input: "$pdf",
                find: "/static/tsvc-gear-search/files/",
                replacement: "https://aparatura.s3.eu-central-1.amazonaws.com/",
              },
            },
            video: {
              $replaceAll: {
                input: "$video",
                find: "/static/tsvc-gear-search/files/",
                replacement: "https://aparatura.s3.eu-central-1.amazonaws.com/",
              },
            },
            catalogs: {
              $replaceAll: {
                input: "$catalogs",
                find: "/static/tsvc-gear-search/files/",
                replacement: "https://aparatura.s3.eu-central-1.amazonaws.com/",
              },
            },
          },
        ],
      },
    },
  ]);
}

async function down() {
  await mongoose.connect(MONGO_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });

  await Gear.updateMany({}, [
    {
      $replaceWith: {
        $mergeObjects: [
          "$$ROOT",
          {
            image: {
              $replaceAll: {
                input: "$image",
                find: "https://aparatura.s3.eu-central-1.amazonaws.com/",
                replacement: "/static/tsvc-gear-search/files/",
              },
            },
            pdf: {
              $replaceAll: {
                input: "$pdf",
                find: "https://aparatura.s3.eu-central-1.amazonaws.com/",
                replacement: "/static/tsvc-gear-search/files/",
              },
            },
            video: {
              $replaceAll: {
                input: "$video",
                find: "https://aparatura.s3.eu-central-1.amazonaws.com/",
                replacement: "/static/tsvc-gear-search/files/",
              },
            },
            catalogs: {
              $replaceAll: {
                input: "$catalogs",
                find: "https://aparatura.s3.eu-central-1.amazonaws.com/",
                replacement: "/static/tsvc-gear-search/files/",
              },
            },
          },
        ],
      },
    },
  ]);
}

module.exports = { up, down };

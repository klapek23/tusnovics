import { S3Client } from "@aws-sdk/client-s3";

const region = import.meta.env.VITE_S3_REGION;

console.log(region);

// Create an Amazon S3 service client object.
const s3Client = new S3Client({
  region,
  credentials: {
    accessKeyId: import.meta.env.VITE_S3_ACCESS_KEY,
    secretAccessKey: import.meta.env.VITE_S3_ACCESS_SECRET,
  },
});

export { s3Client };
